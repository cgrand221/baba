#!/bin/bash

process_name="node"
pids=$(pgrep -f "node.*baba.*parcel")
for pid in $pids; do
    kill -9 "$pid"
done
