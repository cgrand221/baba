# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2025-01-19
- Ignore the password case sensitivity

## [1.0.0] - 2025-01-19
### Added
- groups
- better animations
- L1(1) parser with with a given grammar file : Lezer
- Refactoring of rule evaluation
- optimization : grid of game indexded by positions of elements in grid
- parallelized tests
- most levels of the game
- save game on localStorage and manipulation of saves with browser
- migration from nvm to volta
- added a password after playing 10 levels out of respect for the original creator of the game, 'Hempuli'
### Removed
- legacy compiler
- generated rules with "not", "all"
### changed
- node 20.17 to 22.12

## [0.1.0] - 2024-01-29
### Added

- levels
    - pictures and texts
    - levels in xml 
    - physics of the game, compilation of rules
- tests with jest
- deploy 
- quick level build

