export class Quality{
 
    public static YOU = 'you';//tested
    public static PUSH = 'push';//tested
    public static STOP = 'stop';//tested   
    public static DEFEAT = 'defeat';//tested
    public static WIN = 'win';//tested  
    public static SINK = 'sink';//tested
    public static MOVE  = 'move';//tested
    public static HOT = 'hot';//implemented
    public static MELT = 'melt';//implemented
    public static WORD = 'word';//implemented

    public static SHUT = 'shut';// tested
    public static OPEN = 'open';// tested
    public static FLOAT = 'float';// tested

    public static WEAK = 'weak';// tested

    public static TELE = 'tele';// tested
    public static PULL = 'pull';// tested

    public static SHIFT = 'shift';// tested
    public static FALL = 'fall';// tested
    public static FALL_D = 'fall_d';//implemented
    public static FALL_R = 'fall_r';//implemented
    public static FALL_U = 'fall_u';//implemented
    public static FALL_L = 'fall_l';//implemented
    public static SLEEP = 'sleep';//implemented
    public static MORE = 'more';
    public static RIGHT = 'right';// implemented
    public static UP = 'up';// implemented
    public static LEFT = 'left';// implemented
    public static DOWN = 'down';// implemented
    public static SWAP = 'swap';// tested

    public static SELECT = 'select';// tested
    public static PATH = 'path';// tested
    public static BONUS = 'bonus';// tested
    
    public static qualities= [
        Quality.WORD,
        Quality.YOU,
        Quality.PUSH,
        Quality.STOP,
        Quality.PULL,
        Quality.DEFEAT,
        Quality.WIN,
        Quality.FLOAT,
        Quality.WEAK,
        Quality.SHUT,
        Quality.OPEN,
        Quality.HOT,
        Quality.MELT,
        Quality.SINK,
        Quality.MOVE ,
        Quality.TELE,
        Quality.SHIFT,
        Quality.FALL,
        Quality.FALL_D,
        Quality.FALL_R,
        Quality.FALL_U,
        Quality.FALL_L,
        Quality.SLEEP,
        Quality.MORE,
        Quality.RIGHT,
        Quality.UP,
        Quality.LEFT,
        Quality.DOWN,
        Quality.SWAP,
        Quality.BONUS,
    ];
   
}