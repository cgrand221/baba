export class Condition{

    public static ON = 'on';
    public static NEAR = 'near';
    public static FACING = 'facing';
    public static WITHOUT = 'without';
    public static ABOVE = 'above';
    public static BELOW = 'below';
    public static BESIDE_L = 'beside_l';
    public static BESIDE_R = 'beside_r';
    public static FEELING = 'feeling';
    public static NEXTTO = 'nextto';
    public static SEEING = 'seeing';
    
    public static conditions = [
        Condition.ON,
        Condition.NEAR,
        Condition.FACING,
    ]

}