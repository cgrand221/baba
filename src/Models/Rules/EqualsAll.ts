export class EqualsAll{
 
    public static equalsAll = <T extends {equals: (a:T) => boolean}>(firsts:T[], seconds:T[]) => {
        if(firsts.length !== seconds.length){
            return false;
        }
        return firsts.every((first, index) => {
            let second = seconds[index];
            if(!second){
                return false;
            }
            return first.equals(second);
        });
    }
}