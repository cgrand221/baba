export class Verb{
    public static IS = 'is';
    public static HAS = 'has';
    public static MAKE = 'make';
    public static WRITE = 'write';
    public static verbs= [
        Verb.IS,
        Verb.HAS,
        Verb.MAKE,
        Verb.WRITE,
    ];
}