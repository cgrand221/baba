import { EqualsAll } from "./Rules/EqualsAll";

export class LeftRightBaseRule{
    
    private _disableRights: string[];
   
    constructor(
        private _nbNot: number,
        private _content: string,
        private _start: number,
        private _end: number,
    ){
        this._disableRights = [];
    }

    public get content(): string {
        return this._content;
    }
    public set content(value: string) {
        this._content = value;
    }

    public get nbNot(): number {
        return this._nbNot;
    }
    public set nbNot(value: number) {
        this._nbNot = value;
    }

    public get not(): boolean {
        return this._nbNot % 2 === 1;
    }

    public get end(): number {
        return this._end;
    }
    public set end(value: number) {
        this._end = value;
    }
    
    public get start(): number {
        return this._start;
    }
    public set start(value: number) {
        this._start = value;
    }

    


    public get disableRights(): string[] {
        return this._disableRights;
    }
    public set disableRights(value: string[]) {
        this._disableRights = value;
    }

    public resetDisabled = () => {
        this._disableRights = [];
    }
    

    public equals = (lrbr:LeftRightBaseRule):boolean => {        
        return (
            this.nbNot === lrbr.nbNot
            && this.disableRights.length === lrbr.disableRights.length
            && this.disableRights.every((disableRight, index) => {
                let lrbrdisableRight = lrbr.disableRights[index];
                if(!lrbrdisableRight){
                    return false;
                }
                return disableRight === lrbrdisableRight;
            }) 
            && this.content === lrbr.content
            && this.start === lrbr.start
            && this.end === lrbr.end
        );
    }

    public toString = ():string => {
        let nots:string[]=[];
        for (let i = 0; i < this.nbNot;i++){
            nots.push('not');
        }
        return (
            nots.join(' ')
            + ((nots.length) > 0 ? ' ' : '')
            + this.content
        );
    }

    public static equalsAll = EqualsAll.equalsAll<LeftRightBaseRule>;
}