import { Rule } from "./Rule";


export class LevelSave {
    public constructor(
        public _isWin: boolean,
        public _isBonus: boolean,
        public _nbSpores: number,
        public _nbRequiredSpores: number|null,
        public _nbSporesZoneCompleted: number|null,
        public _savedRules: Rule[],
        public _parentSavedRules: Rule[],
        public _select: string|null,
    ){
    }    
}