import { TextOrObject } from "./TextOrObject";

export class Movement{
    
    
    private _dx: number;
    private _dy: number;
    private _moveDirection: number;
    private _destroy: boolean;
   
    constructor(
        private _textOrObject: TextOrObject,
        private _allowChangeDirection: boolean
    ){
        this._dx = 0;
        this._dy = 0;
        this._destroy = false;
        this._moveDirection = TextOrObject.DEFAULT_MOVE_DIR;
    }
    public get dx(): number {
        return this._dx;
    }
    public set dx(value: number) {
        this._dx = value;
    }

    public get dy(): number {
        return this._dy;
    }
    public set dy(value: number) {
        this._dy = value;
    }

    public get textOrObject(): TextOrObject {
        return this._textOrObject;
    }
    public set textOrObject(value: TextOrObject) {
        this._textOrObject = value;
    }

    public get allowChangeDirection(): boolean {
        return this._allowChangeDirection;
    }
    public set allowChangeDirection(value: boolean) {
        this._allowChangeDirection = value;
    }

    public get moveDirection(): number {
        return this._moveDirection;
    }
    public set moveDirection(value: number) {
        this._moveDirection = value;
    }

    public get destroy(): boolean {
        return this._destroy;
    }
    public set destroy(value: boolean) {
        this._destroy = value;
    }
}