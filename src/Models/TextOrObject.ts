import { Rule } from "./Rule";
import { RLevel } from "./Rules/RLevel";

export class TextOrObject {

    public static UP=1;
    public static RIGHT=2;
    public static DOWN=-1;
    public static LEFT=-2;
    public static DEFAULT_MOVE_DIR = TextOrObject.DOWN;

    public static VIEW_STATE_LOCKED = 1;
    public static VIEW_STATE_VISIBLE = 2;
    public static VIEW_STATE_WIN = 3;
    public static VIEW_STATE_HAS_REQUIRED_SPORES = 4;

    //basic properties
    private _name: string;
    private _moveDirection: number;
    private _id: number;
    private _levelPath: string | null;
    private _becomeLevelPath: string | null;
    private _viewState: number;
 
    

    // calculated properties
    private _rules: Rule[];
    private _isWord: boolean;
    private _isWordOld: boolean;    
    
    private _isStop:boolean;   
    private _isPush:boolean;
    private _isYou:boolean;
    private _isWin: boolean;
    private _isHot: boolean;
    private _isMelt: boolean;
    private _isDefeat: boolean;
    private _isSink: boolean;
    private _nbMoves: number;
    private _isOpen: boolean;
    private _isShut: boolean;
    private _isFloat: boolean;
    private _isWeak: boolean;  
    private _isSelfDestroy: boolean;
    private _disableWeak: boolean;
    private _isTele: boolean;
    private _isPull: boolean;
    private _nbShift: number;
    private _nbFallD: number;
    private _nbFallR: number;
    private _nbFallU: number;
    private _nbFallL: number;
    private _isSleep: boolean;
    private _isGlobalLevel: boolean;
    private _isLevel: boolean;
   

    private _changeMoveDirection: number|null;// same values as _moveDirection or null
    private _isSwap: boolean;// same values as _moveDirection or null
    private _isMore: boolean;// same values as _moveDirection or null    
    private _nbGroup: number;
    private _nbGroupOld: number;
    private _isSelect: boolean;
    private _isPath: boolean;
    private _isBonus: boolean;
   


    //only for view
    private _src: string|null;
    private _text: string | null;
    private _priority: number;
    private _xOld: number;
    private _xMin: number;
    private _xMax: number;
    private _yOld: number;
    private _yMin: number;
    private _yMax: number;
    private _isLevelHighlight: boolean;
   
    

    // only for history
    private _hPrevious: TextOrObject | null;    
    private _hNext: TextOrObject[];    
    private _hIsDestroy: boolean;

 

    // identity
    private static _identity=0;


    constructor(
        _name: string,
        private _isObject: boolean, 
        private _x: number,
        private _y: number,
    ){
        this._name = _name.toLowerCase();
        this._levelPath = null;
        this._becomeLevelPath = null;
        this._viewState = TextOrObject.VIEW_STATE_VISIBLE;
        this._isWordOld = false;
        this._isWord = false;
        this._moveDirection = TextOrObject.DEFAULT_MOVE_DIR;
        this._id = TextOrObject.getIdentity();
        this._rules = [];
        this._isStop = false;
        this._isPush = false;
        this._isYou = false;
        this._isWin = false;
        this._isHot = false;
        this._isMelt = false;
        this._isDefeat = false;
        this._isSink = false;
        this._nbMoves = 0;
        this._isOpen = false;
        this._isShut = false;
        this._isFloat = false;
        this._isWeak = false;
        this._isSelfDestroy = false;
        this._disableWeak = false;
        this._isTele = false;
        this._isPull = false;
        this._nbShift = 0;
        this._nbFallD = 0;
        this._nbFallR = 0;
        this._nbFallU = 0;
        this._nbFallL = 0;
        this._isSleep = false;
        this._isGlobalLevel = false;
        this._isLevel = false;

        this._changeMoveDirection = null;
        this._isSwap = false;
        this._isMore = false;
        this._nbGroup = 0;
        this._isSelect = false;
        this._isPath = false;
        this._isBonus = false;

        this._nbGroupOld = 0;
        this._src = null;
        this._text = null;
        this._priority = 0;
        this._hPrevious = null;
        this._hNext = [];
        this._hIsDestroy = false;
        this._xOld = 0;
        this._xMin = 0;
        this._xMax = 0;
        this._yOld = 0;
        this._yMin = 0;
        this._yMax = 0;
        this._isLevelHighlight = false;
    }

    private static getIdentity(){
        TextOrObject._identity++;
        return TextOrObject._identity;
    }

    public startMovement = ():void => {
        this._xOld = this.x;
        this._xMin = this.x;
        this._xMax = this.x;
        this._yOld = this.y;
        this._yMin = this.y;
        this._yMax = this.y;
    }
    public endMovement = ():void => {
        if(this._xOld === this._xMin){// right move only
            this._xMin = this._xMax;
        }
        else if(this._xOld === this._xMax){// left move only
            this._xMax = this._xMin;
        }
        else if(Math.abs(this.x - this._xMax) < Math.abs(this.x - this._xMin)){// x nearest _xMax, we use _xMin
            this._xMax = this._xMin;
        }
        else{
            this._xMin = this._xMax;
        }

        if(this._yOld === this._yMin){// down move only
            this._yMin = this._yMax;
        }
        else if(this._yOld === this._yMax){// up move only
            this._yMax = this._yMin;
        }
        else if(Math.abs(this.y - this._yMax) < Math.abs(this.y - this._yMin)){// y nearest _yMax, we use _yMin
            this._yMax = this._yMin;
        }
        else{
            this._yMin = this._yMax;
        }
    }

    public resetOtherProperties = (): void => {
        this.isStop = false;
        this.isPush = false;
        this.isYou = false;
        this.isWin = false;
        this.isHot = false;
        this.isMelt = false;
        this.isDefeat = false;
        this.isSink = false;
        this.nbMoves = 0;
        this.isOpen = false;
        this.isShut = false;
        this.isFloat = false;
        this.isWeak = false;
        // we do not change disableWeak this property us affected once per new LevelState
        this.isTele = false;
        this.isPull = false;
        this.nbShift = 0;
        this.nbFallD = 0;
        this.nbFallR = 0;
        this.nbFallU = 0;
        this.nbFallL = 0;
        this.isSleep = false;
        
        // we do not change isGlobalLevel this property will never change
        // we do not change isLevel this property will never change
        this.changeMoveDirection = null;
        this.isSwap = false;
        this.isMore = false;
        this.isSelect = false;
        this.isPath = false;
        this.isBonus = false;
    }

    public resetWord = (): void => {
        this.isWord = false;
    }
    public saveWord = (): void => {
        this._isWordOld = this._isWord;
    }
    public hasWordChanged = (): boolean => {
        return this._isWord !== this._isWordOld;
    }


    public resetGroup = (): void => {
        this.nbGroup = 0;
    }
    public saveGroup = (): void =>{
        this._nbGroupOld = this.nbGroup;
    }
    public hasGroupChanged = (): boolean => {
        return this._nbGroupOld !== this.nbGroup;
    }


    public clone = (): TextOrObject => {
        let textOrObject = new TextOrObject(this.name, this.isObject, this.x, this.y);
        textOrObject.levelPath = this.levelPath;
        textOrObject.becomeLevelPath = this.becomeLevelPath;
        textOrObject.viewState = this.viewState;
        textOrObject.moveDirection = this.moveDirection;
        textOrObject.id = this.id;
        
        textOrObject.rules = this.rules;
        textOrObject.isWord = this.isWord;
        textOrObject.isStop = this.isStop;
        textOrObject.isPush = this.isPush;
        textOrObject.isYou = this.isYou;
        textOrObject.isWin = this.isWin;
        textOrObject.isHot = this.isHot;
        textOrObject.isMelt = this.isMelt;
        textOrObject.isDefeat = this.isDefeat;
        textOrObject.isSink = this.isSink;
        textOrObject.nbMoves = this.nbMoves;
        textOrObject.isOpen = this.isOpen;
        textOrObject.isShut = this.isShut;
        textOrObject.isFloat = this.isFloat;
        textOrObject.isWeak = this.isWeak;
        textOrObject.isSelfDestroy = this.isSelfDestroy;
        textOrObject.disableWeak = this.disableWeak;
        textOrObject.isTele = this.isTele;
        textOrObject.isPull = this.isPull;
        textOrObject.nbShift = this.nbShift;
        textOrObject.nbFallD = this.nbFallD;
        textOrObject.nbFallR = this.nbFallR;
        textOrObject.nbFallU = this.nbFallU;
        textOrObject.nbFallL = this.nbFallL;
        textOrObject.isSleep = this.isSleep;
        textOrObject.isGlobalLevel = this.isGlobalLevel;
        textOrObject.isLevel = this.isLevel;

        textOrObject.changeMoveDirection = null;
        textOrObject.isSwap = this.isSwap;
        textOrObject.isMore = this.isMore;
        textOrObject.nbGroup = this.nbGroup;
        textOrObject.isSelect = this.isSelect;
        textOrObject.isPath = this.isPath;
        textOrObject.isBonus = this.isBonus;
        textOrObject.nbGroup = this.nbGroup;
        textOrObject.src = this.src;
        textOrObject.text = this.text;
        textOrObject.priority = this.priority;

        textOrObject._xOld = this._xOld;
        textOrObject._xMin = this._xMin;
        textOrObject._xMax = this._xMax;
        textOrObject._yOld = this._yOld;
        textOrObject._yMin = this._yMin;
        textOrObject._yMax = this._yMax;
        textOrObject.isLevelHighlight = this.isLevelHighlight;

        textOrObject.hPrevious = this;
        this.hNext.push(textOrObject);
      
        return textOrObject;
    }

    public shutOpen = (too: TextOrObject): boolean => {
        return (
            (this.isFloat === too.isFloat)
            && (
                (this.isShut && too.isOpen)
                || (this.isOpen && too.isShut)
            )
        );
    }

    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value.toLowerCase();
    }

    public isAtSamePlace = (too: TextOrObject): boolean => {
        if(too.isGlobalLevel){
            return true;
        }
        return this.isAtPlace(too.x, too.y);
    }

    public isAtPlace = (x: number, y: number): boolean => {
        if(this.isGlobalLevel){
            return true;
        }
        return this.x === x && this.y === y;
    }

    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        if(value < this._xMin){
            this._xMin = value;
        }
        if(value > this._xMax){
            this._xMax = value;
        }
        this._x = value;
    }

    public get tmpX(): number {
        return this._xMax;
    }
    public get tmpY(): number {
        return this._yMax;
    }

    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        if(value < this._yMin){
            this._yMin = value;
        }
        if(value > this._yMax){
            this._yMax = value;
        }
        this._y = value;
    }
    
    public get isLevelHighlight(): boolean {
        return this._isLevelHighlight;
    }
    public set isLevelHighlight(value: boolean) {
        this._isLevelHighlight = value;
    }
    
    public get isObject(): boolean {
        return this._isObject;
    }
    public set isObject(value: boolean) {
        this._isObject = value;
    }


    public get isWord(): boolean {
        return this._isWord;
    }
    public set isWord(value: boolean) {
        this._isWord = value;
    }

    public get moveDirection(): number {
        return this._moveDirection;
    }
    public set moveDirection(value: number) {
        this._moveDirection = value;
    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }

    public get levelPath(): string | null {
        return this._levelPath;
    }
    public set levelPath(value: string | null) {
        this._levelPath = value;
    }

    public get becomeLevelPath(): string | null {
        return this._becomeLevelPath;
    }
    public set becomeLevelPath(value: string | null) {
        this._becomeLevelPath = value;
    }

    public get viewState(): number {
        return this._viewState;
    }
    public set viewState(value: number) {
        this._viewState = value;
    }

    public get rules(): Rule[] {
        return this._rules;
    }
    public set rules(value: Rule[]) {
        this._rules = value;
    }

    public get isStop() {
        return this._isStop;
    }
    public set isStop(value) {
        this._isStop = value;
    }

    public get isPush() {
        return this._isPush;
    }
    public set isPush(value) {
        this._isPush = value;
    }

    public get isYou() {
        return this._isYou;
    }
    public set isYou(value) {
        this._isYou = value;
    }

    public get isWin(): boolean {
        return this._isWin;
    }
    public set isWin(value: boolean) {
        this._isWin = value;
    }

    public get isHot(): boolean {
        return this._isHot;
    }
    public set isHot(value: boolean) {
        this._isHot = value;
    }

    public get isMelt(): boolean {
        return this._isMelt;
    }
    public set isMelt(value: boolean) {
        this._isMelt = value;
    }

    public get isDefeat(): boolean {
        return this._isDefeat;
    }
    public set isDefeat(value: boolean) {
        this._isDefeat = value;
    }

    public get isSink(): boolean {
        return this._isSink;
    }
    public set isSink(value: boolean) {
        this._isSink = value;
    }

    public get nbMoves(): number {
        return this._nbMoves;
    }
    public set nbMoves(value: number) {
        this._nbMoves = value;
    }

    public get isOpen(): boolean {
        return this._isOpen;
    }
    public set isOpen(value: boolean) {
        this._isOpen = value;
    }

    public get isShut(): boolean {
        return this._isShut;
    }
    public set isShut(value: boolean) {
        this._isShut = value;
    }

    public get isFloat(): boolean {
        return this._isFloat;
    }
    public set isFloat(value: boolean) {
        this._isFloat = value;
    }

    public get isWeak(): boolean {
        return this._isWeak;
    }
    public set isWeak(value: boolean) {
        this._isWeak = value;
    }

    public get isSelfDestroy(): boolean {
        return this._isSelfDestroy;
    }
    public set isSelfDestroy(value: boolean) {
        this._isSelfDestroy = value;
    }

    public get disableWeak(): boolean {
        return this._disableWeak;
    }
    public set disableWeak(value: boolean) {
        this._disableWeak = value;
    }

    public get isTele(): boolean {
        return this._isTele;
    }
    public set isTele(value: boolean) {
        this._isTele = value;
    }

    public get isPull(): boolean {
        return this._isPull;
    }
    public set isPull(value: boolean) {
        this._isPull = value;
    }

    public get nbShift(): number {
        return this._nbShift;
    }
    public set nbShift(value: number) {
        this._nbShift = value;
    }

    public get nbFallD(): number {
        return this._nbFallD;
    }
    public set nbFallD(value: number) {
        this._nbFallD = value;
    }

    public get nbFallR(): number {
        return this._nbFallR;
    }
    public set nbFallR(value: number) {
        this._nbFallR = value;
    }

    public get nbFallU(): number {
        return this._nbFallU;
    }
    public set nbFallU(value: number) {
        this._nbFallU = value;
    }

    public get nbFallL(): number {
        return this._nbFallL;
    }
    public set nbFallL(value: number) {
        this._nbFallL = value;
    }

    public get isSleep(): boolean {
        return this._isSleep;
    }
    public set isSleep(value: boolean) {
        this._isSleep = value;
    }
    
    public get isGlobalLevel(): boolean {
        return this._isGlobalLevel;
    }
    public set isGlobalLevel(value: boolean) {
        this._isGlobalLevel = value;
    }

    public get isLevel(): boolean {
        return this._isLevel;
    }
    public set isLevel(value: boolean) {
        this._isLevel = value;
    }

    public get changeMoveDirection(): number|null {
        return this._changeMoveDirection;
    }
    public set changeMoveDirection(value: number|null) {
        this._changeMoveDirection = value;
    }

    public get isSwap(): boolean {
        return this._isSwap;
    }
    public set isSwap(value: boolean) {
        this._isSwap = value;
    }

    public get isMore(): boolean {
        return this._isMore;
    }
    public set isMore(value: boolean) {
        this._isMore = value;
    }

    public get nbGroup(): number {
        return this._nbGroup;
    }
    public set nbGroup(value: number) {
        this._nbGroup = value;
    }

    public get isSelect(): boolean {
        return this._isSelect;
    }
    public set isSelect(value: boolean) {
        this._isSelect = value;
    }

    public get isPath(): boolean {
        return this._isPath;
    }
    public set isPath(value: boolean) {
        this._isPath = value;
    }

    public get isBonus(): boolean {
        return this._isBonus;
    }
    public set isBonus(value: boolean) {
        this._isBonus = value;
    }

    public get src(): string|null {
        return this._src;
    }
    public set src(value: string|null) {
        this._src = value;
    }

    public get text(): string | null {
        return this._text;
    }
    public set text(value: string | null) {
        this._text = value;
    }
    
    public get priority(): number {
        return this._priority;
    }
    public set priority(value: number) {
        this._priority = value;
    }

    // history
    public get hPrevious(): TextOrObject | null {
        return this._hPrevious;
    }
    public set hPrevious(value: TextOrObject | null) {
        this._hPrevious = value;
    }

    public get hNext(): TextOrObject[] {
        return this._hNext;
    }
    public set hNext(value: TextOrObject[]) {
        this._hNext = value;
    }

    public get hIsDestroy(): boolean {
        return this._hIsDestroy;
    }
    public set hIsDestroy(value: boolean) {
        this._hIsDestroy = value;
    }   
}

