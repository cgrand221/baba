import { Action } from "./Action";
import { All } from "./Rules/All";
import { Empty } from "./Rules/Empty";
import { Group } from "./Rules/Group";
import { RLevel } from "./Rules/RLevel";
import { Source } from "./Source";

export class Level {
    private _objectNames: string[];
    private _textNames: string[];
    private _parent: string|null;
    private _nbRequiredSpores: number | null;
    private _nbSporesZoneCompleted: number | null;
    private _noCheckNbSpores: string[];
    

    private _nextLevelPath: string | null;
    
    constructor(
        _objectNames: string[], 
        _textNames: string[], 
        private _actions: Action[],
        private _sources: Source[], 
        private _file: string, 
        private _width:number, 
        private _height:number
    ){
        this._objectNames = _objectNames.concat([Empty.EMPTY, All.ALL, Group.GROUP, RLevel.LEVEL]);
        this._textNames = _textNames;
        this._parent = null;
        this._nbRequiredSpores = null;
        this._nbSporesZoneCompleted = null;
        this._noCheckNbSpores = [];

        this._nextLevelPath = null;
    }

    public get actions(): Action[] {
        return this._actions;
    }
    public set actions(value: Action[]) {
        this._actions = value;
    }

    public get width(): number {
        return this._width;
    }
    public set width(value: number) {
        this._width = value;
    }

    public get height(): number {
        return this._height;
    }
    public set height(value: number) {
        this._height = value;
    }

    public get parent(): string|null {
        return this._parent;
    }
    public set parent(value: string|null) {
        this._parent = value;
    }

    public get nbRequiredSpores(): number | null {
        return this._nbRequiredSpores;
    }
    public set nbRequiredSpores(value: number | null) {
        this._nbRequiredSpores = value;
    }

    public get nbSporesZoneCompleted(): number | null {
        return this._nbSporesZoneCompleted;
    }
    public set nbSporesZoneCompleted(value: number | null) {
        this._nbSporesZoneCompleted = value;
    }

    public get noCheckNbSpores(): string[] {
        return this._noCheckNbSpores;
    }
    public set noCheckNbSpores(value: string[]) {
        this._noCheckNbSpores = value;
    }

    public get nextLevelPath(): string | null {
        return this._nextLevelPath;
    }
    public set nextLevelPath(value: string | null) {
        this._nextLevelPath = value;
    }

    public get file(): string {
        return this._file;
    }
    public set file(value: string) {
        this._file = value;
    }



    public get sources(): Source[] {
        return this._sources;
    }
    public set sources(value: Source[]) {
        this._sources = value;
    }

    public get objectNames(): string[] {
        return this._objectNames;
    }
    public set objectNames(value: string[]) {
        this._objectNames = value;
    }


    public get textNames(): string[] {
        return this._textNames;
    }
    public set textNames(value: string[]) {
        this._textNames = value;
    }

}