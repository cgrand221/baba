import { KeyCode } from "./KeyCode";

export class Action {
    constructor(
        private _keyOrNumberSolutions: (KeyCode | number)[],
        private _doAction: number,
        private _rules: Set<string>,
        private _requirements: [string, number][],
    ){
    }
    
    public get doAction(): number {
        return this._doAction;
    }
    public set doAction(value: number) {
        this._doAction = value;
    }

    public get keyOrNumberSolutions(): (KeyCode | number)[] {
        return this._keyOrNumberSolutions;
    }
    public set keyOrNumberSolutions(value: (KeyCode | number)[]) {
        this._keyOrNumberSolutions = value;
    }

    public get rules(): Set<string> {
        return this._rules;
    }

    public get requirements(): [string, number][] {
        return this._requirements;
    }
    public set requirements(value: [string, number][]) {
        this._requirements = value;
    }
}