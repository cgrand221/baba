import { LeftRightBaseRule } from "./LeftRightBaseRule";
import { EqualsAll } from "./Rules/EqualsAll";

export class ConditionRule{
    
   
    constructor(
        private _nbNot: number,
        private _condition: string,
        private _leftRightBaseRules: LeftRightBaseRule[],
        private _start: number,
        private _end: number,
    ){
    }

    public get nbNot(): number {
        return this._nbNot;
    }
    public set nbNot(value: number) {
        this._nbNot = value;
    }

    public get not(): boolean {
        return this._nbNot % 2 === 1;
    }

    public get condition(): string {
        return this._condition;
    }
    public set condition(value: string) {
        this._condition = value;
    }

    public get leftRightBaseRules(): LeftRightBaseRule[] {
        return this._leftRightBaseRules;
    }
    public set leftRightBaseRules(value: LeftRightBaseRule[]) {
        this._leftRightBaseRules = value;
    }

    public get start(): number {
        return this._start;
    }
    public set start(value: number) {
        this._start = value;
    }

    public get end(): number {
        return this._end;
    }
    public set end(value: number) {
        this._end = value;
    }

    public toString = ():string => {
        let nots:string[]=[];
        for (let i = 0; i < this.nbNot;i++){
            nots.push('not');
        }
        return (
            nots.join(' ')
            + this.condition
            + ' ' + this.leftRightBaseRules.map(lrbr => lrbr.toString()).join(' ')
        );
    }

    public equals = (conditionRule:ConditionRule):boolean => {
        return (
            this.nbNot === conditionRule.nbNot
            && this.condition === conditionRule.condition
            && LeftRightBaseRule.equalsAll(this.leftRightBaseRules, conditionRule.leftRightBaseRules)
            && this.start === conditionRule.start
            && this.end === conditionRule.end
        );
    }

    public static equalsAll = EqualsAll.equalsAll<ConditionRule>;
}