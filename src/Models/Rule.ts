
import { ConditionRule } from "./ConditionRule";
import { LeftRightBaseRule } from "./LeftRightBaseRule";
import { EqualsAll } from "./Rules/EqualsAll";

export class Rule{
    private _nbNotLonely: number;
    private _lonely: boolean;
    
    constructor(
        private _start: number,
        private _lefts: LeftRightBaseRule[],
        private _conditions: ConditionRule[],
        private _verb: string,
        private _rights: LeftRightBaseRule[],
    ){
        this._nbNotLonely = 0;
        this._lonely = false;
    }
   
    public get lefts(): LeftRightBaseRule[] {
        return this._lefts;
    }
    public set lefts(value: LeftRightBaseRule[]) {
        this._lefts = value;
    }

    public get conditions(): ConditionRule[] {
        return this._conditions;
    }
    public set conditions(value: ConditionRule[]) {
        this._conditions = value;
    }

    public get verb(): string {
        return this._verb;
    }
    public set verb(value: string) {
        this._verb = value;
    }

    public get rights(): LeftRightBaseRule[] {
        return this._rights;
    }
    public set rights(value: LeftRightBaseRule[]) {
        this._rights = value;
    }

    public equals = (rule:Rule):boolean => {
        return (
            LeftRightBaseRule.equalsAll(this.lefts, rule.lefts)
            && ConditionRule.equalsAll(this.conditions, rule.conditions)
            && this.verb === rule.verb
            && LeftRightBaseRule.equalsAll(this.rights, rule.rights)
            && this.nbNotLonely === rule.nbNotLonely
            && this.lonely === rule.lonely
            && this.start === rule.start
        );
    }

    public calcAllRightsDisabled = (): boolean => {
        return (
            this.rights.every(right => !right.not) 
            && (
                // number of all forbidden right does not cover all left
                this.lefts.map(
                    left => left.disableRights.length
                ).reduce(
                    (accumulator, length) => accumulator+length, 
                    0
                ) === this.lefts.length * this.rights.length
            )
        )
    }

    public resetDisabled = () => {
        this.lefts.forEach(left => {
            left.resetDisabled();
        });
    }

    public get nbNotLonely(): number {
        return this._nbNotLonely;
    }
    public set nbNotLonely(value: number) {
        this._nbNotLonely = value;
    }


    public get notLonely(): boolean {
        return this._nbNotLonely % 2 === 1;
    }
    
    public get lonely(): boolean {
        return this._lonely;
    }
    public set lonely(value: boolean) {
        this._lonely = value;
    }

    public get start(): number {
        return this._start;
    }
    public set start(value: number) {
        this._start = value;
    }
    
    public toString = ():string => {
        return (
            this.lefts.map(left => left.toString()).join(' ')
            + (this.conditions.length > 0 ? ' ' : '') 
            + this.conditions.map(condition => condition.toString()).join(' ')
            + ' ' + this.verb + ' '
            + this.rights.map(right => right.toString()).join(' ')
        );
    }

    public static equalsAll = EqualsAll.equalsAll<Rule>;
}