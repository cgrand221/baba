
import { Level } from "./Level";
import { Quality } from "./Rules/Quality";
import { Text } from "./Rules/Text";
import { TextOrObject } from "./TextOrObject";
import { Verb } from "./Rules/Verb";
import { And } from "./Rules/And";
import { Not } from "./Rules/Not";
import { Condition } from "./Rules/Condition";
import { Lonely } from "./Rules/Lonely";
import { Empty } from "./Rules/Empty";
import { All } from "./Rules/All";
import { KeyCode } from "./KeyCode";
import { Group } from "./Rules/Group";
import { IndexGrid } from "./IndexGrid";
import { Source } from "./Source";
import { RLevel } from "./Rules/RLevel";
//
export class LevelState {

    private _level: Level;
    private _textOrObjects: TextOrObject[];
    private _state: number;
    private _parentSavedRules: Set<string>;
    private _savedRules: Set<string>;
    private _withEmptys: boolean;
    private _keyCode: KeyCode;
    private _seed: number;;
    private _indeGrid: IndexGrid;
    private _parentLevelState: LevelState | null;
    
   

    public static STATE_CAN_PLAY = 1;
    public static STATE_CAN_NOT_PLAY = 2;
    public static STATE_IS_WIN = 3;
    public static STATE_IS_INFINITE_LOOP = 4;
    public static STATE_IS_ZONE_COMPLETED = 5;
    public static STATE_IS_NEW_PARENT_RULES = 6;
    public static STATE_IS_BONUS = 7;

    public clone = (): LevelState => {
        let levelState: LevelState = new LevelState(this._level, false);
        this._textOrObjects.forEach(too => {
            too.hNext = [];
            levelState.addTextOrObject(too.clone());
        });
        levelState._seed = this._seed;
        levelState._parentLevelState = this._parentLevelState;
        return levelState;
    }

    public constructor(level: Level, needLevelToo: boolean=true) {
        this._keyCode = KeyCode.KEY_SPACE;
        this._level = level;
        this._textOrObjects = [];
        if(needLevelToo){
            let too = new TextOrObject(RLevel.LEVEL, true, 0, 0);
            too.isGlobalLevel = true;
            this._textOrObjects.push(too);
        }
        this._state = LevelState.STATE_CAN_PLAY;
        this._savedRules = new Set<string>();
        this._parentSavedRules = new Set<string>();
        this._withEmptys = false;
        this._seed = 0x2F6E2B1;
        this._indeGrid = new IndexGrid(this);
        this._parentLevelState = null;
    }

    public resetIndex = () => {
        this._indeGrid = new IndexGrid(this);
    }

    public get level(): Level {
        return this._level;
    }
    public set level(value: Level) {
        this._level = value;
    }

    public addTextOrObject = (...textOrObjects: TextOrObject[]) => {
        textOrObjects.forEach(textOrObject => {
            if (!this._indeGrid.has(textOrObject)) {
                this._textOrObjects.push(textOrObject);
                this._indeGrid.push(textOrObject);
            }
        });
    }

    public removeTextOrObject = (textOrObject: TextOrObject) => {
        let index = this._textOrObjects.indexOf(textOrObject)
        if (index !== -1) {
            this._textOrObjects.splice(index, 1);
            this._indeGrid.remove(textOrObject);
        }

    }

    public applyChangeXY = (
        textOrObject: TextOrObject,
        fn: () => void
    ) => {
        let too = this._indeGrid.remove(textOrObject);
        fn();
        if (too) {
            this._indeGrid.push(textOrObject);
        }
    }

    public get textOrObjects(): TextOrObject[] {
        return this._textOrObjects;
    }

    public set textOrObjects(textOrObjects: TextOrObject[]) {
        this._textOrObjects = textOrObjects;
    }

    public calcLevelSelect() : TextOrObject|null{
        let select = this.textOrObjects.find(too => too.isSelect);
        if(undefined === select){
            return null;
        }
        return select;
    }
    public calcLevelPathOfSelect(select: TextOrObject) : string|null{
        let levelPath: string|null = null;
        let level = this.getTextAndObjectsAt(select.x, select.y).find(too => null !== too.levelPath && !too.isSelect);
        if (undefined !== level){
            levelPath = level.levelPath;
        }
        return levelPath;
    }

    public getTextAndObjectsAt = (x: number, y: number): TextOrObject[] => {

        return this._indeGrid.at(x, y);
        return this.textOrObjects.filter(textOrObject =>
            (
                textOrObject.x === x
                && textOrObject.y === y
            ) || textOrObject.isGlobalLevel
        )
    }

    public getCalcHasGlobalLevelTransformed = (): boolean => {
        return this.textOrObjects.some(too => too.isGlobalLevel && too.name !== RLevel.LEVEL);
    }

    public getWordsObjectToMatch = (): string[] => {
        return this._level.objectNames;
    }

    public getWordsRealObjectToMatch = (): string[] => {
        return this.getWordsObjectToMatch().filter(lc => lc !== Empty.EMPTY && lc !== All.ALL && lc !== Group.GROUP && lc !== RLevel.LEVEL);
    }

    public getWordsToMatch = (): string[] => {
        let words = new Set<string>();
        this.getWordsObjectToMatch().forEach(word => {
            words.add(word);
        });
       
        Quality.qualities.forEach(quality => {
            words.add(quality);
        });
        Verb.verbs.forEach(verb => {
            words.add(verb);
        });
        Condition.conditions.forEach(condition => {
            words.add(condition);
        });
       
        words.add(Text.TEXT);
        words.add(And.AND);
        words.add(Not.NOT);
        words.add(Lonely.LONELY);
        return [...words.values()];
    }

    private randNumber = (): number => {
        // Robert Jenkins’ 32 bit integer hash function
        this._seed = ((this._seed + 0x7ED55D16) + (this._seed << 12)) & 0xFFFFFFFF;
        this._seed = ((this._seed ^ 0xC761C23C) ^ (this._seed >>> 19)) & 0xFFFFFFFF;
        this._seed = ((this._seed + 0x165667B1) + (this._seed << 5)) & 0xFFFFFFFF;
        this._seed = ((this._seed + 0xD3A2646C) ^ (this._seed << 9)) & 0xFFFFFFFF;
        this._seed = ((this._seed + 0xFD7046C5) + (this._seed << 3)) & 0xFFFFFFFF;
        this._seed = ((this._seed ^ 0xB55A4F09) ^ (this._seed >>> 16)) & 0xFFFFFFFF;
        return (this._seed & 0xFFFFFFF) / 0x10000000;

    }

    public startMovement = (): void => {
        this._textOrObjects.forEach(too => {
            too.startMovement();
        });
    }

    public endMovement = (): void => {
        this._textOrObjects.forEach(too => {
            too.endMovement();
        });
    }

    public randomValue = <T>(t: T[]): T | null => {
        return t[Math.floor(this.randNumber() * t.length)] ?? null;
    }

    public get state(): number {
        return this._state;
    }
    public set state(value: number) {
        this._state = value;
    }

    public get parentSavedRules(): Set<string> {
        return this._parentSavedRules;
    }

    public get savedRules(): Set<string> {
        return this._savedRules;
    }
    

    public get withEmptys(): boolean {
        return this._withEmptys;
    }
    public set withEmptys(value: boolean) {
        this._withEmptys = value;
    }

    public get keyCode(): KeyCode {
        return this._keyCode;
    }
    public set keyCode(value: KeyCode) {
        this._keyCode = value;
    }

    public get parentLevelState(): LevelState | null {
        return this._parentLevelState;
    }
    public set parentLevelState(value: LevelState | null) {
        this._parentLevelState = value;
    }

    private getUnsolved = (textOrObjects: TextOrObject[]): TextOrObject[] => {
        return textOrObjects.filter(textOrObject => null !== textOrObject.levelPath && textOrObject.viewState !== TextOrObject.VIEW_STATE_WIN && textOrObject.viewState !== TextOrObject.VIEW_STATE_HAS_REQUIRED_SPORES);
    }

    /**
     * remove unsolved elements from visibles
     */
    private removeUnsolved = (textOrObjects: TextOrObject[]): void => {
        let toRemoves: TextOrObject[] = [];
        textOrObjects.forEach(textOrObject => {
            let unsolveds = this.getUnsolved(
                this.getTextAndObjectsAt(textOrObject.x, textOrObject.y)
            )
            if (unsolveds.length > 0) {
                toRemoves.push(textOrObject);
            }
        });

        toRemoves.forEach(toRemove => {
            let pos = textOrObjects.indexOf(toRemove);
            if (pos !== -1) {
                textOrObjects.splice(pos, 1);
            }
        });
    }

    public propagateViewState = () => {
        let dxys = [
            [0, -1],
            [-1, 0],
            [0, 0],
            [+1, 0],
            [0, +1],
        ] as const;

        let visibles = this.textOrObjects.filter(too => {
            if (null === too.levelPath) {
                return too.viewState !== TextOrObject.VIEW_STATE_LOCKED && too.isPath;
            }
            return (
                too.viewState === TextOrObject.VIEW_STATE_WIN
                || too.viewState === TextOrObject.VIEW_STATE_HAS_REQUIRED_SPORES
            )
        });

        this.removeUnsolved(visibles);


        let change = true;
        while (change) {
            let toAdds: TextOrObject[] = [];
            visibles.forEach(visible => {
                let x = visible.x;
                let y = visible.y;
                dxys.forEach(dxy => {
                    let toos = this.getTextAndObjectsAt(x + dxy[0], y + dxy[1]);
                    let unsolveds = this.getUnsolved(toos);

                    let toChanges = toos.filter(too => {
                        if (null === too.levelPath) {
                            return too.viewState === TextOrObject.VIEW_STATE_LOCKED && too.isPath;// hidden path
                        }
                        return too.viewState === TextOrObject.VIEW_STATE_LOCKED// hidden level
                    });
                    toChanges.forEach(too => {
                        too.viewState = TextOrObject.VIEW_STATE_VISIBLE;
                    });

                    if (unsolveds.length === 0) {
                        toAdds.push(...toChanges);
                    }

                });
            });

            change = false;
            toAdds.forEach(toAdd => {
                if (!visibles.includes(toAdd)) {
                    change = true;
                    visibles.push(toAdd);
                }
            });


        }
    }

    public getSourceOrientation = (textOrObject: TextOrObject, source: Source): string | null => {
        if (!textOrObject.isObject) {
            return source.src
        }
        let keys = [
            'src', 'srcT', 'srcR', 'srcTr', 'srcB', 'srcTb', 'srcRb', 'srcTrb', 'srcL', 'srcTl', 'srcRl', 'srcTrl', 'srcBl', 'srcTbl', 'srcRbl', 'srcTrbl'
        ] as const;
        
        let aaa = 'srcT';
        let choice = 0;
        if (
            textOrObject.y == 0
            || this.getTextAndObjectsAt(textOrObject.x, textOrObject.y - 1).some(too => too.name === textOrObject.name && too.isObject)
        ) {
            choice += 1;//top
        }
        if (
            textOrObject.x+1 == this.level.width
            || this.getTextAndObjectsAt(textOrObject.x+1, textOrObject.y).some(too => too.name === textOrObject.name && too.isObject)
        ) {
            choice += 2;//right
        }
        if (
            textOrObject.y+1 == this.level.height
            || this.getTextAndObjectsAt(textOrObject.x, textOrObject.y+1).some(too => too.name === textOrObject.name && too.isObject)
        ) {
            choice += 4;//bottom
        }
        if (
            textOrObject.x == 0
            || this.getTextAndObjectsAt(textOrObject.x-1, textOrObject.y).some(too => too.name === textOrObject.name && too.isObject)
        ) {
            choice += 8;//left
        }
        
        let src = keys[choice];
        if(src){
            return source[src];
        }
        return source.src
    }
}