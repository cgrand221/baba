import { LevelState } from "./LevelState";
import { RLevel } from "./Rules/RLevel";
import { TextOrObject } from "./TextOrObject";

export class IndexGrid {
    private _width:number;
    private _height:number;
    private _textOrObjects:TextOrObject[][];
    private _levelToo:TextOrObject|null;

    public constructor(
        levelState:LevelState,
       
    ){
        this._width = levelState.level.width;
        this._height = levelState.level.height;
        this._textOrObjects = [];
        this._levelToo = null;
        levelState.textOrObjects.forEach(too => {
            this.push(too);
        });
    }

    private pos = (x: number, y:number) => {
        return y*this._width+x;
    }

    public at = (x:number, y:number): TextOrObject[] => {
        let toos = this._textOrObjects[this.pos(x, y)] ?? [];
        if(null !== this._levelToo){
            return [this._levelToo, ...toos];
        }
        return [...toos];
    }

    public check = (): void => {
        if(this._levelToo === null){
            throw Error('error !!!');
        }
    }

    public push = (too: TextOrObject) => {
        if (too.isGlobalLevel){
            this._levelToo = too;
            return;
        }
        let pos = this.pos(too.x, too.y);
        let toos = this._textOrObjects[pos] ?? [];
        toos.push(too);
        this._textOrObjects[pos] = toos;
    }

    public has = (textOrObject: TextOrObject): boolean => {
        return this.at(textOrObject.x, textOrObject.y).some(too => too === textOrObject);
    }

    public remove = (too: TextOrObject): TextOrObject|null => {
        if(too === this._levelToo){
            this._levelToo = null;
            return too;
        }
        let pos = this.pos(too.x, too.y);
        let toos = this._textOrObjects[pos] ?? [];
        if(toos.length === 0){
            return null;
        }
        let posRemove = toos.indexOf(too);
        if (posRemove === -1){
            return null;
        }
        toos.splice(posRemove, 1);
        this._textOrObjects[pos] = toos;
        return too;
    }
}