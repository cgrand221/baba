export class Source{

    constructor(
        public name: string,
        public isObject: boolean,
        public text: string | null,

        public src: string | null,
        public srcT: string | null,
        public srcR: string | null,
        public srcTr: string | null,
        public srcB: string | null,
        public srcTb: string | null,
        public srcRb: string | null,
        public srcTrb: string | null,
        public srcL: string | null,
        public srcTl: string | null,
        public srcRl: string | null,
        public srcTrl: string | null,
        public srcBl: string | null,
        public srcTbl: string | null,
        public srcRbl: string | null,
        public srcTrbl: string | null,


        public srcRight: string | null,
        public srcBottom: string | null,
        public srcLeft: string | null,
        public priority: number
    ){
    }

  // 'src', 'srcT', 'srcR', 'srcTr', 'srcB', 'srcTb', 'srcRb', 'srcTrb', 'srcL', 'srcTl', 'srcRl', 'srcTrl', 'srcBl', 'srcTbl', 'srcRbl', 'srcTrbl'
}