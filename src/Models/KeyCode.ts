export enum KeyCode {
   KEY_UP='^',
   KEY_RIGHT='>',
   KEY_DOWN='v',
   KEY_LEFT='<',
   KEY_SPACE='_',
   KEY_BACK='←',
   KEY_RESTART='R',
   KEY_ENTER='ENTER',
   KEY_SHIFT='SHIFT',
   KEY_A='A',
   KEY_B='B',
}

