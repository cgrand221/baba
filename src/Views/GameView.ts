import { Level } from "../Models/Level";
import { LevelState } from "../Models/LevelState";
import { TextOrObject } from "../Models/TextOrObject";
import { ConfigLoader } from "../Utils/ConfigLoader";
import { SaveManager } from "../Utils/SaveManager";

export class GameView {

    private static _instance: GameView | null = null;
    private _saveManager: SaveManager;
    private _configLoader: ConfigLoader;
    private _thicknessX: number = 0;
    private _thicknessY: number = 0;

    private _celWidth: number = 0;
    private _celHeight: number = 0;
    private _totalWidth: number = 0;
    private _totalHeight: number = 0;
    private _innerCelWidth: number = 0;
    private _innerCelHeight: number = 0;
    private canvas: HTMLCanvasElement;
    private _nbSpores: HTMLSpanElement;
    private _nbBonus: HTMLSpanElement;
    private _bonusCount: HTMLSpanElement;
    private _nbRequiredSpores: HTMLSpanElement;
    private _sporesCount: HTMLSpanElement;
    private ctx: CanvasRenderingContext2D;
    private _levelSrc:string|null;

    private level: Level;
    private _levelState: LevelState;
    private _textOrObjects: TextOrObject[];
    private _previousTextOrObjects: TextOrObject[];
    private _nbImages = 0;
    private _nbLoad = 0;
    private _nbFrameBeforeMessage = 0;
    private _hasIntervall = false;
    private _posFloat = 0;
    private _cptAnimation = 0;
    private _floatAsc = true;
    private _angle = 0;
    

    static NB_FRAME_BEFORE_MESSAGE = 80;
    static ANIMATION_LIMIT = 8;
    static FLOAT_LIMIT = 80;

    private constructor() {
        this._configLoader = ConfigLoader.instance;
        let canvas = document.querySelector<HTMLCanvasElement>("#myCanvas");
        let nbSpores = document.querySelector<HTMLCanvasElement>("#nbSpores");
        let nbRequiredSpores = document.querySelector<HTMLCanvasElement>("#nbRequiredSpores");
        let sporesCount = document.querySelector<HTMLCanvasElement>("#spores-count");
        let nbBonus = document.querySelector<HTMLCanvasElement>("#nbBonus");
        let bonusCount = document.querySelector<HTMLCanvasElement>("#bonus-count");

        let ctx = canvas?.getContext("2d");
        this._levelState = new LevelState(new Level([], [], [], [], '', 0, 0));
        this._textOrObjects = [];
        this._previousTextOrObjects = [];
        this._saveManager = SaveManager.instance;
        this._levelSrc = null;
        if (canvas && ctx && nbSpores && nbRequiredSpores && sporesCount && nbBonus && bonusCount) {
            this.canvas = canvas;
            this._nbSpores = nbSpores;
            this._nbRequiredSpores = nbRequiredSpores;
            this._sporesCount = sporesCount;
            this._nbBonus = nbBonus;
            this._bonusCount = bonusCount;
            this.ctx = ctx;
            this.level = new Level([], [], [], [], "", 0, 0);
            this.cacheImg = {};
            this._nbImages = 0;
            this._nbLoad = 0;
            this._angle = 0;
        }
        else {
            throw new Error('can not find canvas or context2D or nbSpores or nbRequiredSpores or sporesCount');
        }
    }

    public static get instance(): GameView {
        if (!GameView._instance) {
            GameView._instance = new GameView();
        }
        return GameView._instance;
    }

    public init = (level: Level) => {
        this.level = level;
        this._thicknessX = 1;
        this._thicknessY = 1;

        let canvasWidth = document.body.clientWidth;
        let canvasHeight = document.body.clientHeight - 150;

        this._celWidth = Math.floor((canvasWidth - this._thicknessX) / level.width);
        this._celHeight = Math.floor((canvasHeight - this._thicknessY) / level.height);

        if (this._celHeight > this._celWidth) {
            this._celHeight = this._celWidth;
        }
        else {
            this._celWidth = this._celHeight;
        }


        this._innerCelWidth = this._celWidth - this._thicknessX;
        this._innerCelHeight = this._celHeight - this._thicknessY

        this._totalWidth = this._celWidth * level.width + this._thicknessX;
        this._totalHeight = this._celHeight * level.height + this._thicknessY;

        this.canvas.width = this._totalWidth;
        this.canvas.height = this._totalHeight;
        this.canvas.style.width = this._totalWidth + 'px'
        this.canvas.style.height = this._totalHeight + 'px'
        this._posFloat = 0;
        this._floatAsc = true;
        this._angle = 0;
    }

    private whitePage = () => {
        this.ctx.fillStyle = "#ffffff";
        this.ctx.globalAlpha = 1;
        this.ctx.fillRect(0, 0, this._totalWidth, this._totalHeight);
    }

    private drawGrid = (x: number, y: number) => {
        this.ctx.fillStyle = "#000000";
        this.ctx.globalAlpha = 1;
        let oldX = x;
        for (let i = 0; i <= this.level.width; i++) {
            this.ctx.fillRect(x, y, this._thicknessX, this._totalHeight);
            x += this._celWidth
        }
        x = oldX;
        for (let j = 0; j <= this.level.height; j++) {
            this.ctx.fillRect(x, y, this._totalWidth, this._thicknessY);
            y += this._celHeight
        }

    }

    private drawText = (content: string, contentColor: string, backgroungColor: string | null, x: number, y: number) => {
        this.ctx.font = "48px serif";
        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "middle";

        if (backgroungColor) {
            this.ctx.fillStyle = backgroungColor;
            this.ctx.globalAlpha = 1;
            let textSize = this.ctx.measureText(content);
            this.ctx.fillRect(x - textSize.width / 2 - 20, y - 48 / 2 - 10, textSize.width + 40, 48 + 20);
        }

        this.ctx.fillStyle = contentColor;
        this.ctx.globalAlpha = 1;
        this.ctx.fillText(content, x, y);
    }

    private drawLevelText = (content: string, viewState: number, x: number, y: number) => {
        let fontSize = (this._celWidth/content.length);

        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "middle";
        this.ctx.font = fontSize + "px serif";

        this.ctx.fillStyle = "#ffffff";
        if (
            viewState === TextOrObject.VIEW_STATE_WIN
            || viewState === TextOrObject.VIEW_STATE_HAS_REQUIRED_SPORES
        ) {
            this.ctx.fillStyle = "#a0a0a0";
        }
        this.ctx.globalAlpha = 1;
        this.ctx.fillText(content, x+this._celWidth/2, y+this._celHeight/2);
    }

    private resetRotation = () => {
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    }

    private changeRotation = (moveDirection: number, ratioAnimation: number) => {
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);

        let angle = 0;
        if (moveDirection === TextOrObject.LEFT) {
            angle = Math.PI / 2;
        }
        else if (moveDirection === TextOrObject.RIGHT) {
            angle = -Math.PI / 2;
        }
        else if (moveDirection === TextOrObject.UP) {
            angle = Math.PI;
        }
        else if (moveDirection === TextOrObject.DOWN) {
            angle = 0;
        }

        if (ratioAnimation === 1) {
            this._angle = angle;
        }

        let w = this._totalWidth / 2;
        let h = this._totalHeight / 2;

        this.ctx.translate(w, h);

        this.ctx.rotate(this._angle + (angle - this._angle) * ratioAnimation);
        this.ctx.translate(-w, -h);

    }

    private calcMoveDirection = () => {
        let levelToo = this._textOrObjects.find(too => too.isGlobalLevel) ?? null;
        let moveDirection = TextOrObject.DOWN;
        if (null !== levelToo) {
            moveDirection = levelToo.moveDirection;
        }
        return moveDirection;
    }

    private cacheImg: Record<string, HTMLImageElement>
    private repaint = () => {
        this.whitePage();
        let moveDirection = this.calcMoveDirection();

        let animate = false;
        let toos = this._textOrObjects;
        let ratioAnimation = 1;
        if (
            this._previousTextOrObjects.length > 0
            && this._cptAnimation <= GameView.ANIMATION_LIMIT
        ) {
            animate = true;
            ratioAnimation = this._cptAnimation / GameView.ANIMATION_LIMIT;
            toos = this._previousTextOrObjects;
        }

        let levelToo = toos.find(too => too.isGlobalLevel) ?? null;
        let levelX = 0;
        let levelY = 0;
        if (null !== levelToo) {
            levelX = levelToo.x;
            levelY = levelToo.y;
        }


        toos.forEach(textOrObject => {
            this.ctx.globalAlpha = 1;
            let srcs:string[] = [];
            let src = textOrObject.src;
            if(
                null !== src 
                && (
                    textOrObject.levelPath === null
                    || textOrObject.viewState !== TextOrObject.VIEW_STATE_LOCKED
                )
            ){
                srcs.push(src);
            }
            let text = textOrObject.text;
            let viewState = textOrObject.viewState;

            if (
                viewState === TextOrObject.VIEW_STATE_LOCKED
                && null === textOrObject.levelPath
            ) {
               return;
            }

            let tooX = textOrObject.x;
            let tooY = textOrObject.y;

            if (!textOrObject.isGlobalLevel) {
                tooX += levelX;
                tooY += levelY;
            }


            let x: number = (tooX) * this._celWidth;
            let y: number = (tooY) * this._celHeight;

            let hNext = textOrObject.hNext[0];
            let levelTooHNext = levelToo ? levelToo.hNext[0] : null;
            if (animate && hNext) {


                let hNextx = hNext.x;
                let hNexty = hNext.y;
                if (ratioAnimation < 0.5) {
                    hNextx = hNext.tmpX;
                    hNexty = hNext.tmpY;
                }
                if (levelTooHNext && !textOrObject.isGlobalLevel) {
                    hNextx += levelTooHNext.x;
                    hNexty += levelTooHNext.y;
                }

                x += Math.floor((hNextx - tooX) * this._celWidth * ratioAnimation);
                y += Math.floor((hNexty - tooY) * this._celHeight * ratioAnimation);

                if (hNext.hIsDestroy) {
                    this.ctx.globalAlpha = 1 - ratioAnimation
                }
            }


            if (textOrObject.isFloat) {
                y -= Math.trunc(this._posFloat / 10);
            }


            if (!textOrObject.isGlobalLevel) {
                x += this._thicknessX;
                y += this._thicknessY;
            }

            if (textOrObject.isGlobalLevel) {
                this.resetRotation();
                this.drawGrid(x, y);
                return;
            }

            if(
                null !== textOrObject.levelPath
                && !textOrObject.isLevelHighlight
                && null !== this._levelSrc
            ){
                srcs.push(this._levelSrc);
                src = this._levelSrc
            }

            srcs.reverse().forEach(src =>  {
                this.changeRotation(moveDirection, ratioAnimation);
                let image = this.cacheImg[src];
                if (!image) {
                    let img = new Image();
                    img.src = src;
                    if (img) {
                        this.cacheImg[src] = img;
                        this._nbImages++;
                        img.onload = () => {
                            this._nbLoad++;
                            if (this._nbImages <= this._nbLoad) {
                                this.repaint();
                            }
                        };
                    }
                }
                else {
                    try {
                        this.ctx.drawImage(
                            image,
                            x,
                            y,
                            this._innerCelWidth,
                            this._innerCelHeight
                        );
                    }
                    catch (e) {
                        console.log('can not draw ' + src);
                    }
                }
            });

            if (text && textOrObject.viewState !== TextOrObject.VIEW_STATE_LOCKED) {
                
                this.drawLevelText(text, viewState, x, y);
            }


            if(!textOrObject.isGlobalLevel){
                if (textOrObject.rules.length > 0) {
                    this.ctx.fillStyle = "#ff0000";
                    this.ctx.globalAlpha = 1;
                    this.ctx.fillRect(x, y, 2, this._innerCelHeight);
                    this.ctx.fillRect(x + this._innerCelWidth - 2, y, 2, this._innerCelHeight);
    
                    this.ctx.fillRect(x, y, this._innerCelWidth, 2);
                    this.ctx.fillRect(x, y + this._innerCelHeight - 2, this._innerCelWidth, 2);
    
                    let disabledRules = textOrObject.rules.filter(rule => rule.calcAllRightsDisabled());
                    if (disabledRules.length === textOrObject.rules.length) {
    
                        this.ctx.beginPath();
                        this.ctx.strokeStyle = '#ff0000';
                        this.ctx.lineWidth = 5;
                        this.ctx.stroke
                        this.ctx.moveTo(x, y);
                        this.ctx.lineTo(x + this._celWidth, y + this._celHeight);
                        this.ctx.stroke();
    
                        this.ctx.beginPath();
                        this.ctx.moveTo(x + this._celWidth, y);
                        this.ctx.lineTo(x, y + this._celHeight);
                        this.ctx.stroke();
                    }
                }
                if (textOrObject.isLevelHighlight) {
                    this.ctx.fillStyle = "#00ff00";
                    this.ctx.globalAlpha = 0.5;
                    this.ctx.fillRect(x, y, this._innerCelWidth, this._innerCelHeight);
                }
            }
            if (textOrObject.isWin) {
                this.ctx.fillStyle = "#ffff00";
                this.ctx.globalAlpha = 0.5;
                if (textOrObject.isGlobalLevel) {
                    this.ctx.fillRect(0, 0, this._totalWidth, this._totalHeight);
                }
                else {
                    this.ctx.fillRect(x, y, this._innerCelWidth, this._innerCelHeight);
                }
            }
            
            if (animate && hNext && hNext.hIsDestroy) {
                this.drawDestroy(x, y, ratioAnimation);
            }
        });

        this.resetRotation();
        if (this._levelState.state === LevelState.STATE_IS_WIN) {
            this.drawText("Congratulations", "#000000", "#00ff00", this._totalWidth / 2, this._totalHeight / 2);
        }
        else if (
            (
                this._nbFrameBeforeMessage >= GameView.NB_FRAME_BEFORE_MESSAGE
                && this._levelState.state === LevelState.STATE_CAN_NOT_PLAY
            ) || (
                this._levelState.state === LevelState.STATE_IS_INFINITE_LOOP
            )
        ) {
            this.drawText("r : restart", "#000000", "#aaaaaa", this._totalWidth / 6, this._totalHeight / 8);
            this.drawText("backspace : cancel", "#000000", "#aaaaaa", this._totalWidth * 3 / 6, this._totalHeight / 8);
            this.drawText("space : wait", "#000000", "#aaaaaa", this._totalWidth * 5 / 6, this._totalHeight / 8);

            if (this._levelState.state === LevelState.STATE_IS_INFINITE_LOOP) {
                this.drawText("Infinite loop", "#000000", "#ff0000", this._totalWidth / 2, this._totalHeight / 2);
            }
        }


    }


    private drawDestroy = (xP: number, yP: number, ratioAnimation: number) => {
        this.ctx.fillStyle = "#ff0000";
        this.ctx.globalAlpha = 1;

        // this._cptAnimation
        let xRect = this._celWidth / 10;
        let yRect = this._celHeight / 10;

        let dx = xRect * this._cptAnimation;
        let dy = yRect * this._cptAnimation;

        let x1 = xP - dx;
        let x2 = xP + this._celWidth + dx;
        let xStep = Math.floor((x2 - x1) / 5);
        let y1 = yP - dy;
        let y2 = yP + this._celHeight + dy;
        let yStep = Math.floor((y2 - y1) / 5);
        let alphas = [0, 0.25, 0.5, 0.25, 0];
        let i2 = 0;
        for (let i = x1; i < x2; i += xStep) {
            let alpha = alphas[i2] ?? 0;
            let j2 = 0;
            for (let j = y1; j < y2; j += yStep) {
                this.ctx.globalAlpha = alpha + (alphas[j2] ?? 0);
                this.ctx.fillRect(i, j, xRect, yRect);
                j2++;
            }
            i2++;
        }
    }

    private animationRepaint = () => {
        if (this._levelState.state !== LevelState.STATE_CAN_NOT_PLAY) {
            this._nbFrameBeforeMessage = 0;
        }
        else if (this._nbFrameBeforeMessage < GameView.NB_FRAME_BEFORE_MESSAGE) {
            this._nbFrameBeforeMessage++;
        }

        let repaint = false;
        if (
            this._previousTextOrObjects.length > 0
            && this._cptAnimation <= GameView.ANIMATION_LIMIT
        ) {
            this._cptAnimation++;
            repaint = true;
        }

        if (this._floatAsc) {
            this._posFloat += 1;
            if (this._posFloat >= GameView.FLOAT_LIMIT) {
                this._floatAsc = false;
            }
        }
        else {
            this._posFloat -= 1;
            if (this._posFloat <= 0) {
                this._floatAsc = true;
            }
        }
        if (this._posFloat % 10 === 0) {
            repaint = true;
        }
        if (repaint) {
            window.requestAnimationFrame(this.repaint);
        }

    }

    public view = (previousLevelState: LevelState | null, levelState: LevelState, forceReload: boolean) => {
        if (
            previousLevelState !== levelState
            || forceReload
        ) {

            let levelSave = this._saveManager.getLevelSave(levelState);
            let nbRequiredSpores = levelState.level.nbRequiredSpores;

            if (null !== nbRequiredSpores) {
                this._sporesCount.classList.remove('hidden');
                if (null !== levelSave) {
                    this._nbSpores.innerText = levelSave._nbSpores + '';
                }
                else {
                    this._nbSpores.innerText = '0';
                }
                this._nbRequiredSpores.innerText = levelState.level.nbRequiredSpores + '';
            }
            else {
                this._sporesCount.classList.add('hidden');
                this._nbSpores.innerText = '';
                this._nbRequiredSpores.innerText = '';
            }

            let nbBonus: number = this._saveManager.getNbBonus();
            if (nbBonus > 0) {
                this._bonusCount.classList.remove('hidden');
                this._nbBonus.innerText = nbBonus + '';
            }
            if (previousLevelState) {
                this._previousTextOrObjects = previousLevelState.textOrObjects.sort((a, b) => a.priority - b.priority);
            }
            else {
                this._previousTextOrObjects = [];// do not use length = 0 don't ask why 
            }

            levelState.textOrObjects.forEach(textOrObject => {
                let source = this.level.sources.find(source => source.isObject === textOrObject.isObject && source.name === textOrObject.name);
                if (source) {
                    let src = source.src;
                    textOrObject.text = source.text;
                    if (
                        source.srcRight
                        && source.srcBottom
                        && source.srcLeft
                    ) {
                        switch (textOrObject.moveDirection) {
                            case TextOrObject.RIGHT:
                                src = source.srcRight;
                                break;
                            case TextOrObject.DOWN:
                                src = source.srcBottom;
                                break;
                            case TextOrObject.LEFT:
                                src = source.srcLeft;
                                break;
                        }
                    }
                    else if (
                        source.srcL
                        && source.srcB
                        && source.srcBl
                        && source.srcR
                        && source.srcRl
                        && source.srcRb
                        && source.srcRbl
                        && source.srcT
                        && source.srcTl
                        && source.srcTb
                        && source.srcTbl
                        && source.srcTr
                        && source.srcTrl
                        && source.srcTrb
                        && source.srcTrbl
                    ) {
                        src = levelState.getSourceOrientation(textOrObject, source);
                    }
                    if(null !== src){
                        textOrObject.src = this._configLoader.filePath+src;
                    }
                    
                    textOrObject.priority = source.priority;
                }
            });

            this._levelState = levelState;
            this._textOrObjects = levelState.textOrObjects.sort((a, b) => a.priority - b.priority);
            this._nbFrameBeforeMessage = 0;
            this._cptAnimation = 0;

            if (!this._hasIntervall) {
                this._hasIntervall = true;
                setInterval(this.animationRepaint, 10);
            }
            window.requestAnimationFrame(this.repaint);
        }

       
        this._levelSrc =  levelState.textOrObjects.find(too => too.isGlobalLevel)?.src ?? null;
        
        
    }
}