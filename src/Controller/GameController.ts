import { KeyCode } from "../Models/KeyCode";
import { Level } from "../Models/Level";
import { LevelState } from "../Models/LevelState";
import { Quality } from "../Models/Rules/Quality";
import { FinalLevelReader } from "../Utils/FinalLevelReader";
import { KonamiManager } from "../Utils/KonamiManager";
import { LevelStateManager } from "../Utils/LevelStateManager";
import { SaveManager } from "../Utils/SaveManager";
import { GameView } from "../Views/GameView";

export class GameController {

    private static _instance: GameController | null = null;
    private _gameView: GameView;
    private _finalLevelReader: FinalLevelReader;
    private _levelState: LevelState;
    private _levelStateSave: LevelState;
    private _levelStateManager: LevelStateManager;
    private _saveManager: SaveManager;
    private _konamiManager: KonamiManager;
    private _solve: HTMLSelectElement;
    private _nextKey: HTMLElement;
    private _levelChoice: HTMLSelectElement;
    private _save: HTMLTextAreaElement;
    private _previousLevelState: LevelState | null;
    private _position: number;
    private _nextLevelPath: string | null;
    private _hasLogSolution: boolean;
    private _keyLock: boolean;

    private constructor() {
        this._gameView = GameView.instance
        this._finalLevelReader = FinalLevelReader.instance;
        this._levelStateSave = new LevelState(new Level([], [], [], [], "", 0, 0));
        this._levelState = this._levelStateSave.clone();
        this._levelStateManager = LevelStateManager.instance;
        this._saveManager = SaveManager.instance;
        this._konamiManager = KonamiManager.instance;
        this._previousLevelState = null;
        this._position = 0;
        this._nextLevelPath = null;
        this._hasLogSolution = false;
        this._keyLock = false;
        let solve = document.querySelector<HTMLSelectElement>('#solve');
        const downloadButton = document.querySelector<HTMLElement>('#download-button');
        const exportSave = document.querySelector<HTMLButtonElement>('#export-save');
        const importSave = document.querySelector<HTMLButtonElement>('#import-save');
        const fileInput = document.querySelector<HTMLInputElement>("#file-input")

        let levelChoice = document.querySelector<HTMLSelectElement>('#level_choice');
        let nextKey = document.querySelector<HTMLElement>('#next_key');
        let save = document.querySelector<HTMLTextAreaElement>('#save');
        if (solve && levelChoice && nextKey && save && downloadButton && exportSave && importSave && fileInput) {
            exportSave.addEventListener('click', () => {
                const url = URL.createObjectURL(new Blob([save.value], { type: "text/plain" }));

                let date = new Date();
                date.toString
               
                downloadButton.setAttribute('href', url);
                downloadButton.setAttribute('download', 'baba_is_you_'+date.toISOString()+'.baba.json');
                downloadButton.click();
            });
            fileInput.addEventListener('change', (e) => {
                const fichier =  fileInput.files ? fileInput.files[0] : undefined; 
                if (fichier) {
                    const reader = new FileReader();
                    reader.readAsText(fichier);
                    reader.onload =  (e) => {
                        const result = e.target?.result;
                        if (result && (typeof result === 'string')){
                            save.value = result;
                            save.dispatchEvent(new Event('change'));
                        }
                    };
                }
            })
            importSave.addEventListener('click', () => {
                fileInput.click();
            });

            this._solve = solve;
            this._nextKey = nextKey;
            this._levelChoice = levelChoice;
            this._save = save;
            levelChoice.addEventListener('change', () => {
                levelChoice.blur();
                if (levelChoice.value) {
                    this.loadLevel(levelChoice.value);
                }
            });
            save.addEventListener('change', () => {
                save.blur();
                let saveValue = save.value;
                this._saveManager.fromString(saveValue);
                this.saveGame(this._levelChoice.value);

            });

            document.addEventListener('keydown', (e: KeyboardEvent) => {
                this.keyDoww(e);
            });
            this._solve.addEventListener('input', (e: Event) => {
                this.inputSolve(e);
                this._solve.blur();
            });

            let lsSave = localStorage.getItem('save');
            if (lsSave) {
                save.value = lsSave;
            }

            save.dispatchEvent(new Event('change'));
        }
        else {
            throw new Error('can not find next_key or level_choice or solve');
        }
    }

    public static get instance(): GameController {
        if (!GameController._instance) {
            GameController._instance = new GameController();
        }
        return GameController._instance;
    }

    // controller call
    public loadLevel(level: string) {
        this._finalLevelReader.readLevelState(level, (levelState: LevelState) => {
            this._levelStateManager.init(levelState);
            this._levelStateSave = levelState;
            this._levelState = this._levelStateSave.clone();
            this._hasLogSolution = false;
            this._previousLevelState = null;
            this._gameView.init(this._levelState.level);

            this._solve.options.length = 0;
            if (levelState.level.actions.length > 0) {
                this._solve.options.add(new Option('', ''));
                this._levelState.level.actions.forEach((action, index) => {

                    let actionName: string = 'Action ' + index;
                    if (action.doAction === LevelState.STATE_IS_WIN) {// step 4
                        actionName = Quality.WIN;
                    }
                    else if (action.doAction === LevelState.STATE_IS_BONUS) {// step 4
                        actionName = Quality.BONUS;
                    }
                    else if (action.doAction === LevelState.STATE_IS_ZONE_COMPLETED) {// step 4
                        actionName = 'Complete zone';
                    }
                    else if (action.doAction === LevelState.STATE_IS_NEW_PARENT_RULES && action.rules.size > 0) {// step 4
                        actionName = 'Write rules : ' + [...action.rules].map(rule => rule.toString()).join(',')
                    }
                    this._solve.options.add(new Option(actionName, 'actionNumber' + index));
                })
            }

            this.view(false);
        });
    }


    // controller call
    public inputSolve = (e: Event) => {
        if ('' !== this._solve.value) {
            this.playKey(KeyCode.KEY_RESTART);
        }
        else {
            this._nextKey.innerText = '';
        }
    }


    // controller call
    public keyDoww = (e: KeyboardEvent) => {
        let key: KeyCode | null = null
        let eventKey = e.key.toLowerCase();

        switch (eventKey) {
            case 'arrowdown':
                key = KeyCode.KEY_DOWN;
                break;
            case 'arrowright':
                key = KeyCode.KEY_RIGHT;
                break;
            case 'arrowup':
                key = KeyCode.KEY_UP;
                break;
            case 'arrowleft':
                key = KeyCode.KEY_LEFT;
                break;
            case ' ':
                key = KeyCode.KEY_SPACE;
                break;
            case 'backspace':
                key = KeyCode.KEY_BACK;
                break;
            case 'r':
                key = KeyCode.KEY_RESTART;
                break;
            case 'enter':
                key = KeyCode.KEY_ENTER;
                break;
            case 'a':
                key = KeyCode.KEY_A;
                break;
            case 'b':
                key = KeyCode.KEY_B;
                break;
        }

        let password = localStorage.getItem('password');
        if(password === null && key === KeyCode.KEY_ENTER && this._saveManager.countIsWin() == 10){
            let answer = window.prompt("You seem to like the game 'Baba Is You.' Have you purchased the game? \n\n If so, you should have no trouble answering the following question. Otherwise, you won't be able to continue playing because this demo is not intended to replace the original game. \n\nWhat is the final word in the game 'Baba Is You'?");

            if(null == answer){
                return;
            }
          
            answer = answer.split(' ').map(answer => answer.trim()).join(' ').toLowerCase();
            console.log(answer);
            
            if('YWxsIGlzIGRvbmU=' !== btoa(answer)){
                return;
            }
            localStorage.setItem('password', 'ok');
        }
        if (key) {
            this.playKey(key);
        }
    }

    private saveGame = (levelPath: string | null) => {
        let saveValue = this._saveManager.toString();
        this._save.value = saveValue;
        localStorage.setItem('save', saveValue);
        if (null !== levelPath) {
            this._levelChoice.value = levelPath;
            this._levelChoice.dispatchEvent(new Event('change'));
        }
        this._nextLevelPath = null;
    }

    private playKey = (key: KeyCode) => {
        if (this._keyLock) {
            return;
        }
        this._keyLock = true;

        if (null !== this._nextLevelPath) {
            this.saveGame(this._nextLevelPath);
            this._keyLock = false;
            return;
        }

        let isEnabled = this._konamiManager.isEnabled
        this._konamiManager.playKey(key);
        if (!isEnabled && this._konamiManager.isEnabled) {
            document.querySelectorAll<HTMLElement>('.solve').forEach(solve => { solve.style.display = "block"; });
        }

        let tmpKeyKode: KeyCode | number | null = null;
        let solutionNumber = this._solve.value ? parseInt(this._solve.value.substring('actionNumber'.length)) : null;
        if (solutionNumber !== null && key !== KeyCode.KEY_RESTART) {
            tmpKeyKode = this._levelStateManager.getSolutionKey(this._levelState, solutionNumber, this._position);
            if (typeof tmpKeyKode === 'number') {
                key = KeyCode.KEY_ENTER;
            }
            else if (null !== tmpKeyKode) {
                key = tmpKeyKode;
            }
        }

        if (key === KeyCode.KEY_ENTER) {
            let select = this._levelState.calcLevelSelect();
            if (null !== select) {
                let levelPath = this._levelState.calcLevelPathOfSelect(select);
                if (null !== levelPath) {
                    this._saveManager.setSelectLevelPath(this._levelState, levelPath);
                    this.saveGame(solutionNumber === null ? levelPath : null);
                }
            }
        }

        [this._previousLevelState, this._levelState] = this._levelStateManager.getNextLevellState(
            this._levelState,
            key,
        );

        if (this._levelState.state === LevelState.STATE_IS_BONUS) {
            let action = this._levelStateManager.getSolution();
            console.log('<action keys=\'' + action.join(',') + ',' + this._levelStateManager.getKey(this._levelState) + '\' do=\'bonus\' />');
        }
        if (this._levelState.state === LevelState.STATE_IS_NEW_PARENT_RULES) {
            if (!this._hasLogSolution) {
                this._hasLogSolution = true;
                let action = this._levelStateManager.getSolution();
                console.log('<action keys=\'' + action.join(',') + ',' + this._levelStateManager.getKey(this._levelState) + '\' do=\'new_parent_rules\' new-parent-rules=\'' + [...this._levelState.parentSavedRules].join(',') + '\' />');
            }
        }
        else if (this._levelState.state === LevelState.STATE_IS_WIN) {
            if (!this._hasLogSolution) {
                this._hasLogSolution = true;
                let action = this._levelStateManager.getSolution();
                console.log('<action keys=\'' + action.join(',') + ',' + this._levelStateManager.getKey(this._levelState) + '\' do=\'win\' />');
            }
        }

        this._nextLevelPath = this._levelState.level.nextLevelPath;
        if (
            null !== this._nextLevelPath
            && this._levelState.state !== LevelState.STATE_IS_WIN
            && this._levelState.state !== LevelState.STATE_CAN_PLAY
            && this._levelState.state !== LevelState.STATE_CAN_NOT_PLAY
        ) {
            this.saveGame(this._nextLevelPath);
            this._keyLock = false;

            return;
        }

        if (solutionNumber !== null) {
            if (typeof tmpKeyKode === 'number') {
                this._finalLevelReader.simulateSublevel(
                    this._levelState,
                    tmpKeyKode,
                    () => {
                        this.saveGame(null)
                        this._position++;
                        this._levelState = this._levelStateSave.clone();
                        this._previousLevelState = null;
                        this._levelStateManager.init(this._levelState);
                        this.view(true);
                        this._keyLock = false;
                    }
                );

            }
            else if (key === KeyCode.KEY_RESTART) {
                this._position = 0;
                this.view(false);
                this._keyLock = false;
            }
            else {
                this._position++;
                this.view(false);
                this._keyLock = false;
            }
        }
        else {
            this.view(false);
            this._keyLock = false;
        }
    }

    private view = (forceReload: boolean) => {

        let solutionNumber = this._solve.value ? parseInt(this._solve.value.substring('actionNumber'.length)) : null;
        if (null !== solutionNumber) {
            let tmpKeyKode: KeyCode | number | null = this._levelStateManager.getSolutionKey(this._levelState, solutionNumber, this._position);
            if (typeof tmpKeyKode === 'number') {
                this._nextKey.innerText = tmpKeyKode + '';
            }
            else if (null !== tmpKeyKode) {
                this._nextKey.innerText = tmpKeyKode + '';
            }
            else {
                this._nextKey.innerText = '';
            }

        }
        this._gameView.view(this._previousLevelState, this._levelState, forceReload);

    }

}