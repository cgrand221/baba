
import { QuickLevelStateBuild } from "../Utils/QuickLevelStateBuild";

export class BuildLevelController{
    private static _instance: BuildLevelController|null = null;
    private _quickLevelStateBuild:QuickLevelStateBuild;  
    private xml: HTMLTextAreaElement;
    private declaration: HTMLTextAreaElement;
    private grid: HTMLTextAreaElement;

    private constructor(){
        
        this._quickLevelStateBuild = QuickLevelStateBuild.instance
        let declaration = document.querySelector<HTMLTextAreaElement>("#declaration");
        let grid = document.querySelector<HTMLTextAreaElement>("#grid");
        let xml = document.querySelector<HTMLTextAreaElement>("#xml");

        if(grid && xml && declaration){
            this.declaration = declaration;
            this.grid = grid;
            this.xml = xml;
            this.declaration.addEventListener('input', this.gridChange);
            this.grid.addEventListener('input', this.gridChange);
        }
        else{
            throw new Error('can not find declaration or grid or xml HTMLTextAreaElement ');
        }
    }

    public static get instance(): BuildLevelController
    {
        if(!BuildLevelController._instance){
            BuildLevelController._instance = new BuildLevelController();
        }
        return BuildLevelController._instance;
    }

    public gridChange = () => {
        try{
            let json:  { [key: string]: string } = JSON.parse(this.declaration.value);
            let ls = this._quickLevelStateBuild.buildLevelState(json, this.grid.value);
            if(ls){
                this.xml.textContent = this._quickLevelStateBuild.toXml(ls);
            }
            else{
                this.xml.textContent = 'error';
            }
        }
        catch (e){
            this.xml.textContent = 'error';
        }
    }
}