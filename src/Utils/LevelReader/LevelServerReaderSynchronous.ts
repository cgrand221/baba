
import * as fs from 'fs';
import { LevelReaderInterface } from './LevelReaderInterface';
import { ConfigLoader } from '../ConfigLoader';
import { LevelStateBuildManager } from '../LevelStateBuildManager';
import { LevelState } from '../../Models/LevelState';

export class LevelServerReaderSynchronous implements LevelReaderInterface {
    private static _instance: LevelServerReaderSynchronous|null = null;
    private _configLoader:ConfigLoader;
    private _levelStateBuildManager:LevelStateBuildManager;

    private constructor(){
        this._configLoader = ConfigLoader.instance;
        this._levelStateBuildManager = LevelStateBuildManager.instance;
    }

    public static get instance(): LevelReaderInterface
    {
        if(!LevelServerReaderSynchronous._instance){
            LevelServerReaderSynchronous._instance = new LevelServerReaderSynchronous();
        }
        return LevelServerReaderSynchronous._instance;
    }

    public read = (file: string, callback: (levelState: LevelState) => void): void => {
        const _levelStateBuildManager = this._levelStateBuildManager;
        let levelState = _levelStateBuildManager.buildLevel(file, fs.readFileSync(__dirname + '/../../../static/'+this._configLoader.filePath+file, "utf8"));
        if(null !== levelState){
            callback(levelState);
        }
    }
}