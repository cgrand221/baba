import { LevelState } from "../../Models/LevelState";
import { ConfigLoader } from "../ConfigLoader";
import { LevelStateBuildManager } from "../LevelStateBuildManager";
import { LevelReaderInterface } from "./LevelReaderInterface";

export class LevelBrowserReaderAsynchronous implements LevelReaderInterface {
    private static _instance: LevelBrowserReaderAsynchronous|null = null;
    private _configLoader:ConfigLoader;
    private _levelStateBuildManager:LevelStateBuildManager;

    private constructor(){
        this._configLoader = ConfigLoader.instance;
        this._levelStateBuildManager = LevelStateBuildManager.instance;
    }

    public static get instance(): LevelReaderInterface
    {
        if(!LevelBrowserReaderAsynchronous._instance){
            LevelBrowserReaderAsynchronous._instance = new LevelBrowserReaderAsynchronous();
        }
        return LevelBrowserReaderAsynchronous._instance;
    }

    public read = (file: string, callback: (levelState: LevelState) => void): void => {
        var xhttp = new XMLHttpRequest();
        const _levelStateBuildManager = this._levelStateBuildManager;
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let levelState = _levelStateBuildManager.buildLevel(file, xhttp.responseText);3
                if(null !== levelState){
                    callback(levelState);
                }
            }
        };
        xhttp.open("GET", this._configLoader.filePath+file, true);
        xhttp.send();
    }
}