import { LevelState } from "../../Models/LevelState";
import { LevelReaderInterface } from "./LevelReaderInterface";

export class LevelNothingReader implements LevelReaderInterface {
    private static _instance: LevelNothingReader|null = null;

    private constructor(){
    }

    public static get instance(): LevelReaderInterface
    {
        if(!LevelNothingReader._instance){
            LevelNothingReader._instance = new LevelNothingReader();
        }
        return LevelNothingReader._instance;
    }

    public read = (file: string, callback: (levelState: LevelState) => void): void => {
    }
}