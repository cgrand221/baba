import { LevelState } from "../../Models/LevelState";


export interface LevelReaderInterface{
    read: (file: string, callback: (levelState: LevelState) => void) => void
}