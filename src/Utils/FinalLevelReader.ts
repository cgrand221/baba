import { Action } from "../Models/Action";
import { KeyCode } from "../Models/KeyCode";
import { LevelState } from "../Models/LevelState";
import { LevelBrowserReaderAsynchronous } from "./LevelReader/LevelBrowserReaderAsynchronous";
import { LevelNothingReader } from "./LevelReader/LevelNothingReader";
import { LevelReaderInterface } from "./LevelReader/LevelReaderInterface";
import { LevelServerReaderSynchronous } from "./LevelReader/LevelServerReaderSynchronous";
import { LevelStateManager } from "./LevelStateManager";
import { SaveManager } from "./SaveManager";

export class FinalLevelReader {
    private static _instance: FinalLevelReader | null = null;
    private _levelReader: LevelReaderInterface;
    private _saveManager: SaveManager;
    private _levelStateManager: LevelStateManager;
    private _isInit: boolean;

    private constructor() {
        this._levelReader = LevelNothingReader.instance;
        this._saveManager = SaveManager.instance;
        this._levelStateManager = LevelStateManager.instance;
        this._isInit = false;
    }

    public static get instance(): FinalLevelReader {
        if (!FinalLevelReader._instance) {
            FinalLevelReader._instance = new FinalLevelReader();
        }
        return FinalLevelReader._instance;
    }

    public initLevelReader = (isServer: boolean): void => {
        if (this._isInit) {
            return;
        }
        this._isInit = true;
        if (isServer) {
            this._levelReader = LevelServerReaderSynchronous.instance;
            return;
        }
        this._levelReader = LevelBrowserReaderAsynchronous.instance;
    }

    public readLevelState = (file: string, callback: (levelState: LevelState) => void): void => {
        this._levelReader.read(file,  (levelState: LevelState): void => {
            let parent = levelState.level.parent;
            if(null === parent){
                callback(levelState);
                return;
            }
            this._levelReader.read(parent,  (parentLevelState: LevelState): void => {
                levelState.parentLevelState = parentLevelState;
                callback(levelState);
            });
        });
    }

    public simulateSublevel = (
        levelState: LevelState,
        solutionNumber: number,
        callback: () => void
    ): void => {

        /**
         * We do :
         *   1 we play artificially enter key
         *   2 no game controller action
         *   3 we do not play sublevel, we suppose sublevel's action being correct (checked by other tests)
         *   4 we just set sublevel state to the required state
         *   5 we play space artificially, same as view on browser
         * 
         * This methode is need to play save manager on sinning sublevel in order to continue tests on level
         */

        let select = levelState.calcLevelSelect();
        if (select === null) {
            return;
        }
        let levelPathOfSelect = levelState.calcLevelPathOfSelect(select);
        if (null === levelPathOfSelect) {
            return;
        }

        this.simulateSublevelWithFile(
            levelPathOfSelect,
            solutionNumber,
            callback
        );
    }


    public simulateSublevelWithFile = (
        levelPathOfSelect: string,
        solutionNumber: number,
        callback: () => void
    ): void => {
        this._levelReader.read(levelPathOfSelect, (ls2: LevelState) => {
            if (null === ls2) {
                return;
            }
            let action: Action | null = ls2.level.actions[solutionNumber] ?? null;
            if (null === action) {
                return;
            }

            // step 2 and 3 : nothing to do
            if (action.doAction === LevelState.STATE_IS_WIN) {// step 4
                this._saveManager.setLevelWin(ls2);
            }
            else if (action.doAction === LevelState.STATE_IS_BONUS) {// step 4
                this._saveManager.setLevelBonus(ls2);
            }
            else if (action.doAction === LevelState.STATE_IS_ZONE_COMPLETED) {// step 4
                this._saveManager.setZoneCompleted(ls2);
            }
            else if (action.doAction === LevelState.STATE_IS_NEW_PARENT_RULES) {// step 4
                this._saveManager.setParentSavedRule(ls2, action.rules);
            }
            this._levelStateManager.getNextLevellState(ls2, KeyCode.KEY_SPACE);// step 5

            callback();
        });
    }
}