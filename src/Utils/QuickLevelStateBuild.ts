import { Level } from "../Models/Level";
import { LevelState } from "../Models/LevelState";
import { Source } from "../Models/Source";
import { TextOrObject } from "../Models/TextOrObject";
import { LevelStateManager } from "./LevelStateManager";

export class QuickLevelStateBuild {
    private static _instance: QuickLevelStateBuild | null = null;
    private _levelStateManager: LevelStateManager;

    private constructor() {
        this._levelStateManager = LevelStateManager.instance;
    }

    public static get instance(): QuickLevelStateBuild {
        if (!QuickLevelStateBuild._instance) {
            QuickLevelStateBuild._instance = new QuickLevelStateBuild();
        }
        return QuickLevelStateBuild._instance;
    }

    private currentWidth(levelLine: string): number {
        let iterate = true;
        let currentWidth = 0;
        for (let i = 0; i < levelLine.length; i++) {
            let c = levelLine[i];
            if (c) {
                if (iterate) {
                    if (c === '[') {
                        currentWidth++;
                        iterate = false;
                    }
                    else if (c !== '[' && c !== ']') {
                        currentWidth++;
                    }
                }
                else {
                    if (c === ']') {
                        iterate = true;
                    }
                }
            }
        }
        return currentWidth;
    }

    private getWidthHeight = (levelLines: string[]): { width: number, height: number } | null => {
        let height = 0;
        let width = 0;
        let error = false;
        levelLines.forEach(levelLine => {
            if (levelLine.length > 0 && !error) {
                let currentWidth = this.currentWidth(levelLine);
                if (width === 0) {
                    width = currentWidth;
                }
                else if (width !== currentWidth) {
                    error = true;
                }
                height++;
            }
        });
        if (error) {
            return null;
        }
        return {
            width: width,
            height: height,
        }
    }


    public isElementObject(c: string): boolean {
        if (c.toUpperCase() == c) {
            return false;
        }
        return true;
    }

    private addCurrentLineToLevelState(declarations: { [key: string]: string }, levelState: LevelState, levelLine: string, y: number) {
        let iterate = true;
        let x = 0;
        let dir: number = TextOrObject.DEFAULT_MOVE_DIR;
        for (let i = 0; i < levelLine.length; i++) {
            let c = levelLine[i];
            if (c && c !== ' ') {
                if(!iterate){
                    if (c === '^'){
                        dir = TextOrObject.UP;
                    }
                    else if (c === 'v'){
                        dir = TextOrObject.DOWN;
                    }
                    else if (c === '>'){
                        dir = TextOrObject.RIGHT;
                    }
                    else if (c === '<'){
                        dir = TextOrObject.LEFT;
                    }
                }
                
                if (c === '[') {
                    iterate = false;
                }
                else if (c !== '[' && c !== ']') {
                    let nameOfElement = declarations[c];
                    if (nameOfElement) {
                        let too = new TextOrObject(
                            nameOfElement,
                            this.isElementObject(c),
                            x,
                            y
                        );
                        too.moveDirection = dir;
                       
                        levelState.addTextOrObject(
                            too
                        );
                    }
                }
                else if (c === ']') {
                    iterate = true;
                }
            }
           

            if (iterate) {
                x++;
            }
        }
    }

    public toXml(levelState:LevelState): string {
        let xmls:string[] = [];
        xmls.push('<?xml version="1.0" encoding="UTF-8"?>');
        xmls.push('<level width="'+levelState.level.width+'" height="'+levelState.level.height+'">');
        xmls.push('  <sources>');

        xmls.push('    <objects>');
       
        levelState.getWordsObjectToMatch().forEach(name => {
            xmls.push('      <'+name+' src="../img/objects/'+name+'.png" />');
        });     
        xmls.push('    </objects>');

        xmls.push('    <texts>');
        let toos:TextOrObject[]=[];
        levelState.textOrObjects.filter(too => !too.isObject).forEach(textOrObject => {
            if(!toos.some(too => too.name === textOrObject.name)){
                toos.push(textOrObject);
                xmls.push('      <'+textOrObject.name+' src="../img/texts/'+textOrObject.name+'.png" />');
            }
        });       
        xmls.push('    </texts>');
        xmls.push('  </sources>');
        xmls.push('  <textOrObjects>');
        xmls.push('    <objects>');
        levelState.textOrObjects.filter(too => too.isObject).forEach(too => {
            xmls.push('      <'+too.name+' x="'+too.x+'" y="'+too.y+'" />');
        });  
        xmls.push('    </objects>');
        xmls.push('    <texts>');
        levelState.textOrObjects.filter(too => !too.isObject).forEach(too => {
            xmls.push('      <'+too.name+' x="'+too.x+'" y="'+too.y+'" />');
        });  
        xmls.push('    </texts>');
        xmls.push('  </textOrObjects>');
        xmls.push('</level>');
        return xmls.join("\n");
    }

    public buildLevelState = (declarations: { [key: string]: string }, levelgrid: string): LevelState | null => {
        let levelLines = levelgrid.split('\n');
        let widthHeight = this.getWidthHeight(levelLines);
        if (widthHeight === null) {
            return null;
        }

        let sources: Source[] = [];
        let objectNames: string[] = [];
        let textNames: string[] = [];
        Object.entries(declarations).forEach(charAndName => {
            let c = charAndName[0];
            let name = charAndName[1];
            let isObject = this.isElementObject(c);
            sources.push(
                new Source(name, isObject, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, 1)
            );
            if (isObject) {
                objectNames.push(name);
            }
            else{
                textNames.push(name);
            }
        });

        let levelState = new LevelState(new Level(objectNames, textNames, [], sources, '', widthHeight.width, widthHeight.height));
        let y = 0;
        levelLines.forEach(levelLine => {
            if (levelLine.length > 0) {
                this.addCurrentLineToLevelState(declarations, levelState, levelLine, y);
                y++;
            }
        });
        this._levelStateManager.init(levelState);
        return levelState;
    }

    public levelStateToString = (declarations: { [key: string]: string }, levelState: LevelState, surround: string | null) => {
        let cellss: string[] = [];
        let y = 0;
        if (surround) {
            let cells: string[] = [];
            for (let x = 0; x < levelState.level.width + 2; x++) {
                cells.push(surround);
            }
            cellss.push(cells.join(''));
        }
        for (let y = 0; y < levelState.level.height; y++) {
            let cells: string[] = [];
            if (surround) {
                cells.push(surround);
            }
            for (let x = 0; x < levelState.level.width; x++) {
                let textAndObjects = levelState.getTextAndObjectsAt(x, y).filter(too => !too.isGlobalLevel);
                let chars: string[] = textAndObjects.map(too => this.toShortName(declarations, too));
                let char = chars[0];
                if (chars.length > 1) {
                    cells.push('[' + chars.sort().join('') + ']');
                }
                else if (char) {
                    cells.push(char);
                }
                else {
                    cells.push(' ');
                }
            }

            if (surround) {
                cells.push(surround);
            }
            cellss.push(cells.join(''));

        }
        if (surround) {
            let cells: string[] = [];
            for (let x = 0; x < levelState.level.width + 2; x++) {
                cells.push(surround);
            }
            cellss.push(cells.join(''));
        }
        return cellss.join("\n");
    }

    private toShortName = (declarations: { [key: string]: string }, textOrObject: TextOrObject): string => {
        let declaration = Object.entries(declarations).find(charAndName =>
            this.isElementObject(charAndName[0]) === textOrObject.isObject
            && charAndName[1] === textOrObject.name
        );
        if (declaration) {
            return declaration[0];
        }
        return ' ';
    }

}
