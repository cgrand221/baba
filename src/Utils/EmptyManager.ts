import { LevelState } from "../Models/LevelState";
import { Empty } from "../Models/Rules/Empty";
import { TextOrObject } from "../Models/TextOrObject";

export class EmptyManager{
    private static _instance: EmptyManager|null = null;

    private constructor(){
    }

    public static get instance(): EmptyManager
    {
        if(!EmptyManager._instance){
            EmptyManager._instance = new EmptyManager();
        }
        return EmptyManager._instance;
    }

    public completeWithEmptys = (levelState: LevelState) => {
        if(levelState.withEmptys){
            this.cleanEmptys(levelState)
        }
        if(this.addEmptys(levelState)){
            levelState.withEmptys = true;
        }
    }

    public removeEmptys = (levelState: LevelState) => {
        if(levelState.withEmptys){
            let toRemoves:TextOrObject[] = [];
            for(let x = 0;x < levelState.level.width;x++){
                for(let y = 0;y < levelState.level.height;y++){
                    let toos = levelState.getTextAndObjectsAt(x, y);
                    let emptys = toos.filter(too => too.name === Empty.EMPTY && too.isObject);
                    toRemoves.push(...emptys);
                }
            }
            toRemoves.forEach(toRemove => {
                levelState.removeTextOrObject(toRemove);
            });
            levelState.withEmptys = false;
        }
    }

    private addEmptys = (levelState: LevelState): boolean => {
        let emptys:TextOrObject[] = [];
        for(let x = 0;x < levelState.level.width;x++){
            for(let y = 0;y < levelState.level.height;y++){
                let toos = levelState.getTextAndObjectsAt(x, y);
                if (toos.length <= 1){// if size is 1, there is only level
                    emptys.push(new TextOrObject(Empty.EMPTY, true, x, y));
                }
            }
        }
        levelState.addTextOrObject(...emptys);
        return emptys.length > 0;
    }

    private cleanEmptys = (levelState: LevelState): void => {
        let toRemoves:TextOrObject[] = [];
        for(let x = 0;x < levelState.level.width;x++){
            for(let y = 0;y < levelState.level.height;y++){
                let toos = levelState.getTextAndObjectsAt(x, y);
                let emptys = toos.filter(too => too.name === Empty.EMPTY && too.isObject);
                if(toos.some(too => too.name !== Empty.EMPTY || !too.isObject)){
                    toRemoves.push(...emptys);
                }
                else if (emptys.length > 1){
                    toRemoves.push(...emptys.slice(1));
                }
            }
        }
        
        toRemoves.forEach(toRemove => {
            levelState.removeTextOrObject(toRemove);
        })
       
    }
}