import { LeftRightBaseRule } from "../Models/LeftRightBaseRule";
import { Rule } from "../Models/Rule";
import { TextOrObject } from "../Models/TextOrObject";

export class RuleFinderManager{
    
    private static _instance: RuleFinderManager|null = null;
    private _positionInCombination:number[];
    private _combinations:TextOrObject[];
    private _matchdItems:string[];

    private constructor(){
        this._positionInCombination = [];
        this._combinations = [];
        this._matchdItems = [];
    }

    public static get instance(): RuleFinderManager
    {
        if(!RuleFinderManager._instance){
            RuleFinderManager._instance = new RuleFinderManager();
        }
        return RuleFinderManager._instance;
    }


    public init = (combinations:TextOrObject[], textToCompile:string, matchdItems:string[]): void => {
        this._combinations = combinations;
        this._matchdItems = matchdItems;
        this._positionInCombination = [];
        let j = 0;
        let k = 0;
        combinations.forEach((combination, index) => {
            for (let i = 0;i < combination.name.length;i++){
                this._positionInCombination.push(index);
            }
        });
    }

    private findTextOrObjects = (posItem:number):TextOrObject[] => {
        let length = 0;
        for(let i=0;i < posItem;i++){
            let matchdItem = this._matchdItems[i];
            if(matchdItem){
                length += matchdItem.length;
            }
        }
       
        let item = this._matchdItems[posItem];
        let toos:TextOrObject[] = [];
        if(item){
            for(let i = 0;i < item.length;i++){
                let pos = this._positionInCombination[length+i] ?? -1;
                if(pos !== -1){
                    let too = this._combinations[pos];
                    if(too && !toos.includes(too)){
                        toos.push(too);
                    }
                }
            }
        }
        return toos;
    }

    public tagRule = (rule: Rule, start: number, end:number) => {// b ab b is ... => 0,1,3
        for(let i = start;i<end;i++){
            let toos = this.findTextOrObjects(i);
            
            toos.forEach(too => {
                too.rules.push(rule);
            });
            
        }         
    }

}