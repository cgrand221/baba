import { ConditionRule } from "../../Models/ConditionRule";
import { LeftRightBaseRule } from "../../Models/LeftRightBaseRule";
import { Rule } from "../../Models/Rule";
import { parser as leftParser } from "./left_parser" ;
import { parser as rightParser } from "./right_parser";
import {SyntaxNodeRef } from '@lezer/common';

export class LezerManager {
    private static _instance: LezerManager | null = null;

    private constructor() {
    }

    public static get instance(): LezerManager {
        if (!LezerManager._instance) {
            LezerManager._instance = new LezerManager();
        }
        return LezerManager._instance;
    }

    public parse = (lefts: string[], rights: string[]): Rule|null => {
        let rule = new Rule(0, [],[], '', []);
        this.parseLeft(lefts, rule);
        
        if(rule.lefts.length === 0){
            return null;
        }

        this.parseVerbRight(rights, rule);

        if(rule.rights.length === 0){
            return null;
        }
        
        return rule;
    }

    private parseLeft = (lefts: string[], rule:Rule): void => {
        
        let textToParse = [...lefts].reverse().join(' ');
        let leftTree = leftParser.parse(textToParse);

        let error:boolean = false;
        let cr:ConditionRule|null=null;
        let lrbrs:LeftRightBaseRule[]=[];
        let conditions:ConditionRule[]=[];
        let nbNotLonely = 0;
        let lonely = false;
        let ruleEnd:number = 0;
        
        leftTree.iterate({
            enter: (node: SyntaxNodeRef): boolean => {
                let name = textToParse.substring(node.from, node.to).toLowerCase();

                // cond                             cond                             left
                // (flag notand baba not) on not and (flag not and baba not) on not (flag not and baba not)

                if(
                    error || 
                    node.type.isError 
                ){
                    error = true;
                    return false;
                }
                else if (node.name === 'Conditionnal'){ 
                    cr = new ConditionRule(0, name, [], node.from, node.to);
                    cr.leftRightBaseRules = lrbrs;
                    conditions.push(cr);
                    lrbrs=[];
                }
                else if (node.name === 'Not'){
                    if(lonely){
                        ruleEnd = node.to;
                        nbNotLonely++;
                    }
                    else if(null !== cr){
                        cr.nbNot++;
                    }
                    else if(null !== lrbrs){
                        let lrbr = lrbrs[lrbrs.length-1] ?? null;
                        if(null !== lrbr){
                            lrbr.nbNot++;
                            lrbr.end = node.to;
                        }
                    }
                }
                else if (node.name === 'Lonely'){    
                    ruleEnd = node.to;
                    lonely = true;
                }
                else if (
                    node.name === 'Object'
                    || node.name === 'Quality'
                ){
                    let lrbr = new LeftRightBaseRule(0, name, node.from, node.to);
                    lrbrs.push(lrbr);
                    
                }
                    
                
                return true;
            },
            leave: (node: SyntaxNodeRef): boolean => {
                if(
                    error || 
                    node.type.isError 
                ){
                    error = true;
                    return false;
                }

                return true;
            },
        });

        if(lrbrs.length === 0 && conditions.length > 0){
            // error in conditions => no conditions and we keep left
            let condition = conditions[conditions.length-1] ?? null;
            if(null !== condition){
                lrbrs = condition.leftRightBaseRules;
                conditions = [];
            }
        }        
        if(!lonely){
            let lrbr = lrbrs[lrbrs.length-1] ?? null;
            if (null !== lrbr){
                ruleEnd = lrbr.end;
            }
        }
        
        // calc pos tion with reverse
        lrbrs.forEach(lrbr => {
            let start =  lrbr.start;
            let end =  lrbr.end;
            lrbr.start = textToParse.length - end;
            lrbr.end = textToParse.length - start;
        });
        conditions.forEach(condition => {
            let start = condition.start;
            let end = condition.end;
            condition.start = textToParse.length - end;
            condition.end = textToParse.length - start;
        });
        
        let ruleStart = {
            start: textToParse.length - ruleEnd
        };

        // remove space positions
        this.removeSpacePos (lrbrs, conditions, textToParse, ruleStart);

        rule.lefts = lrbrs.reverse();
        rule.conditions = conditions.reverse();
        rule.nbNotLonely = nbNotLonely;
        rule.lonely = lonely;
        rule.start = ruleStart.start;
    }

    

    private parseVerbRight = (rights: string[], rule:Rule): void => {
        let textToParse = rights.join(' ');

        let error:boolean = false;
        let notPos:number|null = null;
        let nbNot:number = 0;
        let lrbrs:LeftRightBaseRule[]=[];
        let verb:string='';
        
        let rightTree = rightParser.parse(textToParse);
        rightTree.iterate({
            enter: (node: SyntaxNodeRef): boolean => {
                let name = textToParse.substring(node.from, node.to).toLowerCase();
                
                if(
                    error || 
                    node.type.isError 
                ){
                    
                    error = true;
                    return false;
                }
                
                if (node.name === 'Not'){
                    if(null === notPos){
                        notPos = node.from;
                    }
                    nbNot++;
                }               
                else if (
                    node.name === 'Object'
                    || node.name === 'Quality'
                ){
                    
                    let lrbr = new LeftRightBaseRule(nbNot, name, notPos !== null ? notPos : node.from, node.to);
                    lrbrs.push(lrbr);
                    
                    nbNot = 0;
                    notPos = null;
                }
                else if(
                    node.name === 'OtherVerb'
                    || node.name === 'IsWrite'
                ){
                    verb = name;
                }       
                
                return true;
            },
            leave: (node: SyntaxNodeRef): boolean => {
                if(
                    error || 
                    node.type.isError 
                ){
                    error = true;
                    return false;
                }

                return true;
            },
        });
        
        this.removeSpacePos (lrbrs, [], textToParse, null);
        rule.rights = lrbrs;
        rule.verb = verb;
    }

    public removeSpacePos = (lrbrs: LeftRightBaseRule[], conditions:ConditionRule[], textToParse: string, ruleStart: {
        start: number
    }|null ) => {
        let nbSpaces:number[] = [];

        let nbSpace = 0;
        for (let i = 0;i < textToParse.length;i++){
            let c = textToParse[i] ?? null;
            if(null !== c){
                nbSpaces[i] = nbSpace;
                if(c === ' '){
                    nbSpace++;
                }
            }
        }
        
        let lastNbSpace = nbSpaces[nbSpaces.length-1] ?? 0;
        conditions.forEach(condition => {
            condition.start -= (nbSpaces[condition.start] ?? 0);
            condition.end -= (nbSpaces[condition.end] ?? lastNbSpace);
        });
        lrbrs.forEach(lrbr => {
            
            lrbr.start -= (nbSpaces[lrbr.start] ?? 0);
            lrbr.end -= (nbSpaces[lrbr.end] ?? lastNbSpace);
        });
        if(null !== ruleStart){
            ruleStart.start -= (nbSpaces[ruleStart.start] ?? 0);
        }
    }
}