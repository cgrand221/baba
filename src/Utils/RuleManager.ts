

import { LevelState } from "../Models/LevelState";
import { Quality } from "../Models/Rules/Quality";
import { Rule } from "../Models/Rule";
import { Text } from "../Models/Rules/Text";
import { TextOrObject } from "../Models/TextOrObject";
import { Verb } from "../Models/Rules/Verb";
import { LeftRightBaseRule } from "../Models/LeftRightBaseRule";
import { RuleCompileManager } from "./RuleCompileManager";
import { EmptyManager } from "./EmptyManager";
import { RuleCheckerManager } from "./RuleCheckerManager";
import { Group } from "../Models/Rules/Group";
import { Cursor } from "../Models/Rules/Cursor";
import { Line } from "../Models/Rules/Line";
import { SaveManager } from "./SaveManager";
import { ConfigLoader } from "./ConfigLoader";
import { RLevel } from "../Models/Rules/RLevel";

export class RuleManager {

    private static _instance: RuleManager | null = null;
    private _ruleCompileManager: RuleCompileManager;
    private _emptyManager: EmptyManager;
    private _ruleCheckerManager: RuleCheckerManager;
    private _saveManager: SaveManager;
    private _configLoader: ConfigLoader;

    private constructor() {
        this._ruleCompileManager = RuleCompileManager.instance;
        this._emptyManager = EmptyManager.instance;
        this._ruleCheckerManager = RuleCheckerManager.instance;
        this._saveManager = SaveManager.instance;
        this._configLoader = ConfigLoader.instance;
    }

    public static get instance(): RuleManager {
        if (!RuleManager._instance) {
            RuleManager._instance = new RuleManager();
        }
        return RuleManager._instance;
    }

    public init = (levelState: LevelState) => {
        this._ruleCompileManager.init(levelState);
    }

    public applySave =  (levelState: LevelState): void => {
        levelState.textOrObjects.forEach(too => {
            let levelPath = too.levelPath;
            if(null !== levelPath){
                if(this._saveManager.isLevelWinPath(levelPath)){
                    too.viewState = TextOrObject.VIEW_STATE_WIN;
                }
                else if(this._saveManager.hasLevelRequiredSporesPath(levelPath)){
                    too.viewState = TextOrObject.VIEW_STATE_HAS_REQUIRED_SPORES;
                }
            }
        });

        let file = levelState.level.file;
        let selectLevelPath = this._saveManager.getSelectLevelPath(file);
        if (null === selectLevelPath){
            return;
        }
        let too = levelState.textOrObjects.find(too => too.levelPath === selectLevelPath)
        if(undefined === too){
            return;
        }
        let select = levelState.textOrObjects.find(too => too.isSelect);
        if(undefined === select){
            return;
        }
        select.x = too.x;
        select.y = too.y;
    }

    public compileAndCalcProperties = (levelState: LevelState, rules: Rule[], progressWord: boolean): void => {
        this._emptyManager.completeWithEmptys(levelState);
        this._ruleCheckerManager.group = [];
        let nbLoop = 0;
        let progressGroup = true;
        let ruleNoWrites:Rule[] = rules.filter(rule => rule.verb !== Verb.WRITE);

        this.resetAllProperties(levelState);

        while (
            (
                progressGroup
                || progressWord
            )
            && nbLoop < 4
        ) {

            if (progressWord) {
                rules.length = 0;
                levelState.textOrObjects.forEach(too => {
                    too.rules.length = 0;
                });

                // _ruleCompileManager uses levelState.textOrObjects[...].isWord
                rules.push(...this._ruleCompileManager.getRules(levelState, false));
                rules.push(...this._ruleCompileManager.getRules(levelState, true));
                rules.push(new Rule(
                    0,
                    [new LeftRightBaseRule(0, Text.TEXT, 0, 1)],
                    [],
                    Verb.IS,
                    [new LeftRightBaseRule(0, Quality.PUSH, 2, 3)],
                ));

                rules.push(new Rule(
                    0,
                    [new LeftRightBaseRule(0, RLevel.LEVEL, 0, 1)],
                    [],
                    Verb.IS,
                    [new LeftRightBaseRule(0, Quality.STOP, 2, 3)],
                ));

                let needSelectAndPath = levelState.textOrObjects.some(too => (
                    too.levelPath !== null 
                    || too.name === Cursor.CURSOR 
                    || too.name === Line.LINE
                ));

                if(
                    needSelectAndPath
                ){
                    rules.push(new Rule(
                        0,
                        [new LeftRightBaseRule(0, Cursor.CURSOR, 0, 1)],
                        [],
                        Verb.IS,
                        [new LeftRightBaseRule(0, Quality.SELECT, 2, 3)],
                    ));
                    rules.push(new Rule(
                        0,
                        [new LeftRightBaseRule(0, Line.LINE, 0, 1)],
                        [],
                        Verb.IS,
                        [new LeftRightBaseRule(0, Quality.PATH, 2, 3)],
                    ));
                   
                }
                
            }
            ruleNoWrites.length = 0
            ruleNoWrites.push(...rules.filter(rule => rule.verb !== Verb.WRITE));


            this.calcDisabledProperties(levelState, rules);

            progressGroup = false;
            progressWord = false;

            // to respect order of applied properties, we have to restart on next loop if needed
            if (this.calcGroupProperties(levelState, ruleNoWrites)) {
                progressGroup = true;
            }
            else if (this.calcWordProperties(levelState, ruleNoWrites)) {
                progressWord = true;
            }

            nbLoop++;
        }

        if (progressGroup || progressWord) {
            levelState.state = LevelState.STATE_IS_INFINITE_LOOP;
            levelState.textOrObjects.forEach(too => {
                too.resetWord();
                too.resetGroup();
                too.resetOtherProperties();
                too.rules.length = 0;
            });
            rules.length = 0;
            return;
        }

        this.calcOtherProperties(levelState, ruleNoWrites);
    }

    public calcWordProperties = (levelState: LevelState, rules: Rule[]): boolean => {
        levelState.textOrObjects.forEach(too => {
            too.saveWord();
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.WORD, (too: TextOrObject) => {
            too.isWord = true;
        }, () => { }, (too: TextOrObject) => {
            too.resetWord();
        });
        return (levelState.textOrObjects.some(too => too.hasWordChanged()));
    }

    private calcDisabledProperties = (levelState: LevelState, rules: Rule[]) => {
        rules.forEach(rule => {
            rule.resetDisabled();
        });
        this._ruleCheckerManager.calcDisabledRules(levelState, rules);
    }

    private calcGroupProperties = (levelState: LevelState, rules: Rule[]): boolean => {
        // group
        levelState.textOrObjects.forEach(too => {
            too.saveGroup();
        });
        let group: string[] = [];
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Group.GROUP, (too: TextOrObject) => {
            too.nbGroup++;
        }, (rule: Rule, contents: string[]) => {
            if (rule.conditions.length === 0) {
                contents.forEach(content => {
                    group.push(content);
                });
            }
        }, (too: TextOrObject) => {
            too.resetGroup();
        });

        this._ruleCheckerManager.group = group;
        return (
            levelState.textOrObjects.some(too => too.hasGroupChanged())
        );
    }

    private resetAllProperties = (levelState: LevelState) => {
        levelState.textOrObjects.forEach(too => {
            too.resetWord();
            too.resetGroup();
            too.resetOtherProperties();
        });
    }

    private calcOtherProperties = (levelState: LevelState, rules: Rule[]) => {
        this._ruleCheckerManager.calcSelfDestroy(levelState, rules, (too: TextOrObject) => {
            too.isSelfDestroy = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.PUSH, (too: TextOrObject) => {
            too.isPush = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.STOP, (too: TextOrObject) => {
            too.isStop = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.YOU, (too: TextOrObject) => {
            too.isYou = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.WIN, (too: TextOrObject) => {
            too.isWin = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.HOT, (too: TextOrObject) => {
            too.isHot = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.MELT, (too: TextOrObject) => {
            too.isMelt = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.DEFEAT, (too: TextOrObject) => {
            too.isDefeat = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.SINK, (too: TextOrObject) => {
            too.isSink = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.MOVE, (too: TextOrObject) => {
            too.nbMoves++;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.SHUT, (too: TextOrObject) => {
            too.isShut = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.OPEN, (too: TextOrObject) => {
            too.isOpen = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.FLOAT, (too: TextOrObject) => {
            too.isFloat = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.WEAK, (too: TextOrObject) => {
            too.isWeak = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.TELE, (too: TextOrObject) => {
            too.isTele = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.PULL, (too: TextOrObject) => {
            too.isPull = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.SHIFT, (too: TextOrObject) => {
            too.nbShift++;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.FALL, (too: TextOrObject) => {
            too.nbFallD++;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.FALL_D, (too: TextOrObject) => {
            too.nbFallD++;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.FALL_R, (too: TextOrObject) => {
            too.nbFallR++;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.FALL_U, (too: TextOrObject) => {
            too.nbFallU++;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.FALL_L, (too: TextOrObject) => {
            too.nbFallL++;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.SLEEP, (too: TextOrObject) => {
            too.isSleep = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.UP, (too: TextOrObject) => {
            too.changeMoveDirection = TextOrObject.UP;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.RIGHT, (too: TextOrObject) => {
            too.changeMoveDirection = TextOrObject.RIGHT;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.DOWN, (too: TextOrObject) => {
            too.changeMoveDirection = TextOrObject.DOWN;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.LEFT, (too: TextOrObject) => {
            too.changeMoveDirection = TextOrObject.LEFT;
        });

        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.SWAP, (too: TextOrObject) => {
            too.isSwap = true;
            too.isPush = false;
            too.isStop = false;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.MORE, (too: TextOrObject) => {
            too.isMore = true;
        });

        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.SELECT, (too: TextOrObject) => {
            too.isSelect = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.PATH, (too: TextOrObject) => {
            too.isPath = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, Quality.BONUS, (too: TextOrObject) => {
            too.isBonus = true;
        });
        this._ruleCheckerManager.processMultipleQualities(levelState, rules, RLevel.LEVEL, (too: TextOrObject) => {
            if(too.levelPath === null){
                levelState.savedRules.add(too.name+' '+Verb.IS+' '+RLevel.LEVEL);
                too.levelPath = too.becomeLevelPath;
                too.isLevelHighlight = true;
            }
        });
    }


}