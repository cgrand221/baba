import { LevelState } from "../../Models/LevelState";
import { Movement } from "../../Models/Movement";
import { Rule } from "../../Models/Rule";
import { Verb } from "../../Models/Rules/Verb";
import { TextOrObject } from "../../Models/TextOrObject";
import { TransformManager } from "./TransformManager";

export class DestroyManager {
    private static _instance: DestroyManager | null = null;
    private _transformManager: TransformManager;

    private constructor() {
        this._transformManager = TransformManager.instance;
    }

    public static get instance(): DestroyManager {
        if (!DestroyManager._instance) {
            DestroyManager._instance = new DestroyManager();
        }
        return DestroyManager._instance;
    }

    public destroyAll = (levelState: LevelState, rules: Rule[]):boolean => {
        let change = false;
        if(this.sink(levelState, rules)){
            change = true;
        }        
        /*if(this.boom(levelState, rules, keyCode)){
            change = true;
        }*/        
        if(this.weak(levelState, rules)){
            change = true;
        }
        if(this.hotMeld(levelState,rules)){
            change = true;
        }
        if(this.defeat(levelState, rules)){
            change = true;
        }
        if(this.shutOpen(levelState, rules, [])){
            change = true;
        }
        if(this.selfDestroy(levelState, rules)){
            change = true;
        }
        return change;
    }

    private hotMeld = (levelState: LevelState, rules: Rule[]): boolean => {
        return this.destroyer(
            levelState,
            levelState.textOrObjects.filter(too => too.isMelt),
            levelState.textOrObjects.filter(too => too.isHot),
            false,
            true,
            true,
            rules,
            []
        );
    }

    private defeat = (levelState: LevelState, rules: Rule[]): boolean => {
       
        
        return this.destroyer(
            levelState,
            levelState.textOrObjects.filter(too => too.isYou),
            levelState.textOrObjects.filter(too => too.isDefeat),
            false,
            true,
            true,
            rules,
            []
        );
    }

    private sink = (levelState: LevelState, rules: Rule[]): boolean => {
        return this.destroyer(
            levelState,
            levelState.textOrObjects.filter(too => !too.isGlobalLevel),
            levelState.textOrObjects.filter(too => too.isSink),
            true,
            true,
            false,
            rules,
            []
        );
    }

    public shutOpen = (levelState: LevelState, rules: Rule[], movements: Movement[]): boolean => {
        let opens = levelState.textOrObjects.filter(too => too.isOpen);
        let shuts = levelState.textOrObjects.filter(too => too.isShut);
        let toDestroy:TextOrObject[] = [];
        let cancelMovements: Movement[] = [];
        let tmpShuts = [...shuts];
        tmpShuts.forEach(shut => {
            let openPos = opens.findIndex(
                open => open !== shut && open.isAtSamePlace(shut) && open.shutOpen(shut)
            );
            let open = openPos === -1 ? null : opens[openPos];
            let shutPos = shuts.indexOf(shut);
            if(open && openPos !== -1 && shutPos !== -1){
                opens.splice(openPos, 1);
                shuts.splice(shutPos, 1);
                toDestroy.push(shut);
                toDestroy.push(open);
                let mShut = movements.find(movement => movement.textOrObject === shut);
                let mOpen = movements.find(movement => movement.textOrObject === open);
                if(mShut && !mOpen){
                    cancelMovements.push(mShut);
                }
                else if(!mShut && mOpen){
                    cancelMovements.push(mOpen);
                }
            }
        });
        toDestroy.push(...shuts.filter(shut => shut.isOpen && !toDestroy.includes(shut)));        
        
        return this.doHas(levelState, toDestroy, rules, cancelMovements);
    }

    private weak = (levelState: LevelState, rules: Rule[]) => {
        return this.destroyer(
            levelState,
            levelState.textOrObjects.filter(too => too.isWeak && !too.disableWeak),
            levelState.textOrObjects.filter(too => !too.isGlobalLevel),
            false,
            true,
            false,
            rules,
            []
        );
    }

    public weakOnStop = (levelState: LevelState, movements: Movement[], rules: Rule[]): boolean => {
        let toos = movements.filter(movement => movement.destroy).map(movement => movement.textOrObject);
        return this.weakOnFall(levelState, toos, rules);
    }

    public weakOnFall = (levelState: LevelState, toos: TextOrObject[], rules: Rule[]): boolean => {
        toos.forEach(too => {
            too.disableWeak = true;
        });

        return this.doHas(levelState, toos, rules, []);

    }

    private selfDestroy = (levelState: LevelState, rules: Rule[]): boolean => {
        let selfDestroys = levelState.textOrObjects.filter(too => too.isSelfDestroy)
        return this.doHas(levelState, selfDestroys, rules, []);
    }

    private isDestroySourceAndTarget = (source: TextOrObject, target: TextOrObject, sourceSelfDestroy: boolean): boolean => {
        return (
            source.isAtSamePlace(target)
            && source.isFloat === target.isFloat
            && (
                // target can not destroy itself, unless source is open and source is locked
                sourceSelfDestroy
                || source !== target
            )
        )
    }

    private findToDestroys = (targets: TextOrObject[], sources: TextOrObject[], sourceSelfDestroy: boolean): TextOrObject[] => {
        return targets.filter(
            target => sources.find(source => this.isDestroySourceAndTarget(source, target, sourceSelfDestroy))
        );
    }

    private destroyer = (
        levelState: LevelState,
        targets: TextOrObject[],
        sources: TextOrObject[],
        destroySource: boolean,
        destroyTarget: boolean,
        sourceSelfDestroy: boolean,
        rules: Rule[],
        cancelMovements: Movement[]
    ) => {

        let targetToDestroys: TextOrObject[] = [];
        let sourceToDestroys: TextOrObject[] = [];

        if (destroyTarget) {
            targetToDestroys = this.findToDestroys(targets, sources, sourceSelfDestroy);
            if (destroySource) {
                sourceToDestroys = this.findToDestroys(sources, targetToDestroys, sourceSelfDestroy);
            }
        }

        let change = false;
        if(this.doHas(levelState, targetToDestroys, rules, cancelMovements)){
            change = true;
        }
        if(this.doHas(levelState, sourceToDestroys, rules, cancelMovements)){
            change = true;
        }

        return change;
    }

    private doHas = (levelState: LevelState, textOrObjects: TextOrObject[], rules: Rule[], cancelMovements: Movement[]): boolean => {
        let toos:TextOrObject[]=[];
        if(!levelState.textOrObjects.some(too => too.isGlobalLevel)){
            toos = levelState.textOrObjects;
        }
        else{
            textOrObjects.forEach(too => {
                if(!toos.includes(too)){
                    toos.push(too)!
                }
            })
        }
       
        return this._transformManager.transform(levelState, toos, rules, Verb.HAS, true, false, cancelMovements);
    }
}