import { LevelState } from "../../Models/LevelState";
import { Rule } from "../../Models/Rule";
import { TextOrObject } from "../../Models/TextOrObject";
import { RuleManager } from "../RuleManager";
import { DestroyManager } from "./DestroyManager";
import { TransformManager } from "./TransformManager";

export class MorePlayHoldDestroyColorsManager{

    private static _instance: MorePlayHoldDestroyColorsManager|null = null;
  
    private _transformManager:TransformManager;
    private _ruleManager:RuleManager;
    private _destroyManager:DestroyManager;

    private constructor(){
        this._transformManager = TransformManager.instance
        this._ruleManager = RuleManager.instance
        this._destroyManager = DestroyManager.instance
    }

    public static get instance(): MorePlayHoldDestroyColorsManager
    {
        if(!MorePlayHoldDestroyColorsManager._instance){
            MorePlayHoldDestroyColorsManager._instance = new MorePlayHoldDestroyColorsManager();
        }
        return MorePlayHoldDestroyColorsManager._instance;
    }

    public doWork = (levelState: LevelState, rules:Rule[]): boolean => {
        if(levelState.getCalcHasGlobalLevelTransformed()){
            return false;
        }
        let change = false;
        if(this.more(levelState)){
            change = true;
            this._ruleManager.compileAndCalcProperties(levelState, rules, false);
        }

        /*if(this.play(levelState, rules, keyCode)){
            change = true;
        }

        if(this.hold(levelState, rules, keyCode)){
            change = true;
        }*/
      
        if(this._destroyManager.destroyAll(levelState, rules)){
            change = true;
        }
        
        /*if(this.eat(levelState, rules, keyCode)){
            change = true;
        }*/

        if(this._transformManager.make(levelState, rules)){
            change = true;
        }

        /*if(this.colors(levelState, rules, keyCode)){
            change = true;
        }*/
        
        return change;
    }

    private moreAtXY = (levelState: LevelState, x:number, y:number, more:TextOrObject): TextOrObject[] => {
        if(!levelState
            .getTextAndObjectsAt(x, y)
            .filter(too => !too.isGlobalLevel)
            .some(too => (
                too.name === more.name && too.isObject === more.isObject
                || too.isPush
                || too.isPull
                || too.isStop
            ))
        ){
            return [new TextOrObject(more.name, more.isObject, x, y)];
        }
        return [];
    }

    private more = (levelState: LevelState): boolean => {
        let mores = levelState.textOrObjects.filter(too => too.isMore);
        let change = false;
        mores.forEach(more => {
            let x = more.x-1;
            if(x >= 0){
                levelState.addTextOrObject(...this.moreAtXY(levelState, x, more.y, more));
                change = true;
            }
            x+=2;
            if(x < levelState.level.width){
                levelState.addTextOrObject(...this.moreAtXY(levelState, x, more.y, more));
                change = true;
            }
            let y = more.y-1;
            if(y >= 0){
                levelState.addTextOrObject(...this.moreAtXY(levelState, more.x, y, more));
                change = true;
            }
            y+=2;
            if(y < levelState.level.height){
                levelState.addTextOrObject(...this.moreAtXY(levelState, more.x, y, more));
                change = true;
            }
        });
        return change;
    }

}