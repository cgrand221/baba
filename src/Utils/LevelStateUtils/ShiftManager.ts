import { LevelState } from "../../Models/LevelState";
import { Movement } from "../../Models/Movement";
import { TextOrObject } from "../../Models/TextOrObject";

export class ShiftManager{
    private static _instance: ShiftManager|null = null;

    private constructor(){
    }

    public static get instance(): ShiftManager
    {
        if(!ShiftManager._instance){
            ShiftManager._instance = new ShiftManager();
        }
        return ShiftManager._instance;
    }

    public prepareMovementAndDirs = (levelState: LevelState, movements: Movement[]): boolean => {
        return this.prepareMovementsWithMovementCreate(levelState, movements, this.movementCreate);
    }

    public prepareDirs = (levelState: LevelState): boolean => {
        return this.prepareMovementsWithMovementCreate(levelState, [], this.noMovementCreate);
    }

    private  prepareMovementsWithMovementCreate = (levelState: LevelState, movements: Movement[], movementCreate: (movements: Movement[], too: TextOrObject, dx: number, dy:number) => void): boolean => {
        let shifts = levelState.textOrObjects.filter(too => too.nbShift > 0);
        
        let change = false;
        shifts.forEach(shift => {
            let toos = levelState
                .getTextAndObjectsAt(shift.x, shift.y)
                .filter(too => too !== shift && too.isFloat === shift.isFloat && !too.isGlobalLevel)
                ;
            if(shift.isGlobalLevel){
                toos = levelState.textOrObjects.filter(too => too.isFloat === shift.isFloat && !too.isGlobalLevel);
            }
            let dx = 0;
            let dy = 0;
            switch (shift.moveDirection) {
                case TextOrObject.LEFT:
                    dx = -shift.nbShift;
                    break;
                case TextOrObject.RIGHT:
                    dx = shift.nbShift;
                    break;
                case TextOrObject.UP:
                    dy = -shift.nbShift;
                    break;
                case TextOrObject.DOWN:
                    dy = shift.nbShift;
                    break;
            }

            toos.forEach(too => {
                movementCreate(movements, too, dx, dy);
                if(too.moveDirection !== shift.moveDirection){
                    too.moveDirection = shift.moveDirection;
                    change = true;
                }
            });
        });

        return change;
    }


    private movementCreate = (movements: Movement[], too: TextOrObject, dx: number, dy:number) => {
        let movement = movements.find(movement => movement.textOrObject === too);
        if (undefined === movement) {
            movement = new Movement(too, false);
            movement.dx = 0;
            movement.dy = 0;
            movements.push(movement);
        }
        movement.dx += dx;
        movement.dy += dy;
        return movement;
    }

    private noMovementCreate = (movements: Movement[], too: TextOrObject, dx: number, dy:number) => {
    }
}