import { Level } from "../../Models/Level";
import { LevelState } from "../../Models/LevelState";
import { Movement } from "../../Models/Movement";
import { Rule } from "../../Models/Rule";
import { TextOrObject } from "../../Models/TextOrObject";
import { DestroyManager } from "./DestroyManager";


export class FallManager {
    private static _instance: FallManager | null = null;
    private _destroyManager: DestroyManager;

    private constructor() {
        this._destroyManager = DestroyManager.instance;
    }

    public static get instance(): FallManager {
        if (!FallManager._instance) {
            FallManager._instance = new FallManager();
        }
        return FallManager._instance;
    }

    public doWork = (levelState: LevelState, rules: Rule[]): boolean => {
        if(levelState.getCalcHasGlobalLevelTransformed()){
            return false;
        }
        let change = false;
        let falls = levelState.textOrObjects.filter(too => (
            (too.nbFallD !== too.nbFallU)
            || (too.nbFallR !== too.nbFallL)
        ));
        let needFall = true;
        while (needFall) {
            needFall = false;
            falls.forEach(too => {
                if (this.fall(levelState, rules, too)) {
                    change = true;
                    needFall = true;
                }
            });
        }

        return change;
    }


    private doXOrY = (
        level: Level,
        tooXs: TextOrObject[],
        tooYs: TextOrObject[],
        x: number,
        y: number,
        dx: number,
        dy: number,
        isGlobalLevel: boolean
    ): readonly [boolean, boolean] => {
        let doX = false;
        let doY = false;
        let xMin = 0;
        let xMax = level.width;
        let yMin = 0;
        let yMax = level.height;
        if(isGlobalLevel){
            xMin = -1;
            xMax = level.width+1;
            yMin = -1;
            yMax = level.height+1;
        }
        if ((dy > 0) && (y + dy < yMax) && (isGlobalLevel || tooYs.length === 0)) {// fallD
            doY = true;
        }
        else if ((dx > 0) && (x + dx < xMax) && (isGlobalLevel || tooXs.length === 0)) {// fallR
            doX = true;
        }
        else if ((dy < 0) && (y + dy >= yMin) && (isGlobalLevel || tooYs.length === 0)) {// fallU
            doY = true;
        }
        else if ((dx < 0) && (x + dx >= xMin) && (isGlobalLevel || tooXs.length === 0)) {// fallL
            doX = true;
        }

        return [doX, doY];
    }

    private fall = (levelState: LevelState, rules: Rule[], too: TextOrObject): boolean => {

        
        let x = too.x;
        let y = too.y;

        let doX = false;
        let doY = false;

        let dx = Math.sign(too.nbFallR - too.nbFallL);
        let dy = Math.sign(too.nbFallD - too.nbFallU);
        let tooShutOpenX: TextOrObject | null = null;
        let tooShutOpenY: TextOrObject | null = null;
       
        let allWeaks: TextOrObject[] = [];
        do {
            let tooXs = levelState.getTextAndObjectsAt(x + dx, y).filter(too => !too.isGlobalLevel).filter(too => too.isPush || too.isPull || too.isStop);
            let tooYs = levelState.getTextAndObjectsAt(x, y + dy).filter(too => !too.isGlobalLevel).filter(too => too.isPush || too.isPull || too.isStop);
            tooShutOpenX = tooXs.find(tooX => tooX.shutOpen(too)) ?? null;
            tooShutOpenY = tooYs.find(tooY => tooY.shutOpen(too)) ?? null;

            [doX, doY] = this.doXOrY(
                levelState.level, 
                tooXs.filter(tooX => tooX !== tooShutOpenX), 
                tooYs.filter(tooY => tooY !== tooShutOpenY), 
                x,
                y, 
                dx, 
                dy,
                too.isGlobalLevel
            );

            if (doX) {
                let weaks = tooXs.filter(too => too.isWeak);
                allWeaks.push(...weaks);               
                x += dx;
                if(tooShutOpenX){
                    doX = false;
                }
            }
            else if (doY) {
                let weaks = tooYs.filter(too => too.isWeak);
                allWeaks.push(...weaks);
                y += dy;
                if(tooShutOpenY){
                    doY = false;
                }
            }

        } while (doX || doY);

        let change = ((x !== too.x) || (y !== too.y));
        levelState.applyChangeXY(too, () => {
            too.x = x;
            too.y = y;
        })
        
        if (this._destroyManager.shutOpen(levelState, rules, [])) {
            change = true;
        }
        if (this._destroyManager.weakOnFall(levelState, allWeaks, rules)) {
            change = true;
        }
        return change;
    }
}