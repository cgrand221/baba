import { KeyCode } from "../../Models/KeyCode";
import { LevelState } from "../../Models/LevelState";
import { Movement } from "../../Models/Movement";
import { Rule } from "../../Models/Rule";
import { TextOrObject } from "../../Models/TextOrObject";
import { DestroyManager } from "./DestroyManager";
import { RuleManager } from "../RuleManager";
import { ShiftManager } from "./ShiftManager";

export class MoveManager {

    private static _instance: MoveManager | null = null;
    private _destroyManager: DestroyManager;
    private _ruleManager: RuleManager;
    private _shiftManager: ShiftManager;

    private constructor() {
        this._destroyManager = DestroyManager.instance
        this._ruleManager = RuleManager.instance
        this._shiftManager = ShiftManager.instance
    }

    public static get instance(): MoveManager {
        if (!MoveManager._instance) {
            MoveManager._instance = new MoveManager();
        }
        return MoveManager._instance;
    }

    private doOneRotation = (
        currentMovement: Movement, saveMovementDx: number, saveMovementDy: number, simulatedMovements: Movement[]
    ): boolean => {
       
        let moveDirection:number|null = null;
        if (saveMovementDx > 0 && currentMovement.textOrObject.moveDirection !== TextOrObject.RIGHT) {            
            moveDirection = TextOrObject.RIGHT;
        }
        else if (saveMovementDx < 0 && currentMovement.textOrObject.moveDirection !== TextOrObject.LEFT) {           
            moveDirection = TextOrObject.LEFT;
        }
        if (saveMovementDy > 0 && currentMovement.textOrObject.moveDirection !== TextOrObject.DOWN) {            
            moveDirection = TextOrObject.DOWN;
        }
        else if (saveMovementDy < 0 && currentMovement.textOrObject.moveDirection !== TextOrObject.UP) {            
            moveDirection = TextOrObject.UP;
        }
        if(moveDirection){
            let simulatedMovement = this.getOrGenerateSimulatedMovement(simulatedMovements, currentMovement.textOrObject);
            simulatedMovement.moveDirection = moveDirection;
            return true;
        }
        return false;
    }

    private doRealOneMovement = (
        simulatedMovements: Movement[],
        levelState: LevelState,
        rules: Rule[]
    ): boolean => {
        let change = this._destroyManager.weakOnStop(levelState, simulatedMovements, rules);
        simulatedMovements.forEach(simulatedMovement => {
            let too = simulatedMovement.textOrObject;
            if (
                too.moveDirection !== simulatedMovement.moveDirection
                && !too.isGlobalLevel
            ) {
                change = true;
                too.moveDirection = simulatedMovement.moveDirection;
            }
            if (simulatedMovement.dx !== 0) {
                change = true;
                levelState.applyChangeXY(too, () => {
                    too.x += simulatedMovement.dx;
                });
            }
            if (simulatedMovement.dy !== 0) {
                change = true;
                levelState.applyChangeXY(too, () => {
                    too.y += simulatedMovement.dy;
                });
            }
        });
        this._destroyManager.shutOpen(levelState, rules, simulatedMovements);
        return change;
    }


    private doOneSwap = (
        levelState: LevelState,
        currentMovement: Movement,
        movementToos: TextOrObject[],
        simulatedMovements: Movement[]
    ):boolean => {
        let x = currentMovement.textOrObject.x;
        let y = currentMovement.textOrObject.y;
        let dx = Math.sign(currentMovement.dx);
        let dy = Math.sign(currentMovement.dy);

        let newX = x+dx;
        let newY = y+dy;
        if(
            this.isInside(levelState, newX, newY)
        ){
            this.generateSimulatedMovement(simulatedMovements, currentMovement.textOrObject, dx, dy, true);

            let toos = levelState.getTextAndObjectsAt(newX, newY).filter(too => !too.isGlobalLevel);
            toos.forEach(too => {
                this.generateSimulatedMovement(simulatedMovements, too, -dx, -dy, true);
            });
        }
        else if (this.applyDirectionChange(currentMovement)) {
            return false;
        }
        
        currentMovement.dx -= dx;
        currentMovement.dy -= dy;

        return true;
    }

    private doOneMovementOrSwap = (
        movement: Movement,
        levelState: LevelState,
        movementToos: TextOrObject[],
        simulatedMovements: Movement[],
    ) => {
        const doOneMovementOrSwap =  movement.textOrObject.isSwap ? this.doOneSwap : this.doOneMovement;
        let saveMovementDx = movement.dx;
        let saveMovementDy = movement.dy;
        let change = false;
        let allowChangeDirection = movement.allowChangeDirection;
        
        change = doOneMovementOrSwap(levelState, movement, movementToos, simulatedMovements);
        if (
            allowChangeDirection && !movement.allowChangeDirection && !change
        ) {
            change = doOneMovementOrSwap(levelState, movement, movementToos, simulatedMovements);
        }
        if(!change){
            this.doOneRotation(movement, saveMovementDx, saveMovementDy, simulatedMovements);
        }
    }

    private doAllMovements = (movements: Movement[], levelState: LevelState, movementToos: TextOrObject[], rules: Rule[]): boolean => {
        let change = false;
        movements = movements.filter(movement => movement.dx !== 0 || movement.dy !== 0);

        while (movements.length > 0) {
            let simulatedMovements: Movement[] = [];
            let tmpMovements: Movement[] = [];
          
            movements.forEach((movement) => {                
                this.doOneMovementOrSwap(  
                    movement,
                    levelState,
                    movementToos,
                    simulatedMovements
                );
                if (
                    movement.dx != 0
                    || movement.dy !== 0

                ) {
                    tmpMovements.push(movement);
                }
            });

            if (this.doRealOneMovement(simulatedMovements, levelState, rules)) {
                change = true;
            }

            movements = tmpMovements;
        }

        return change;
    }

    private shutOpens = (
        lastToos: TextOrObject[],
        toos: TextOrObject[],
    ): boolean => {
        if (
            lastToos.some(
                lastToo => (
                    lastToo.isPush
                    && !lastToo.isShut 
                    && !lastToo.isOpen
                )
            )
        ) {
            return false;
        }
    
        const lastToRemoveIndices: number[] = [];
        const toosToRemoveIndices: number[] = [];
    
        toos.forEach((too, tooPos) => {
            const lastTooPos = lastToos.findIndex(lastToo => lastToo.shutOpen(too));
            
            if (lastTooPos !== -1) {
                lastToRemoveIndices.push(lastTooPos);
                toosToRemoveIndices.push(tooPos);
            }
        });
    
        lastToRemoveIndices.reverse().forEach(pos =>  lastToos.splice(pos, 1))
        toosToRemoveIndices.reverse().forEach(pos =>  toos.splice(pos, 1))
    
        return (lastToRemoveIndices.length > 0 || toosToRemoveIndices.length > 0);
    }

    private getOrGenerateSimulatedMovement = (simulatedMovements: Movement[], textOrObject: TextOrObject): Movement => {
        let simulatedMovement = simulatedMovements.find(movement => movement.textOrObject === textOrObject);
        if (!simulatedMovement) {
            simulatedMovement = new Movement(textOrObject, false);
            simulatedMovement.dx = 0;
            simulatedMovement.dy = 0;
            simulatedMovement.moveDirection = textOrObject.moveDirection;
            simulatedMovements.push(simulatedMovement);
        }
        return simulatedMovement;
    }

    private getPushWeaks = (textOrObjects: TextOrObject[], movementToos: TextOrObject[], allowChangeDirection: boolean): readonly [TextOrObject[], TextOrObject[]] => {
        let x = -1;
        let y = -1;

        let pushes: TextOrObject[] = [];
        let lastWeaks: TextOrObject[] = [];
        let tmpPushes: TextOrObject[] = [];
        textOrObjects.forEach(textOrObject => {
            if (
                x !== textOrObject.x
                || y !== textOrObject.y

            ) {
                x = textOrObject.x;
                y = textOrObject.y;
                let toos = textOrObjects.filter(too => too.isAtSamePlace(textOrObject));
                let weaks = toos.filter(too => too.isWeak && (!allowChangeDirection || !movementToos.includes(too)));

                if (toos.length === weaks.length) {
                    pushes = tmpPushes
                    lastWeaks = weaks;
                }

                tmpPushes = tmpPushes.concat(toos);
            }

        });

        return [pushes, lastWeaks];

    }

    private allowPull = (
        levelState: LevelState,
        pull: TextOrObject,
        dx: number,
        dy: number,
        movementToos: TextOrObject[],
    ): boolean => {

        let x = pull.x;
        let y = pull.y;
        let isPUllAndYouAtXY = movementToos.some(mt => mt.x === x && mt.y === y);
        x += dx;
        y += dy;

        if (
            movementToos.some(mt => mt.x === x && mt.y === y) // pull, you
            || (isPUllAndYouAtXY && levelState.getTextAndObjectsAt(x, y).some(too => too.isPush && !too.isGlobalLevel))// [pull, you], push
        ) {
            return true;
        }

        let allowPull = false;
        let tmpPull: TextOrObject | null = null;
        // we stop we do not have pull or when we have you
        do {
            allowPull = movementToos.some(mt => mt.x === x && mt.y === y);
            tmpPull = levelState.getTextAndObjectsAt(x, y).find(too => too.isPull && !too.isGlobalLevel) ?? null;

            x += dx;
            y += dy;
        } while (tmpPull && !allowPull);

        return allowPull;
    }


    private preparePull = (levelState: LevelState, currentMovement: Movement): TextOrObject => {
        let headToo = currentMovement.textOrObject;
        let x = headToo.x;
        let y = headToo.y;

        let dx = Math.sign(currentMovement.dx);
        let dy = Math.sign(currentMovement.dy);

        let too: TextOrObject | undefined = headToo;
        do {
            headToo = too;
            x -= dx;
            y -= dy;
            too = levelState.getTextAndObjectsAt(x, y).find(too => too.isPull && !too.isGlobalLevel)
        } while (too);

        return headToo;
    }

    private calcCurrentPushes = (
        levelState: LevelState,
        toos: TextOrObject[],
        headToos: TextOrObject[],
        dx: number,
        dy: number,
        movementToos: TextOrObject[],
        allPushes: TextOrObject[],
        lastToos: TextOrObject[]
    ): TextOrObject[] => {
        let retour = toos.filter(
            too => (
                (headToos.includes(too))
                // we are allowed to pull, only if after a list of pull we have you : 
                || ((too.isPull) && this.allowPull(levelState, too, dx, dy, movementToos))
                || (
                    (allPushes.length > 0)
                    && (
                        too.isPush
                        || (too.isStop && !too.isWeak && movementToos.includes(too))
                        || ((lastToos.some(too => too.isPull)) && movementToos.includes(too))
                    )
                )
            )
        );


        return retour;
    }

    private isInside = (
        levelState: LevelState,
        x: number,
        y: number,
    ):boolean => {
        return (
            x >= 0
            && x < levelState.level.width
            && y >= 0
            && y < levelState.level.height
        );
    }

    private calcStop = (
        levelState: LevelState,
        toos: TextOrObject[],
        isHeadToo: boolean,
        x: number,
        y: number,
        lastToos: TextOrObject[],
        currentPushes: TextOrObject[]
    ): boolean => {
        if (!isHeadToo) {
            return (
                !this.isInside(levelState, x, y)
                || (
                    // if keys all disapeared, there is free space, so stop is not true
                    lastToos.length !== 0
                    && toos.some(too => ((too.isStop || too.isPull) && !too.isWeak && !too.isSwap) && !currentPushes.includes(too))
                )
            );
        }
        return false;
    }


    private cancelStopPull = (
        isStop: boolean,
        isInPull: boolean,
        currentPushes: TextOrObject[],
        allPushes: TextOrObject[],
        lastToos: TextOrObject[]
    ): boolean => {
        if (
            isStop && isInPull
        ) {
            // we cancel stop and we reset allPushes
            let too = currentPushes.find(too => too.isPull);
            if (too) {
                allPushes.length = 0;
                currentPushes.length = 0;
                currentPushes.push(too);
                isStop = false;
                lastToos.length = 0;
            }
        }

        return isStop;
    }

    private calcIsInPull = (
        x: number,
        y: number,
        dx: number,
        dy: number,
        limitPullX: number,
        limitPullY: number
    ): boolean => {
        return (
            (dx > 0 && x <= limitPullX)
            || (dy > 0 && y <= limitPullY)
            || (dx < 0 && x >= limitPullX)
            || (dy < 0 && y >= limitPullY)
        );
    }

    private doOnePushPull = (
        levelState: LevelState,
        currentMovement: Movement,
        movementToos: TextOrObject[],
        allPushes: TextOrObject[],
        allSwaps: TextOrObject[],
    ): boolean => {
        let limitPullX = currentMovement.textOrObject.x;
        let limitPullY = currentMovement.textOrObject.y;
        let dx = Math.sign(currentMovement.dx);
        let dy = Math.sign(currentMovement.dy);
        let headToos: TextOrObject[] = [currentMovement.textOrObject];
        let headToo = this.preparePull(levelState, currentMovement);

        if (headToo !== currentMovement.textOrObject) {
            headToos.push(headToo);
        }

        let x = headToo.x;
        let y = headToo.y;
        let isStop = false;
        let continueOnNext = true;
        let lastToos: TextOrObject[] = [];
        // next you have we push or pull elements
        while (
            continueOnNext
        ) {
            let toos = levelState.getTextAndObjectsAt(x, y).filter(too => !too.isGlobalLevel);
            let changeShutOpen = this.shutOpens(lastToos, toos);

            let isHeadToo = toos.some(too => headToos.includes(too));
            if (changeShutOpen) {
                currentMovement.allowChangeDirection = false;
            }

            let currentPushes = this.calcCurrentPushes(levelState, toos, headToos, dx, dy, movementToos, allPushes, lastToos);

            let isInPull = this.calcIsInPull(x, y, dx, dy, limitPullX, limitPullY);
            if(!isHeadToo){
                allSwaps.push(...toos.filter(too => too.isSwap))
            }

            isStop = this.calcStop(levelState, toos, isHeadToo, x, y, lastToos, currentPushes);
            lastToos = toos;
            isStop = this.cancelStopPull(isStop, isInPull, currentPushes, allPushes, lastToos);


            if (!isStop) {
                allPushes.push(...currentPushes);
            }
            x += dx;
            y += dy;
            continueOnNext = (currentPushes.length > 0 && !isStop);
        }
        return isStop;
    }

    private generateSimulatedMovement = (simulatedMovements: Movement[], textOrObject: TextOrObject, dx: number, dy: number, changeMoveDirection: boolean): void => {
        let simulatedMovement = this.getOrGenerateSimulatedMovement(simulatedMovements, textOrObject);
        let moveDirection = simulatedMovement.moveDirection;
        if (dx > 0) {
            moveDirection = TextOrObject.RIGHT;
            simulatedMovement.dx = 1;
        }
        else if (dx < 0) {
            moveDirection = TextOrObject.LEFT;
            simulatedMovement.dx = -1;
        }
        else if (dy > 0) {
            moveDirection = TextOrObject.DOWN;
            simulatedMovement.dy = 1;
        }
        else if (dy < 0) {
            moveDirection = TextOrObject.UP;
            simulatedMovement.dy = -1;
        }
        if(changeMoveDirection){
            simulatedMovement.moveDirection = moveDirection;
        }
    }

    private applyDirectionChange = (
        currentMovement: Movement
    ): boolean => {
        if (currentMovement.allowChangeDirection) {
            currentMovement.allowChangeDirection = false;
            currentMovement.textOrObject.moveDirection = -currentMovement.textOrObject.moveDirection;
            currentMovement.dx = -currentMovement.dx;
            currentMovement.dy = -currentMovement.dy;
            return true;
        }
        return false;
    }
    // return true if object or text at x,y can be moved like dx, dy
    private doOneMovement = (
        levelState: LevelState,
        currentMovement: Movement,
        movementToos: TextOrObject[],
        simulatedMovements: Movement[]
    ): boolean => {

        let dx = Math.sign(currentMovement.dx);
        let dy = Math.sign(currentMovement.dy);

        if(currentMovement.textOrObject.isGlobalLevel){
            this.generateSimulatedMovement(simulatedMovements, currentMovement.textOrObject, dx, dy, true);
            currentMovement.dx -= dx;
            currentMovement.dy -= dy;
            return true;
        }

        // allPushes : all objects which need to pushed      
        let allPushes: TextOrObject[] = [];
        let allSwaps: TextOrObject[] = [];
        let isStop = this.doOnePushPull(levelState, currentMovement, movementToos, allPushes, allSwaps);

        if (isStop) {
            let lastWeaks: TextOrObject[] = [];
            [allPushes, lastWeaks] = this.getPushWeaks(allPushes, movementToos, currentMovement.allowChangeDirection);

            if (lastWeaks.length === 0) {
                if (this.applyDirectionChange(currentMovement)) {
                    return false;
                }

                currentMovement.dx = 0;
                currentMovement.dy = 0;
                return false;
            }

            lastWeaks.forEach(lastWeak => {
                let simulatedMovement = this.getOrGenerateSimulatedMovement(simulatedMovements, lastWeak);

                simulatedMovement.destroy = true;
            });
        }


        currentMovement.dx -= dx;
        currentMovement.dy -= dy;

        allPushes.forEach(textOrObject => {
            this.generateSimulatedMovement(simulatedMovements, textOrObject, dx, dy, true);
        });
        allSwaps
            .filter(textOrObject => !allPushes.includes(textOrObject))
            .forEach(textOrObject => {
                this.generateSimulatedMovement(simulatedMovements, textOrObject, -dx, -dy, false);
            })
            ;
        allPushes
            .filter(textOrObject => textOrObject.isSwap)
            .forEach(textOrObject => {
                let toos = levelState.getTextAndObjectsAt(textOrObject.x, textOrObject.y).filter(too => !too.isGlobalLevel);
                toos
                    .filter(textOrObject => !textOrObject.isSwap)
                    .forEach(too => {
                        this.generateSimulatedMovement(simulatedMovements, too, -dx, -dy, true);
                    });
            })
            ;
        return true;
    }

    public doWork = (levelState: LevelState, rules: Rule[], keyCode: KeyCode): boolean => {
        let change = false;


        levelState.startMovement();

        // todo : do real movement between steps :
        if (this.youAndyou2Andyou3(levelState, keyCode, rules)) {
            change = true;
            this._ruleManager.compileAndCalcProperties(levelState, rules, false);
        };
        if (this.moveAndAutoAndChill(levelState, rules)) {
            change = true;
            this._ruleManager.compileAndCalcProperties(levelState, rules, false);
        }
        //this.nudgeRnudgeUnudgeLnudgeD(levelState, keyCode);
        if (this.fearShift(levelState, rules)) { // shift: main movement
            change = true;
            this._ruleManager.compileAndCalcProperties(levelState, rules, false);
        }

        levelState.endMovement();
        return change;
    }

    private fearShift = (levelState: LevelState, rules: Rule[]): boolean => {
        let movements: Movement[] = []
        let change = this._shiftManager.prepareMovementAndDirs(levelState, movements);
        if (this.doAllMovements(
            movements,
            levelState,
            movements.map(movement => movement.textOrObject),
            rules,
        )) {
            change = true;
        }
        return change
    }

    private transformKeyToMovements = (movements: Movement[], keyCode: KeyCode) => {
        if (movements.length > 0) {
            let dx = 0;
            let dy = 0;

            switch (keyCode) {
                case KeyCode.KEY_LEFT:
                    dx = -1;
                    break;
                case KeyCode.KEY_RIGHT:
                    dx = 1;
                    break;
                case KeyCode.KEY_UP:
                    dy = -1;
                    break;
                case KeyCode.KEY_DOWN:
                    dy = 1;
                    break;
            }

            movements.forEach(movement => {
                movement.dx += dx;
                movement.dy += dy;
            });
        }
    }

    private youAndyou2Andyou3 = (levelState: LevelState, keyCode: KeyCode, rules: Rule[]): boolean => {
        let movements: Movement[] = [];
        let youToos = levelState.textOrObjects.filter(too => too.isYou && !too.isSleep)
        youToos.forEach(youToo => {
            movements.push(new Movement(youToo, false));
        });
        this.transformKeyToMovements(movements, keyCode);

        return this.doAllMovements(
            movements,
            levelState,
            youToos,
            rules,
        );
    }

    private moveAndAutoAndChill = (levelState: LevelState, rules: Rule[]): boolean => {
        let movements: Movement[] = [];
        let moveToos = levelState.textOrObjects.filter(too => too.nbMoves > 0);
        moveToos.forEach(moveToo => {
            let movement = new Movement(moveToo, true);
            movements.push(movement);

            switch (moveToo.moveDirection) {
                case TextOrObject.LEFT:
                    movement.dx -= moveToo.nbMoves;
                    break;
                case TextOrObject.RIGHT:
                    movement.dx += moveToo.nbMoves;
                    break;
                case TextOrObject.UP:
                    movement.dy -= moveToo.nbMoves;
                    break;
                case TextOrObject.DOWN:
                    movement.dy += moveToo.nbMoves;
                    break;
            }
        });

        return this.doAllMovements(
            movements,
            levelState,
            moveToos,
            rules
        );
    }
}