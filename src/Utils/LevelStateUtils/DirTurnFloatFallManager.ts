import { LevelState } from "../../Models/LevelState";
import { Rule } from "../../Models/Rule";
import { TextOrObject } from "../../Models/TextOrObject";
import { FallManager } from "./FallManager";

export class DirTurnFloatFallManager{

    private static _instance: DirTurnFloatFallManager|null = null;
    private fallManager:FallManager;

    private constructor(){
        this.fallManager = FallManager.instance
    }

    public static get instance(): DirTurnFloatFallManager
    {
        if(!DirTurnFloatFallManager._instance){
            DirTurnFloatFallManager._instance = new DirTurnFloatFallManager();
        }
        return DirTurnFloatFallManager._instance;
    }

    public doWork = (levelState: LevelState, rules:Rule[]): boolean => {
        if(levelState.getCalcHasGlobalLevelTransformed()){
            return false;
        }
        let change = false;
        if(this.leftAndUpAndRightAndDown(levelState, rules)){
            change = true;
        }
        /*if(this.turnClockwiseAndcounterClockwise(levelState, rules, keyCode)){
            change = true;
        }
        if(this.float(levelState, rules, keyCode)){
            change = true;
        }*/
        
        if(this.fallManager.doWork(levelState, rules)){
            change = true;
        }
        
        return change;
    }

    public leftAndUpAndRightAndDown = (levelState: LevelState, rules:Rule[]): boolean => {
        let change = false;
        levelState.textOrObjects.forEach(too => {
            if(too.changeMoveDirection){
                if(too.moveDirection !== too.changeMoveDirection){
                    change = true;
                    too.moveDirection = too.changeMoveDirection;
                }
            }
        })
        return change;
    }
}