import { LevelState } from "../../Models/LevelState";
import { Rule } from "../../Models/Rule";
import { RLevel } from "../../Models/Rules/RLevel";
import { Text } from "../../Models/Rules/Text";
import { Verb } from "../../Models/Rules/Verb";
import { SaveManager } from "../SaveManager";

export class EndWinBonusManager{

    private static _instance: EndWinBonusManager|null = null;
    private _saveManager: SaveManager;

    private constructor(){
        this._saveManager = SaveManager.instance;
    }

    public static get instance(): EndWinBonusManager
    {
        if(!EndWinBonusManager._instance){
            EndWinBonusManager._instance = new EndWinBonusManager();
        }
        return EndWinBonusManager._instance;
    }

    public doWork = (levelState: LevelState, rules: Rule[]): void => { 
        
        let yous = levelState.textOrObjects.filter(too => too.isYou);
        let selects = levelState.textOrObjects.filter(too => too.isSelect);
        

        if(levelState.state === LevelState.STATE_IS_INFINITE_LOOP){
            return;
        }
        const globalLevels = levelState
            .textOrObjects
            .filter(too => too.isGlobalLevel && (!too.isObject || too.name !== RLevel.LEVEL))
            ;
        levelState.parentSavedRules.clear();
        globalLevels.forEach(globalLevel => {
            levelState.state = LevelState.STATE_IS_NEW_PARENT_RULES;
            
            if(globalLevel.isObject){ 
                // level is flag
                levelState.parentSavedRules.add(RLevel.LEVEL+' '+Verb.IS+' '+globalLevel.name);
            }
            else if(globalLevel.name === RLevel.LEVEL){
                // level is text => level is text
                levelState.parentSavedRules.add(RLevel.LEVEL+' '+Verb.IS+' '+Text.TEXT);
            }
            else{
                // level write baba => level write baba
                levelState.parentSavedRules.add(RLevel.LEVEL+' '+Verb.WRITE+' '+globalLevel.name);
            }
        });
        if(globalLevels.length > 0){
           return; 
        }
       
        if (
            yous.length === 0
            && selects.length === 0
        ) {
            levelState.state = LevelState.STATE_CAN_NOT_PLAY;
            return;
        }
        if (
            yous.some(you => levelState.getTextAndObjectsAt(you.x, you.y).some(too => too.isWin && you.isFloat === too.isFloat))
        ) {
            levelState.state = LevelState.STATE_IS_WIN;
            return;
        }
        if (
            yous.some(you => levelState.getTextAndObjectsAt(you.x, you.y).some(too => too.isBonus && you.isFloat === too.isFloat))
        ) {
            levelState.state = LevelState.STATE_IS_BONUS;
            return;
        }
       
    }

    public zoneCompletedState = (levelState: LevelState): void => { 
        if(this._saveManager.hasLevelZoneCompleted(levelState)){
            levelState.state = LevelState.STATE_IS_ZONE_COMPLETED;
        }
    }
}