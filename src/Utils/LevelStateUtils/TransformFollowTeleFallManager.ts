
import { KeyCode } from "../../Models/KeyCode";
import { LevelState } from "../../Models/LevelState";
import { Rule } from "../../Models/Rule";
import { Verb } from "../../Models/Rules/Verb";
import { TextOrObject } from "../../Models/TextOrObject";
import { DestroyManager } from "./DestroyManager";
import { ShiftManager } from "./ShiftManager";
import { TransformManager } from "./TransformManager";

export class TransformFollowTeleFallManager {

    private static _instance: TransformFollowTeleFallManager | null = null;
    private _transformManager: TransformManager;
    private _shiftManager: ShiftManager;
    private _destroyManager:DestroyManager;

    private constructor() {
        this._destroyManager = DestroyManager.instance
        this._transformManager = TransformManager.instance;
        this._shiftManager = ShiftManager.instance;
    }



    public static get instance(): TransformFollowTeleFallManager {
        if (!TransformFollowTeleFallManager._instance) {
            TransformFollowTeleFallManager._instance = new TransformFollowTeleFallManager();
        }
        return TransformFollowTeleFallManager._instance;
    }

    public doWork = (levelState: LevelState, rules: Rule[], keyCode: KeyCode): boolean => {
        if(levelState.getCalcHasGlobalLevelTransformed()){
            return false;
        }
        let change = false;

        if(this.transformAndRevert(levelState, rules)){
            change = true;
        }

        if (this.select(levelState, keyCode)){
            change = true;
        }
        
        if(this._destroyManager.destroyAll(levelState, rules)){
            change = true;
        }
       
        /*if(this.followBack(levelState, rules)){
            change = true;
        }*/

       
        if(this.teleShiftDir(levelState, keyCode)){// shift: changing direction
            change = true;
        }
        

        return change;
    }

    private transformAndRevert = (levelState: LevelState, rules: Rule[]): boolean => {
        let change = false;
        
        if(this._transformManager.transform(levelState, levelState.textOrObjects, rules, Verb.IS, false, false, [])){
            change = true;
        }
        if(this._transformManager.transform(levelState, levelState.textOrObjects, rules, Verb.WRITE, false, false, [])){
            change = true;
        }

        return change;
    }

    private teleShiftDir = (levelState: LevelState, keyCode: KeyCode): boolean => {
        let change = false;
        if (this.tele(levelState)){
            change = true;
        }
        if (this._shiftManager.prepareDirs(levelState)){
            change = true;
        }
        
        return change;
    }

    private tele = (levelState: LevelState): boolean => {
        let sources = levelState.textOrObjects.filter(too => too.isTele);

        let excludes:TextOrObject[] = [];
        let change = false;
        sources.forEach(source => {
            let toTeles = levelState
                .getTextAndObjectsAt(source.x, source.y)
                .filter(too => 
                    (
                        (
                            too.name !== source.name 
                            || too.isObject !== source.isObject 
                        )
                        && too.isFloat == source.isFloat
                        && !too.isGlobalLevel
                        && !excludes.includes(too)
                    )
                );
            let destinations = sources.filter(src => (
                src !== source
                && (
                    (src.isObject && src.isObject === source.isObject && src.name === source.name)// baba is tele
                    || (!src.isObject && src.isObject === source.isObject)// text is tele
                )
            ));
            toTeles.forEach(toTele => {
                let destination = levelState.randomValue(destinations);
                if (destination) {
                    levelState.applyChangeXY(toTele, () => {
                        toTele.x = destination.x;
                        toTele.y = destination.y;
                    });
                  
                    change = true;
                }
            });
            excludes = excludes.concat(toTeles);
        });

        return change;
    }

    public select = (levelState: LevelState, keyCode: KeyCode): boolean => {
        let selects = levelState.textOrObjects.filter(too => too.isSelect);

        let dx = 0;
        let dy = 0;
        let change = false;
    
        switch (keyCode) {
            case KeyCode.KEY_LEFT:
                dx = -1;
                break;
            case KeyCode.KEY_RIGHT:
                dx = 1;
                break;
            case KeyCode.KEY_UP:
                dy = -1;
                break;
            case KeyCode.KEY_DOWN:
                dy = 1;
                break;
        }

        if(dx !== 0 || dy !== 0){
            selects.forEach(too => {
                let newX = too.x + dx;
                let newY = too.y + dy;
                if( levelState.getTextAndObjectsAt(newX, newY).some(too => (
                    (too.isPath && too.viewState !== TextOrObject.VIEW_STATE_LOCKED)
                    || (null !== too.levelPath && too.viewState !== TextOrObject.VIEW_STATE_LOCKED)
                ))){
                    change = true;
                    levelState.applyChangeXY(too, () => {
                        too.x = newX;
                        too.y = newY;
                    });
                }                
            });
            
        }
    
        return change;
    }
}