import { LevelState } from "../../Models/LevelState";
import { Movement } from "../../Models/Movement";
import { Rule } from "../../Models/Rule";
import { All } from "../../Models/Rules/All";
import { Group } from "../../Models/Rules/Group";
import { RLevel } from "../../Models/Rules/RLevel";
import { Text } from "../../Models/Rules/Text";
import { Verb } from "../../Models/Rules/Verb";
import { TextOrObject } from "../../Models/TextOrObject";
import { RuleCheckerManager } from "../RuleCheckerManager";

export class TransformManager {
    private static _instance: TransformManager | null = null;
    private _ruleCheckerManager: RuleCheckerManager;
    private _allowLevelTransformation: boolean;

    private constructor() {
        this._ruleCheckerManager = RuleCheckerManager.instance;
        this._allowLevelTransformation = false;
    }

    public init = (allowLevelTransformation: boolean) => {
        this._allowLevelTransformation = allowLevelTransformation;
    }

    public static get instance(): TransformManager {
        if (!TransformManager._instance) {
            TransformManager._instance = new TransformManager();
        }
        return TransformManager._instance;
    }

    private simplify = (tooToRemoves: TextOrObject[], tooToAdds: TextOrObject[]) => {
        const newTooToRemoves = tooToRemoves.filter(tooToRemove => !tooToAdds.some(tooToAdd => (
            tooToAdd.name === tooToRemove.name
            && tooToAdd.isObject === tooToRemove.isObject
            // do not use tooToAdd.isAtSamePlace
            && tooToAdd.x === tooToRemove.x
            && tooToAdd.y === tooToRemove.y
        )));
        const newTooToAdds = tooToAdds.filter(tooToAdd => !tooToRemoves.some(tooToRemove => (
            tooToAdd.name === tooToRemove.name
            && tooToAdd.isObject === tooToRemove.isObject
            // do not use tooToAdd.isAtSamePlace
            && tooToAdd.x === tooToRemove.x
            && tooToAdd.y === tooToRemove.y
        )));

        tooToRemoves.length = 0;
        tooToAdds.length = 0;
        tooToRemoves.push(...newTooToRemoves);
        tooToAdds.push(...newTooToAdds);
    }

    private cancelMovementForToo = (levelState: LevelState, tooToRemoves: TextOrObject[], textOrObject: TextOrObject, cancelMovements: Movement[]) => {
        // we remove the old object
        if (tooToRemoves.includes(textOrObject)) {
            return;
        }
        tooToRemoves.push(textOrObject);
        let cancelMovement = cancelMovements.find(cancelMovement => cancelMovement.textOrObject === textOrObject);
        if (cancelMovement) {
            levelState.applyChangeXY(textOrObject, () => {
                textOrObject.x -= cancelMovement.dx;
                textOrObject.y -= cancelMovement.dy;
            })
            
        }
    }

    private addToAdds = (levelState: LevelState, isMake: boolean, textOrObject: TextOrObject, newTextOrObject: TextOrObject, tooToAdds: TextOrObject[]) => {
        if (
            !isMake
            || !(
                levelState
                    .getTextAndObjectsAt(newTextOrObject.x, newTextOrObject.y)
                    .some(too => (
                        too.name === newTextOrObject.name
                        && too.isObject === newTextOrObject.isObject
                    ))
            )

        ) {
            tooToAdds.push(newTextOrObject);
            textOrObject.hNext.push(newTextOrObject);
            newTextOrObject.hPrevious = textOrObject;
        }
    }

    public transform = (
        levelState: LevelState,
        textOrObjects: TextOrObject[],
        rules: Rule[],
        verb: string,
        forceToRemove: boolean,
        isMake: boolean,
        cancelMovements: Movement[]
    ): boolean => {
        let objectAndTexts = [Text.TEXT].concat(levelState.getWordsObjectToMatch());
        let ruleTransforms = rules;

        let tooToAdds: TextOrObject[] = [];
        let tooToRemoves: TextOrObject[] = [];
        let isWrite = verb === Verb.WRITE;

        // for all objects of levelState
        textOrObjects.forEach(textOrObject => {
            textOrObject.hNext = [];
            ruleTransforms.forEach(ruleTransform => {
                if(ruleTransform.verb !== verb){
                    return;
                }
                ruleTransform.lefts.forEach(left => {
                    if (
                        // todo condition ?
                        this._ruleCheckerManager.isbasicLeftChecked(left.content, left.not, textOrObject.isObject, textOrObject.nbGroup, textOrObject.name, textOrObject.isLevel)
                    ) {
                        ruleTransform.rights.forEach(right => {
                            if (
                                // we find a rule which transform this object
                                !right.not
                                && right.content !==  Group.GROUP
                                && !left.disableRights.includes(right.content)
                                && (
                                    isWrite 
                                    || (
                                        objectAndTexts.includes(right.content)
                                        && (
                                            right.content !== RLevel.LEVEL
                                            || (
                                                this._allowLevelTransformation && left.content !== RLevel.LEVEL
                                            )
                                            || (
                                                textOrObject.levelPath === null
                                                && textOrObject.becomeLevelPath === null
                                            )
                                        )
                                    )
                                )
                                && this._ruleCheckerManager.isRuleChecked(
                                    levelState,
                                    rules,
                                    ruleTransform,
                                    left,
                                    right,
                                    textOrObject
                                )
                            ) {
                                this.cancelMovementForToo(levelState, tooToRemoves, textOrObject, cancelMovements);

                                let newTextOrObject: TextOrObject;
                                if (right.content == All.ALL) { // ... is text
                                    levelState.getWordsRealObjectToMatch().forEach(name => {
                                        // we create all new object
                                        newTextOrObject = new TextOrObject(
                                            name,
                                            !isWrite,
                                            textOrObject.x,
                                            textOrObject.y
                                        );
                                        this.applyProperties(textOrObject, newTextOrObject);
                                        this.addToAdds(levelState, isMake, textOrObject, newTextOrObject, tooToAdds);
                                    });

                                }
                                else if (right.content == Text.TEXT) { // ... is text
                                    let name = Text.TEXT;// ... write text
                                    if(!isWrite){ // ... is text
                                        if(null !==  textOrObject.levelPath){// 
                                            name = RLevel.LEVEL;
                                        }
                                        else{
                                            name = textOrObject.name;
                                        }
                                    }

                                    // we create all new object
                                    newTextOrObject = new TextOrObject(
                                        name,
                                        false,
                                        textOrObject.x,
                                        textOrObject.y
                                    );
                                    this.applyProperties(textOrObject, newTextOrObject);
                                    this.addToAdds(levelState, isMake, textOrObject, newTextOrObject, tooToAdds);
                                }
                                else {
                                    // we create all new object
                                    newTextOrObject = new TextOrObject(
                                        right.content, // ... is baba
                                        !isWrite,
                                        textOrObject.x,
                                        textOrObject.y
                                    );
                                    this.applyProperties(textOrObject, newTextOrObject);
                                    this.addToAdds(levelState, isMake, textOrObject, newTextOrObject, tooToAdds);
                                }

                            }
                        });
                    }

                });
            });


            if (forceToRemove) {
                this.cancelMovementForToo(levelState, tooToRemoves, textOrObject, cancelMovements);
            }

        });

        this.simplify(tooToRemoves, tooToAdds);

        // we remove old object from levelState
        if (!isMake) {
            tooToRemoves.forEach(tooToRemove => {
                tooToRemove.hIsDestroy = true;
                levelState.removeTextOrObject(tooToRemove);
            });
        }


        // we add new object to levelState
        tooToAdds.forEach(tooToAdd => {
            levelState.addTextOrObject(
                tooToAdd
            );
        });

        return (
            tooToRemoves.length > 0
            || tooToAdds.length > 0
        );
    }

    private applyProperties = (oldTextOrObject:TextOrObject, newTextOrObject:TextOrObject): void => {
        newTextOrObject.moveDirection = oldTextOrObject.moveDirection;
        newTextOrObject.isGlobalLevel = oldTextOrObject.isGlobalLevel;
        newTextOrObject.levelPath = oldTextOrObject.levelPath;
        newTextOrObject.moveDirection = oldTextOrObject.moveDirection;
        newTextOrObject.isLevel = false;
        newTextOrObject.isLevelHighlight = null !== oldTextOrObject.levelPath;
    }

    public make = (
        levelState: LevelState,
        rules: Rule[]
    ): boolean => {
        return this.transform(levelState, levelState.textOrObjects, rules, Verb.MAKE, false, true, []);
    }
}