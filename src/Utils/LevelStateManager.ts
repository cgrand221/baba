
import { KeyCode } from "../Models/KeyCode";
import { LevelState } from "../Models/LevelState";
import { DirTurnFloatFallManager } from "./LevelStateUtils/DirTurnFloatFallManager";
import { EndWinBonusManager } from "./LevelStateUtils/EndWinBonusManager";
import { MoveManager } from "./LevelStateUtils/MoveManager";
import { MorePlayHoldDestroyColorsManager } from "./LevelStateUtils/MorePlayHoldDestroyColorsManager";
import { RuleManager } from "./RuleManager";
import { TransformFollowTeleFallManager } from "./LevelStateUtils/TransformFollowTeleFallManager";
import { EmptyManager } from "./EmptyManager";
import { Rule } from "../Models/Rule";
import { SaveManager } from "./SaveManager";
import { FallManager } from "./LevelStateUtils/FallManager";
import { TransformManager } from "./LevelStateUtils/TransformManager";

export class LevelStateManager {

    private static _instance: LevelStateManager | null = null;
    private _ruleManager: RuleManager
    private _history: LevelState[];
    private _moveManager:MoveManager;
    private _transformFollowTeleFallManager: TransformFollowTeleFallManager;
    private _transformManager: TransformManager;
    private _fallManager: FallManager;
    private _dirTurnFloatFallManager: DirTurnFloatFallManager;
    private _morePlayHoldDestroyColorsManager: MorePlayHoldDestroyColorsManager;
    private _endWinBonusManager: EndWinBonusManager;
    private _emptyManager: EmptyManager;
    private _saveManager: SaveManager;

    private constructor() {
        this._ruleManager = RuleManager.instance;
        this._moveManager = MoveManager.instance;
        this._transformFollowTeleFallManager = TransformFollowTeleFallManager.instance;
        this._transformManager = TransformManager.instance;
        this._fallManager = FallManager.instance;
        this._dirTurnFloatFallManager = DirTurnFloatFallManager.instance;
        this._morePlayHoldDestroyColorsManager = MorePlayHoldDestroyColorsManager.instance;
        this._endWinBonusManager = EndWinBonusManager.instance;
        this._emptyManager = EmptyManager.instance;
        this._saveManager = SaveManager.instance;
        this._history = [];
    }

    public static get instance(): LevelStateManager {
        if (!LevelStateManager._instance) {
            LevelStateManager._instance = new LevelStateManager();
        }
        return LevelStateManager._instance;
    }

    private _historyManage = (ls: LevelState, keyCode: KeyCode):LevelState|null  => {
       
        
        let lsh:LevelState|undefined;
        if(keyCode == KeyCode.KEY_RESTART){
            lsh = this._history[0];
            this._history = [];
            if(undefined !== lsh){
                return lsh;
            }
            return ls;
        }
        if(keyCode == KeyCode.KEY_BACK){
            lsh = this._history.pop();
            if(undefined !== lsh){
                return lsh;
            }
            return ls;
        }
       
        return null
    }

    public initFromSavemanagerOnly = (levelState: LevelState) => {
        this._history.length = 0;       
        
        this._ruleManager.init(levelState);
        let rules:Rule[] = this._saveManager.getAppliedRules(levelState);
       
        this._transformManager.init(false);
        this.compileAndDoWorks(levelState, KeyCode.KEY_SPACE, rules, false);
        rules.length = 0;
        levelState.textOrObjects.forEach(too => {
            too.rules.length = 0;
        });
    }

    private initFromLevelStateOnly = (levelState: LevelState) => {
        let rules:Rule[] = [];

        this._transformManager.init(true);
        this._ruleManager.compileAndCalcProperties(levelState, rules, true);
        this._ruleManager.applySave(levelState);
        levelState.propagateViewState();
        this._endWinBonusManager.zoneCompletedState(levelState);
        this.removeEmptys(levelState);
    }

    public init = (levelState: LevelState) => {
        
        let parentLevelState = levelState.parentLevelState;
        if(null !== parentLevelState){
            this.initFromSavemanagerOnly(parentLevelState);
        }
        this.initFromSavemanagerOnly(levelState);
        this.initFromLevelStateOnly(levelState);
    }
    

    public removeEmptys = (levelState: LevelState) => {
        this._emptyManager.removeEmptys(levelState); 
    }

    public getSolutionKey = (ls: LevelState, solutionNumber: number, position: number): KeyCode|number|null => {       
        let tmpKeyKode:KeyCode|number|null = null;
        let action = ls.level.actions[solutionNumber] ?? null;
        
        if(null !== action){
             tmpKeyKode = action.keyOrNumberSolutions[position] ?? null;
        }
        
        return tmpKeyKode;
    }

    private compileAndDoWorks = (
        levelState: LevelState,
        keyCode: KeyCode, 
        rules: Rule[], 
        progressWord: boolean
    ): boolean => {
       
        this._ruleManager.compileAndCalcProperties(levelState, rules, progressWord);
        levelState.textOrObjects.forEach(too => {
            too.disableWeak = false;
        });
        
        let change = false;
        if (this._moveManager.doWork(levelState, rules, keyCode)) {
            this._ruleManager.compileAndCalcProperties(levelState, rules, progressWord);
            change = true;
        }
        
        if (this._transformFollowTeleFallManager.doWork(levelState, rules, keyCode)) {//call destroy, tele
            this._ruleManager.compileAndCalcProperties(levelState, rules, progressWord);
            change = true;
        }
        if(this._fallManager.doWork(levelState, rules)){
            this._ruleManager.compileAndCalcProperties(levelState, rules, progressWord);
            change = true;
        }
        if (this._dirTurnFloatFallManager.doWork(levelState, rules)) {
            this._ruleManager.compileAndCalcProperties(levelState, rules, progressWord);
            change = true;
        }
        
        // this._levelBlockManager.doWork(levelState, rules)
        if(this._morePlayHoldDestroyColorsManager.doWork(levelState, rules)){// call more
            this._ruleManager.compileAndCalcProperties(levelState, rules, progressWord);
            change = true;
        }
       
        return change;
    }

    public getNextLevellState = (
        ls: LevelState, 
        keyCode: KeyCode, 
    ): readonly [LevelState|null, LevelState] => {

        this._saveManager.initWithLevel(ls)
        let lsh: LevelState|null = this._historyManage(ls, keyCode);
        if (lsh){
            return [null, lsh];
        }

        let oldState = ls.state;
        let levelState: LevelState = ls.clone();
        let rules:Rule[] = [];
        let change = this.compileAndDoWorks(levelState, keyCode, rules, true);

        this._endWinBonusManager.doWork(levelState, rules);
        if(oldState !== levelState.state){
            change = true;
        }  

        this._saveManager.manageSave(levelState, keyCode)
        if(change){   
            levelState.keyCode = keyCode;
            this.removeEmptys(levelState);       
            this._history.push(ls);
            return [ls, levelState];
        }    
        return [ls, ls];//in case ok equality, grid is not repaint by view, messages like "restart", "cancel", "wait" stays the the screen
    }
    

    public getSolution = (): string[] => {
        let action:LevelState[] = [];
        action.push(...this._history);
        action.splice(0, 1);
        return action.map(this.getKey)
    }

    public getKey = (ls: LevelState): string => {
        let rKey:string = 'S';
        switch (ls.keyCode){
            case KeyCode.KEY_DOWN:
                rKey = 'D';
                break;
            case KeyCode.KEY_RIGHT:
                rKey = 'R';
                break;
            case KeyCode.KEY_UP:
                rKey = 'U';
                break;
            case KeyCode.KEY_LEFT:
                rKey = 'L';
                break;
            case KeyCode.KEY_SPACE:
                rKey = 'S';
                break;
            case KeyCode.KEY_BACK:
                rKey = 'B';
                break;
            case KeyCode.KEY_RESTART:
                rKey = 'T';
                break;
        }
        return rKey;
    }
}