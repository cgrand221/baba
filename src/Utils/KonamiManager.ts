import { KeyCode } from "../Models/KeyCode";

export class KonamiManager{
    private static _instance: KonamiManager|null = null;
    

    private _isEnabled: boolean;
    private _nbKeyPressed: number;
    private _state: number;
    private _sequence: KeyCode[];
    

    private constructor(){
        this._isEnabled = false;
        this._state = 0;
        this._nbKeyPressed = 0;
        this._sequence = [
            KeyCode.KEY_UP,
            KeyCode.KEY_UP,
            KeyCode.KEY_DOWN,
            KeyCode.KEY_DOWN,
            KeyCode.KEY_LEFT,
            KeyCode.KEY_RIGHT,
            KeyCode.KEY_LEFT,
            KeyCode.KEY_RIGHT,
            KeyCode.KEY_B,
            KeyCode.KEY_A,
        ];
    }

    public static get instance(): KonamiManager
    {
        if(!KonamiManager._instance){
            KonamiManager._instance = new KonamiManager();
        }
        return KonamiManager._instance;
    }

    public playKey = (keyCode:KeyCode): void => {
        
        if(this._isEnabled){
            return;
        }
        if(keyCode === KeyCode.KEY_RESTART){
            this._nbKeyPressed = 0;
            this._state = 0;
            return;
        }
        if(this._nbKeyPressed !== this._state){
            this._state = 0;
            return;
        }
        let requiredKey = this._sequence[this._state];
        if(!requiredKey){
            return;
        }
        if(keyCode === requiredKey){
            this._state++;
        }
        this._nbKeyPressed++;
        if(this._state === this._sequence.length){
            this._isEnabled = true;
        }
    }

    public get isEnabled(): boolean {
        return this._isEnabled;
    }
}