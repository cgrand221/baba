import { Action } from "../Models/Action";
import { KeyCode } from "../Models/KeyCode";
import { Level } from "../Models/Level";
import { LevelState } from "../Models/LevelState";
import { Source } from "../Models/Source";
import { TextOrObject } from "../Models/TextOrObject";
var parseString = require('xml2js').parseString;

export class LevelStateBuildManager {
    private static _instance: LevelStateBuildManager | null = null;

    private constructor() {
    }

    public static get instance(): LevelStateBuildManager {
        if (!LevelStateBuildManager._instance) {
            LevelStateBuildManager._instance = new LevelStateBuildManager();
        }
        return LevelStateBuildManager._instance;
    }

    private addSource = (sources: Source[], isObject: boolean, defaultPriority: string, tagName: string, attrs: object) => {
        let src: string | null = null;
        let srcL: string | null = null;
        let srcB: string | null = null;
        let srcBl: string | null = null;
        let srcR: string | null = null;
        let srcRl: string | null = null;
        let srcRb: string | null = null;
        let srcRbl: string | null = null;
        let srcT: string | null = null;
        let srcTl: string | null = null;
        let srcTb: string | null = null;
        let srcTbl: string | null = null;
        let srcTr: string | null = null;
        let srcTrl: string | null = null;
        let srcTrb: string | null = null;
        let srcTrbl: string | null = null;

        let text: string | null = null;
        let srcRight: string | null = null;
        let srcBottom: string | null = null;
        let srcLeft: string | null = null;
        let priority: string | null = null;
        if ('src' in attrs) {
            let attr = attrs['src'];
            if (typeof attr === 'string') {
                src = attr;
            }
        }
        if ('src-l' in attrs) {
            let attr = attrs['src-l'];
            if (typeof attr === 'string') {
                srcL = attr;
            }
        }
        if ('src-b' in attrs) {
            let attr = attrs['src-b'];
            if (typeof attr === 'string') {
                srcB = attr;
            }
        }
        if ('src-bl' in attrs) {
            let attr = attrs['src-bl'];
            if (typeof attr === 'string') {
                srcBl = attr;
            }
        }
        if ('src-r' in attrs) {
            let attr = attrs['src-r'];
            if (typeof attr === 'string') {
                srcR = attr;
            }
        }
        if ('src-rl' in attrs) {
            let attr = attrs['src-rl'];
            if (typeof attr === 'string') {
                srcRl = attr;
            }
        }
        if ('src-rb' in attrs) {
            let attr = attrs['src-rb'];
            if (typeof attr === 'string') {
                srcRb = attr;
            }
        }
        if ('src-rbl' in attrs) {
            let attr = attrs['src-rbl'];
            if (typeof attr === 'string') {
                srcRbl = attr;
            }
        }
        if ('src-t' in attrs) {
            let attr = attrs['src-t'];
            if (typeof attr === 'string') {
                srcT = attr;
            }
        }
        if ('src-tl' in attrs) {
            let attr = attrs['src-tl'];
            if (typeof attr === 'string') {
                srcTl = attr;
            }
        }
        if ('src-tb' in attrs) {
            let attr = attrs['src-tb'];
            if (typeof attr === 'string') {
                srcTb = attr;
            }
        }
        if ('src-tbl' in attrs) {
            let attr = attrs['src-tbl'];
            if (typeof attr === 'string') {
                srcTbl = attr;
            }
        }
        if ('src-tr' in attrs) {
            let attr = attrs['src-tr'];
            if (typeof attr === 'string') {
                srcTr = attr;
            }
        }
        if ('src-trl' in attrs) {
            let attr = attrs['src-trl'];
            if (typeof attr === 'string') {
                srcTrl = attr;
            }
        }
        if ('src-trb' in attrs) {
            let attr = attrs['src-trb'];
            if (typeof attr === 'string') {
                srcTrb = attr;
            }
        }
        if ('src-trbl' in attrs) {
            let attr = attrs['src-trbl'];
            if (typeof attr === 'string') {
                srcTrbl = attr;
            }
        }
        if ('text' in attrs) {
            let attr = attrs['text'];
            if (typeof attr === 'string') {
                text = attr;
            }
        }
        if ('src-right' in attrs) {
            let attr = attrs['src-right'];
            if (typeof attr === 'string') {
                srcRight = attr;
            }
        }
        if ('src-bottom' in attrs) {
            let attr = attrs['src-bottom'];
            if (typeof attr === 'string') {
                srcBottom = attr;
            }
        }
        if ('src-left' in attrs) {
            let attr = attrs['src-left'];
            if (typeof attr === 'string') {
                srcLeft = attr;
            }
        }
        if ('priority' in attrs) {
            let attr = attrs['priority'];
            if (typeof attr === 'string') {
                priority = attr;
            }
        }

        if (
            null !== text
            || null !== src
        ) {
            sources.push(
                new Source(
                    tagName,
                    isObject,
                    text,

                    src,
                    srcT,
                    srcR,
                    srcTr,
                    srcB,
                    srcTb,
                    srcRb,
                    srcTrb,
                    srcL,
                    srcTl,
                    srcRl,
                    srcTrl,
                    srcBl,
                    srcTbl,
                    srcRbl,
                    srcTrbl,

                    srcRight,
                    srcBottom,
                    srcLeft,
                    parseInt(priority ? priority : defaultPriority)
                )
            );
        }
    }


    private addTextOrObject = (levelState: LevelState, isObject: boolean, tagName: string, globalViewState: string, oneBObject: object): void => {

        Object.entries(oneBObject).forEach(infoElement => {
            let x: number = -1;
            let y: number = -1;
            let viewState: string = globalViewState;
            let moveDirection: string | null = null;
            let levelPath: string | null = null;
            let becomeLevelPath: string | null = null;
            let info: unknown = infoElement[1];
            if (typeof info === 'object' && info !== null && '$' in info) {
                let attrs: unknown = info['$'];
                if (typeof attrs === 'object' && attrs !== null && 'x' in attrs && 'y' in attrs) {
                    let attrX = attrs['x'];
                    if (typeof attrX === 'string') {
                        x = parseInt(attrX);
                    }
                    let attrY = attrs['y'];
                    if (typeof attrY === 'string') {
                        y = parseInt(attrY);
                    }
                    if ('view-state' in attrs && typeof attrs['view-state'] === 'string') {
                        viewState = attrs['view-state'];
                    }
                    if ('move-direction' in attrs && typeof attrs['move-direction'] === 'string') {
                        moveDirection = attrs['move-direction'];
                    }
                    if ('level-path' in attrs && typeof attrs['level-path'] === 'string') {
                        levelPath = attrs['level-path'];
                    }
                    if ('become-level-path' in attrs && typeof attrs['become-level-path'] === 'string') {
                        becomeLevelPath = attrs['become-level-path'];
                    }


                    if (
                        x >= 0
                        && y >= 0
                    ) {
                        let too = new TextOrObject(
                            tagName,
                            isObject,
                            x,
                            y
                        );
                        if (viewState === 'locked') {
                            too.viewState = TextOrObject.VIEW_STATE_LOCKED
                        }
                        else if (viewState === 'visible') {
                            too.viewState = TextOrObject.VIEW_STATE_VISIBLE
                        }
                        else if (viewState === 'has-required-spores') {
                            too.viewState = TextOrObject.VIEW_STATE_HAS_REQUIRED_SPORES
                        }
                        else if (viewState === 'win') {
                            too.viewState = TextOrObject.VIEW_STATE_WIN
                        }
                        if (moveDirection) {
                            if (moveDirection === 'up') {
                                too.moveDirection = TextOrObject.UP;
                            }
                            else if (moveDirection === 'down') {
                                too.moveDirection = TextOrObject.DOWN;
                            }
                            else if (moveDirection === 'left') {
                                too.moveDirection = TextOrObject.LEFT;
                            }
                            else if (moveDirection === 'right') {
                                too.moveDirection = TextOrObject.RIGHT;
                            }
                        }
                        if(null !== becomeLevelPath){
                            too.becomeLevelPath = becomeLevelPath;
                        }
                        if (null !== levelPath) {
                            too.levelPath = levelPath;
                            too.isLevel = true;
                        }
                        levelState.addTextOrObject(
                            too
                        );
                    }
                }
            }

        });
    }



    public buildLevel = (file: string, responseText: string): LevelState | null => {


        let xmlDoc: unknown = {};
        parseString(responseText, (err: unknown, result: unknown) => {
            if (err) console.log(err);
            xmlDoc = result;
        });

        if (typeof xmlDoc !== 'object' || xmlDoc === null) {
            return null;
        }
        if (!('level' in xmlDoc)) {
            return null;
        }

        const xmlLevel = xmlDoc['level'];

        if (typeof xmlLevel !== 'object' || xmlLevel === null) {
            return null;
        }
        if (!('$' in xmlLevel)) {
            return null;
        }

        const attrs = xmlLevel['$'];

        if (typeof attrs !== 'object' || attrs === null) {
            return null;
        }


        let width: number = -1;
        let height: number = -1;
        let parent: string | null = null;
        let nbRequiredSpores: number | null = null;
        let nbSporesZoneCompleted: number | null = null;
        let noCheckNbSpores: string[] = [];

        let widthAttr: unknown = null;
        let heightAttr: unknown = null;
        let parentAttr: unknown = null;
        let nbRequiredSporesAttr: unknown = null;
        let nbSporesZoneCompletedAttr: unknown = null;
        let noCheckNbSporesAttr: unknown = null;

        if ('width' in attrs) {
            widthAttr = attrs['width'];
        }
        if ('height' in attrs) {
            heightAttr = attrs['height'];
        }
        if ('parent' in attrs) {
            parentAttr = attrs['parent'];
        }
        if ('nb-required-spores' in attrs) {
            nbRequiredSporesAttr = attrs['nb-required-spores'];
        }
        if ('nb-spores-zone-completed' in attrs) {
            nbSporesZoneCompletedAttr = attrs['nb-spores-zone-completed'];
        }
        if ('no-check-nb-spores' in attrs) {
            noCheckNbSporesAttr = attrs['no-check-nb-spores'];
        }

        if (typeof widthAttr === 'string') {
            width = parseInt(widthAttr);
        }
        if (typeof heightAttr === 'string') {
            height = parseInt(heightAttr);
        }
        if (typeof parentAttr === 'string') {
            parent = parentAttr;
        }
        if (typeof nbRequiredSporesAttr === 'string') {
            nbRequiredSpores = parseInt(nbRequiredSporesAttr);
        }
        if (typeof nbSporesZoneCompletedAttr === 'string') {
            nbSporesZoneCompleted = parseInt(nbSporesZoneCompletedAttr);
        }
        if (typeof noCheckNbSporesAttr === 'string') {
            noCheckNbSpores = noCheckNbSporesAttr.split(',');
        }

        if (width > 0 && height > 0) {

            let sources: Source[] = [];
            let actions: Action[] = [];

            let bSolution: unknown = null;
            if ('actions' in xmlLevel) {
                let bSolutions = xmlLevel['actions'];

                if (typeof bSolutions === 'object' && bSolutions !== null && 0 in bSolutions) {
                    let bSolutionsZero = bSolutions[0];
                    if (typeof bSolutionsZero === 'object' && bSolutionsZero !== null && 'action' in bSolutionsZero) {
                        bSolution = bSolutionsZero['action'];
                    }
                }
            }

            if (typeof bSolution === 'object' && bSolution !== null) {
                Object.entries(bSolution).forEach(oneBSolutionElement => {
                    let oneBSolution: unknown = oneBSolutionElement[1];
                    if (typeof oneBSolution === 'object' && oneBSolution !== null && '$' in oneBSolution) {
                        let attrs = oneBSolution['$'];

                        let keyOrNumberSolutions: (KeyCode | number)[] = [];
                        let doAction: number = LevelState.STATE_CAN_PLAY;
                        let newParentRules:Set<string> = new Set<string>();
                        let requirements: [string, number][] = [];

                        if (typeof attrs === 'object' && attrs !== null && 'do' in attrs) {
                            let dovalue = attrs['do'];
                            if (dovalue && typeof dovalue === 'string') {
                                if (dovalue === 'win') {
                                    doAction = LevelState.STATE_IS_WIN;
                                }
                                if (dovalue === 'bonus') {
                                    doAction = LevelState.STATE_IS_BONUS;
                                }
                                else if (dovalue === 'zone_completed') {
                                    doAction = LevelState.STATE_IS_ZONE_COMPLETED;
                                }
                                else if (dovalue === 'new_parent_rules') {
                                    if('new-parent-rules' in attrs){
                                        let newParentRulesValue = attrs['new-parent-rules'];
                                        if (newParentRulesValue && typeof newParentRulesValue === 'string') {
                                            doAction = LevelState.STATE_IS_NEW_PARENT_RULES;
                                            newParentRulesValue.split(',').forEach(npr => newParentRules.add(npr));
                                        }
                                    }
                                }
                            }
                        }

                        if('requirement' in oneBSolution){
                            let requirementValues = oneBSolution['requirement'];
                            if (typeof requirementValues === 'object' && requirementValues !== null) {
                                Object.entries(requirementValues).forEach(requirementValue => {
                                    let oneBObject: unknown = requirementValue[1];
                                    if (typeof oneBObject === 'object' && oneBObject !== null && '$' in oneBObject) {
                                        let attrs = oneBObject['$'];
                                        if (typeof attrs === 'object' && attrs !== null) {
                                            let actionNumber: number|null = null;
                                            let otherLevel: string|null = null;
                                            if('actionNumber' in attrs && typeof (attrs['actionNumber']) === 'string'){
                                                actionNumber = parseInt(attrs['actionNumber']);
                                            }
                                            if('level' in attrs && typeof (attrs['level']) === 'string'){
                                                otherLevel = attrs['level'];
                                            }
                                            if(
                                                null !== actionNumber
                                                && null !== otherLevel
                                            ){
                                                requirements.push([otherLevel, actionNumber]);
                                            }
                                        }
                                    }
                                })
                            }
                        }

                        if (typeof attrs === 'object' && attrs !== null && 'keys' in attrs) {
                            let allKeys = attrs['keys'];

                            if (allKeys && typeof allKeys === 'string') {
                                let keys: string[] = allKeys.split(',');

                                keyOrNumberSolutions = keys.map(key => {// L,R,U,D,S,B,T                                
                                    let rKey: KeyCode | number = KeyCode.KEY_SPACE;
                                    switch (key) {
                                        case 'D':
                                            rKey = KeyCode.KEY_DOWN;
                                            break;
                                        case 'R':
                                            rKey = KeyCode.KEY_RIGHT;
                                            break;
                                        case 'U':
                                            rKey = KeyCode.KEY_UP;
                                            break;
                                        case 'L':
                                            rKey = KeyCode.KEY_LEFT;
                                            break;
                                        case 'S':
                                            rKey = KeyCode.KEY_SPACE;
                                            break;
                                        case 'B':
                                            rKey = KeyCode.KEY_BACK;
                                            break;
                                        case 'T':
                                            rKey = KeyCode.KEY_RESTART;
                                            break;
                                        default:
                                            rKey = parseInt(key);

                                    }
                                    return rKey;
                                });

                                actions.push(
                                    new Action(keyOrNumberSolutions, doAction, newParentRules, requirements)
                                );
                            }
                        }
                    }

                });
            }

            let bSource: unknown = null;
            if ('sources' in xmlLevel) {
                let bSources = xmlLevel['sources'];
                if (typeof bSources === 'object' && bSources !== null && 0 in bSources) {
                    bSource = bSources[0];
                }
            }


            if (typeof bSource === 'object' && bSource !== null) {
                if ('objects' in bSource) {
                    let bObjects = bSource['objects'];
                    if (typeof bObjects === 'object' && bObjects !== null && 0 in bObjects) {
                        let oneBObjects = bObjects[0];
                        if (typeof oneBObjects === 'object' && oneBObjects !== null) {
                            Object.entries(oneBObjects).forEach(bObjectElemnt => {
                                let bTagName = bObjectElemnt[0];
                                let bObject: unknown = bObjectElemnt[1];
                                if (typeof bObject === 'object' && bObject !== null && 0 in bObject) {
                                    let oneBobject = bObject[0];

                                    if (typeof oneBobject === 'object' && oneBobject !== null && '$' in oneBobject) {

                                        let attrs = (oneBobject['$']);
                                        if (typeof attrs === 'object' && attrs !== null) {
                                            this.addSource(sources, true, '1', bTagName, attrs)
                                        }

                                    }
                                }
                            })
                        }
                    }
                }

                if ('texts' in bSource) {
                    let bObjects = bSource['texts'];
                    if (typeof bObjects === 'object' && bObjects !== null && 0 in bObjects) {
                        let oneBObjects = bObjects[0];
                        if (typeof oneBObjects === 'object' && oneBObjects !== null) {
                            Object.entries(oneBObjects).forEach(bObjectElemnt => {
                                let bTagName = bObjectElemnt[0];
                                let bObject: unknown = bObjectElemnt[1];
                                if (typeof bObject === 'object' && bObject !== null && 0 in bObject) {
                                    let oneBobject = bObject[0];

                                    if (typeof oneBobject === 'object' && oneBobject !== null && '$' in oneBobject) {

                                        let attrs = (oneBobject['$']);
                                        if (typeof attrs === 'object' && attrs !== null) {
                                            this.addSource(sources, false, '100', bTagName, attrs)
                                        }

                                    }
                                }
                            })
                        }
                    }
                }
            }

            let level = new Level(
                sources
                    .filter(source => source.isObject)
                    .map(source => source.name),
                sources
                    .filter(source => !source.isObject)
                    .map(source => source.name),
                actions,
                sources,
                file,
                width,
                height
            );
            level.nbRequiredSpores = nbRequiredSpores;
            level.nbSporesZoneCompleted = nbSporesZoneCompleted;
            level.noCheckNbSpores = noCheckNbSpores;

            if (null !== parent) {
                level.parent = parent;
            }
            let levelState = new LevelState(level);
            if ('textOrObjects' in xmlLevel) {
                let btextOrObjects = xmlLevel['textOrObjects'];
                if (typeof btextOrObjects === 'object' && btextOrObjects !== null && '0' in btextOrObjects) {
                    let oneBtextOrObjects = btextOrObjects[0];
                    if (typeof oneBtextOrObjects === 'object' && oneBtextOrObjects !== null) {
                        if ('objects' in oneBtextOrObjects) {
                            let bObjects = oneBtextOrObjects['objects'];



                            if (typeof bObjects === 'object' && bObjects !== null && '0' in bObjects) {
                                let oneBObjects = bObjects[0];
                                if (typeof oneBObjects === 'object' && oneBObjects !== null) {
                                    let viewState: string = 'visible';
                                    if ('$' in oneBObjects) {
                                        let attrs = oneBObjects['$'];
                                        if (typeof attrs === 'object' && attrs !== null && 'view-state' in attrs && typeof (attrs['view-state']) === 'string') {
                                            viewState = attrs['view-state'];
                                        }
                                    }

                                    Object.entries(oneBObjects).forEach(oneBObjectElement => {
                                        let tagName: string = oneBObjectElement[0];
                                        let oneBObject: unknown = oneBObjectElement[1];
                                        if (typeof oneBObject === 'object' && oneBObject !== null) {
                                            this.addTextOrObject(levelState, true, tagName, viewState, oneBObject);
                                        }
                                    })
                                }
                            }
                        }

                        if ('texts' in oneBtextOrObjects) {
                            let bTexts = oneBtextOrObjects['texts'];
                            if (typeof bTexts === 'object' && bTexts !== null && '0' in bTexts) {
                                let oneBTexts = bTexts[0];
                                if (typeof oneBTexts === 'object' && oneBTexts !== null) {
                                    Object.entries(oneBTexts).forEach(oneBTextElement => {
                                        let tagName: string = oneBTextElement[0];
                                        let oneBText: unknown = oneBTextElement[1];
                                        if (typeof oneBText === 'object' && oneBText !== null) {
                                            this.addTextOrObject(levelState, false, tagName, 'visible', oneBText);
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
            }
            
            return levelState;
        }
        return null;
    }
}