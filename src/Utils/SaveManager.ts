import { KeyCode } from "../Models/KeyCode";
import { LeftRightBaseRule } from "../Models/LeftRightBaseRule";
import { LevelSave } from "../Models/LevelSave";
import { LevelState } from "../Models/LevelState";
import { Rule } from "../Models/Rule";
import { RLevel } from "../Models/Rules/RLevel";
import { TextOrObject } from "../Models/TextOrObject";
import { ConfigLoader } from "./ConfigLoader";

type jsonType = {
    [key: string]: {
        isWin: 'true' | 'false',
        isBonus: 'true' | 'false',
        nbSpores: number | null,
        nbRequiredSpores: number | null,
        nbSporesZoneCompleted: number | null,
        savedRules: string[],
        parentSavedRules: string[],
        select: string | null,
    }
}

export class SaveManager {
    private static _instance: SaveManager | null = null;
    private _configLoader: ConfigLoader;
    private _levelSaves: {
        [key: string]: LevelSave
    };

    private constructor() {
        this._levelSaves = {};
        this._configLoader = ConfigLoader.instance;
    }

    public static get instance(): SaveManager {
        if (!SaveManager._instance) {
            SaveManager._instance = new SaveManager();
        }
        return SaveManager._instance;
    }

    public initWithLevel = (levelState: LevelState,) => {
        let levelFilePath = levelState.level.file;
        
        let nbRequiredSpores=levelState.level.nbRequiredSpores;
        let levelSave = this._levelSaves[levelFilePath] ?? null;
        if (null === levelSave) {
            levelSave = new LevelSave(false, false, 0, null, levelState.level.nbSporesZoneCompleted, [], [], null);
            this._levelSaves[levelFilePath] = levelSave;
        }
        levelSave._nbRequiredSpores = nbRequiredSpores;
    }

    public manageSave = (
        levelState: LevelState,
        key: KeyCode|null,
    ): void => {

        if (levelState.state === LevelState.STATE_IS_WIN) {
            this.setLevelWin(levelState);
            return;
        }
        if (levelState.state === LevelState.STATE_IS_BONUS) {
            this.setLevelBonus(levelState);
            return;
        }
        if (levelState.state === LevelState.STATE_IS_NEW_PARENT_RULES) {
            this.setParentSavedRule(levelState, levelState.parentSavedRules);
            return;
        }
        if(levelState.savedRules.size > 0){
            this.setSavedRule(levelState, levelState.savedRules);
            return;
        }
    }

    private addRules = (savedRulesValue: object): Rule[] => {
        let entriesRule = Object.entries(savedRulesValue);
        let rules: Rule[] = [];
        entriesRule.forEach(entryRule => {

            let key = entryRule[0];
            let ruleValue = entryRule[1];
            if (typeof ruleValue === 'string') {
                let ruleValues = ruleValue.split(' ');
                let left = ruleValues[0] ?? null;
                let verb = ruleValues[1] ?? null;
                let right = ruleValues[2] ?? null;
                if(null !== left && null !== verb && null !== right) {
                    rules.push(new Rule(
                        0,
                        [
                            new LeftRightBaseRule(0, left, 0, 1),
                        ],
                        [],
                        verb,
                        [
                            new LeftRightBaseRule(0, right, 0, 1),
                        ],
                    ));
                }
                

            }
        });
        return rules;
    }

    public fromString = (jsonString: string) => {
        let json = null;
        try {
            json = JSON.parse(jsonString);
        }
        catch (e) {
        }
        if (null === json) {
            this._levelSaves = {};
            return;
        }
        let entries = Object.entries(json);

        let levelSaves: {
            [key: string]: LevelSave
        } = {};
        entries.forEach(entry => {
            let file = entry[0];
            let levelSave = entry[1];
            if (typeof levelSave !== 'object' || levelSave === null) {
                return false;
            }

            let isWin = false;
            let isBonus = false;
            let nbSpores = 0;
            let nbRequiredSpores:number|null = null;
            let nbSporesZoneCompleted:number|null = null;
            let savedRules: Rule[] = [];
            let parentSavedRules: Rule[] = [];

            if ('isWin' in levelSave) {
                let isWinValue = levelSave.isWin;
                if (typeof isWinValue === 'string' && isWinValue !== null) {
                    isWin = (isWinValue === 'true');
                }
            }

            if ('isBonus' in levelSave) {
                let isBonusValue = levelSave.isBonus;
                if (typeof isBonusValue === 'string' && isBonusValue !== null) {
                    isBonus = (isBonusValue === 'true');
                }
            }

            if ('nbSporesZoneCompleted' in levelSave) {
                let nbSporesZoneCompletedValue = levelSave.nbSporesZoneCompleted;
                if (typeof nbSporesZoneCompletedValue === 'number') {
                    nbSporesZoneCompleted = nbSporesZoneCompletedValue;
                }
            }

            if ('nbSpores' in levelSave) {
                let nbSporesValue = levelSave.nbSpores;
                if (typeof nbSporesValue === 'number' && nbSporesValue !== null) {
                    nbSpores = nbSporesValue;
                }
            }

            if ('nbRequiredSpores' in levelSave) {
                let nbRequiredSporesValue = levelSave.nbRequiredSpores;
                if (typeof nbRequiredSporesValue === 'number') {
                    nbRequiredSpores = nbRequiredSporesValue;
                }
            }

            if ('savedRules' in levelSave) {
                let savedRulesValue = levelSave.savedRules;
                
                if (typeof savedRulesValue === 'object' && savedRulesValue !== null) {
                    savedRules = this.addRules(savedRulesValue);
                }
            }

            if ('parentSavedRules' in levelSave) {
                let parentSavedRulesValue = levelSave.parentSavedRules;
                if (typeof parentSavedRulesValue === 'object' && parentSavedRulesValue !== null) {
                    parentSavedRules = this.addRules(parentSavedRulesValue);
                }
            }

            let select: string | null = null;
            if ('select' in levelSave) {
                let selectValue = levelSave.select;
                if (typeof selectValue === 'string' ) {
                    select = selectValue;

                }
            }
            
            levelSaves[file] = new LevelSave(isWin, isBonus, nbSpores, nbRequiredSpores, nbSporesZoneCompleted, savedRules, parentSavedRules, select);
        });
        this._levelSaves = levelSaves;
    }

    public countIsWin = ():number => {
        let nbWin = 0;
        let entries = Object.entries(this._levelSaves);
        entries.forEach(entrie => {
            let levelSave = entrie[1];
            if(levelSave._isWin){
                nbWin++;
            }
        });
        return nbWin
    }

    public toString = (): string => {
        let entries = Object.entries(this._levelSaves);
        let json: jsonType = {};
        entries.forEach(entry => {
            let file = entry[0];
            let levelSave = entry[1];
            json[file] = {
                isWin: levelSave._isWin ? 'true' : 'false',
                isBonus: levelSave._isBonus ? 'true' : 'false',
                nbSpores: levelSave._nbSpores,
                nbRequiredSpores: levelSave._nbRequiredSpores,
                nbSporesZoneCompleted: levelSave._nbSporesZoneCompleted,
                savedRules: levelSave._savedRules.map(rule => rule.toString()),
                parentSavedRules: levelSave._parentSavedRules.map(rule => rule.toString()),
                select: levelSave._select,
            }
        });
        return JSON.stringify(json);
    }

    public getLevelSave =  (levelState: LevelState): LevelSave|null => {
        let levelFilePath = levelState.level.file;
        return this._levelSaves[levelFilePath] ?? null;
    }

    public setLevelWin = (levelState: LevelState) => {
        
        let levelFilePath = levelState.level.file;
        
        let isAlreadyWin = this.isLevelWinPath(levelFilePath);
        let parent: string | null = levelState.level.parent;
        if(null !== parent){
            parent = parent;
        }

        if(!isAlreadyWin){
            let levelSave = this._levelSaves[levelFilePath] ?? null;
            if (null === levelSave) {
                levelSave = new LevelSave(false, false, 0, null, levelState.level.nbSporesZoneCompleted, [], [], null);
                this._levelSaves[levelFilePath] = levelSave;
            }
            levelSave._isWin = true;

            if (null !== parent) {
                this.addSpore(parent);
            }
        }
        levelState.level.nextLevelPath = parent;
    }

    public setLevelBonus = (levelState: LevelState) => {
        
        let levelFilePath = levelState.level.file;
        
        let isAlreadyBonus = this.isLevelBonusPath(levelFilePath);
        

        if(!isAlreadyBonus){
            let levelSave = this._levelSaves[levelFilePath] ?? null;
            if (null === levelSave) {
                levelSave = new LevelSave(false, false, 0, null, levelState.level.nbSporesZoneCompleted, [], [], null);
                this._levelSaves[levelFilePath] = levelSave;
            }
            levelSave._isBonus = true;
        }
    }

    
    public setSavedRule = (levelState: LevelState, fromRules: Set<string>) => {
        let levelFilePath = levelState.level.file;
        let levelSave = this._levelSaves[levelFilePath] ?? null;
        if (null === levelSave) {
            levelSave = new LevelSave(false, false, 0, null, levelState.level.nbSporesZoneCompleted, [], [], null);
            this._levelSaves[levelFilePath] = levelSave;
        }
        let strRules = levelSave._savedRules.map(rule => rule.toString());
        fromRules.forEach(fromRule => {
            if(strRules.includes(fromRule.toString())){
                return;
            }
            const ruleElements = fromRule.split(' ');
            const name = ruleElements[0] ?? null;
            const verb = ruleElements[1] ?? null;
            if(null !== verb && null !== name){
                let rule = new Rule(
                    0,
                    [
                        new LeftRightBaseRule(0, name, 0, 1),
                    ],
                    [],
                    verb,
                    [
                        new LeftRightBaseRule(0, RLevel.LEVEL, 0, 1),
                    ],
                )
                levelSave._savedRules.push(rule);
            }
        });
    }

    public setParentSavedRule = (levelState: LevelState, fromRules: Set<string>) => {
        let levelFilePath = levelState.level.file;
        
        // https://babaiswiki.fandom.com/wiki/The_Box
        // conditionnal on level => map
        
        let parent: string | null = levelState.level.parent;
        
        
        let rules:Rule[] = [];
        fromRules.forEach(fromRule => {
            const ruleElements = fromRule.split(' ');
            const verb = ruleElements[1] ?? null;
            const name = ruleElements[2] ?? null;
            if(null !== verb && null !== name){
                rules.push(new Rule(
                    0,
                    [
                        new LeftRightBaseRule(0, RLevel.LEVEL, 0, 1),
                    ],
                    [],
                    verb,
                    [
                        new LeftRightBaseRule(0, name, 0, 1),
                    ],
                ));
            }
        });
        let levelSave = this._levelSaves[levelFilePath] ?? null;
        if (null === levelSave) {
            levelSave = new LevelSave(false, false, 0, null, levelState.level.nbSporesZoneCompleted, [], [], null);
            this._levelSaves[levelFilePath] = levelSave;
        }
        levelSave._parentSavedRules = rules;      
        levelState.level.nextLevelPath = parent;
    }

    public setZoneCompleted =  (levelState: LevelState) => {
        let levelFilePath = levelState.level.file;
        let levelSave = this._levelSaves[levelFilePath] ?? null;
        if (null === levelSave) {
            levelSave = new LevelSave(false, false, 0, null, levelState.level.nbSporesZoneCompleted, [], [], null);
            this._levelSaves[levelFilePath] = levelSave;
        }
        let hasLevelZoneCompleted = this.hasLevelZoneCompleted(levelState);
        let nbSporesZoneCompleted = levelState.level.nbSporesZoneCompleted;
        let parent = levelState.level.parent;
        if(null !== parent){
            parent = parent;
        }
        if(!hasLevelZoneCompleted && null !== nbSporesZoneCompleted){
            levelSave._nbSpores = nbSporesZoneCompleted;
        }
        levelState.level.nextLevelPath = parent;
    }

    private addSpore = (file: string) => {
        let levelSave = this._levelSaves[file] ?? null;
        if (null === levelSave) {
           return;
        }
        if(
            levelSave._nbSporesZoneCompleted === null
            || levelSave._nbSpores < levelSave._nbSporesZoneCompleted
        ){
            levelSave._nbSpores++;
        }
    }




    

    public isLevelWinPath = (file: string): boolean => {
        let levelSave = this._levelSaves[file] ?? null;
        if (null === levelSave) {
            return false;
        }
        return levelSave._isWin;
    }

    public isLevelBonusPath = (file: string): boolean => {
        let levelSave = this._levelSaves[file] ?? null;
        if (null === levelSave) {
            return false;
        }
        return levelSave._isBonus;
    }

    public hasLevelRequiredSporesPath = (file: string): boolean => {
        let levelSave = this._levelSaves[file] ?? null;
        if (null === levelSave) {
            return false;
        }
        if(null === levelSave._nbRequiredSpores){
            return false;
        }
        
        return (
            levelSave._nbSpores >= levelSave._nbRequiredSpores
        );
    }

    public hasLevelZoneCompleted = (levelState: LevelState): boolean => {
        let file = levelState.level.file;
        return (
            this.hasAllNbSpores(file)
            && levelState.textOrObjects.every(too => (too.viewState !== TextOrObject.VIEW_STATE_LOCKED))
            && levelState.textOrObjects.every(too => (
                null === too.levelPath 
                || this.isLevelWinPath(too.levelPath) 
                || levelState.level.noCheckNbSpores.includes(too.levelPath) 
                || this.hasAllNbSpores(too.levelPath)
            ))
        );
    }

    public hasAllNbSpores = (file: string): boolean => {
        let levelSave = this._levelSaves[file] ?? null;
       
        if (null === levelSave) {
            return false;
        }
        if(null === levelSave._nbSporesZoneCompleted){
            return false;
        }
       
        return (
            levelSave._nbSpores >= levelSave._nbSporesZoneCompleted 
        );
    }

    public getSelectLevelPath = (file: string): string|null => {
        let levelSave = this._levelSaves[file] ?? null;
        if (null === levelSave) {
            return null;
        }
        return levelSave._select
    }

    public setSelectLevelPath = (levelState: LevelState, levelPath:string) => {
        let file = levelState.level.file;
        let levelSave = this._levelSaves[file] ?? null;
        if (null === levelSave) {
            levelSave = new LevelSave(false, false, 0, null, levelState.level.nbSporesZoneCompleted, [], [], null);
            this._levelSaves[file] = levelSave;
        }
        levelSave._select = levelPath;
    }

    public getRules = (file: string) :Rule[] => {
        let levelSave = this._levelSaves[file] ?? null;
        if (null === levelSave) {
            return [];
        }
        return levelSave._parentSavedRules;
    }

    public getAppliedRules = (levelState: LevelState,): Rule[] => {
        let rules:Rule[] = [];
        levelState.textOrObjects.forEach(too => {
            let file = too.levelPath;
            if(null === file){
                return;
            }
            
            
            let levelSave = this._levelSaves[file] ?? null;
            if (null === levelSave) {
                return ;
            }

            levelSave._parentSavedRules.forEach(rule => {// level is baba, level write level, ...
                let leftRule = rule.lefts[0] ?? null;
                let rightRule = rule.rights[0] ?? null;
                if(null === leftRule || null === rightRule){
                    return;
                }
                let leftContent = leftRule.content;
                let rightContent = rightRule.content;
                if(leftContent === RLevel.LEVEL) {
                    leftContent = too.name;
                }
                rules.push(new Rule(
                    0,
                    [
                        new LeftRightBaseRule(0, leftContent, 0, 1),
                    ],
                    [],
                    rule.verb,
                    [
                        new LeftRightBaseRule(0, rightContent, 0, 1),
                    ],
                ));
            });
        });
        
        let levelSave = this.getLevelSave(levelState);
        if(null !== levelSave){
            levelSave._savedRules.forEach(rule => {
                let leftRule = rule.lefts[0] ?? null;
                let rightRule = rule.rights[0] ?? null;
                if(null === leftRule || null === rightRule){
                    return;
                }
                let leftContent = leftRule.content;
                let rightContent = rightRule.content;
                rules.push(new Rule(
                    0,
                    [
                        new LeftRightBaseRule(0, leftContent, 0, 1),
                    ],
                    [],
                    rule.verb,
                    [
                        new LeftRightBaseRule(0, rightContent, 0, 1),
                    ],
                ));
            });
        }

        
        
        return rules;
    }

    public getNbBonus = () : number => {
        return Object.entries(this._levelSaves).reduce((previous, fileLevelSave) => previous+(fileLevelSave[1]._isBonus ? 1 : 0), 0);
    }
}