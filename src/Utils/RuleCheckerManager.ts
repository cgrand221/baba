import { ConditionRule } from "../Models/ConditionRule";
import { LeftRightBaseRule } from "../Models/LeftRightBaseRule";
import { LevelState } from "../Models/LevelState";
import { Rule } from "../Models/Rule";
import { All } from "../Models/Rules/All";
import { Condition } from "../Models/Rules/Condition";
import { Empty } from "../Models/Rules/Empty";
import { Group } from "../Models/Rules/Group";
import { Quality } from "../Models/Rules/Quality";
import { RLevel } from "../Models/Rules/RLevel";
import { Text } from "../Models/Rules/Text";
import { Verb } from "../Models/Rules/Verb";
import { TextOrObject } from "../Models/TextOrObject";

export class RuleCheckerManager {

    private static _instance: RuleCheckerManager | null = null;
    private _group: string[];


    private constructor() {
        this._group = [];
    }

    public static get instance(): RuleCheckerManager {
        if (!RuleCheckerManager._instance) {
            RuleCheckerManager._instance = new RuleCheckerManager();
        }
        return RuleCheckerManager._instance;
    }

    public isbasicLeftChecked = (
        leftContent: string,
        leftNot: boolean,
        isObject: boolean,
        nbGroup: number,
        content: string,
        isLevel: boolean,
    ): boolean => {
        if (isObject) {
            if (leftNot) {
                return (
                    (leftContent !== content && leftContent !== All.ALL && content !== Empty.EMPTY && content !== Group.GROUP && content !== RLevel.LEVEL) ||
                    (leftContent === All.ALL && content === Empty.EMPTY) ||
                    (leftContent === Group.GROUP && nbGroup === 0)
                );
            }
            return (
                leftContent === content ||
                (leftContent === All.ALL && content !== Empty.EMPTY && content !== Group.GROUP && content !== RLevel.LEVEL) ||
                (leftContent === Group.GROUP && nbGroup > 0) ||
                (leftContent === RLevel.LEVEL && isLevel)
            );

        }
        if (leftNot) {
            return leftContent === All.ALL;
        }
        return leftContent === Text.TEXT || (leftContent === Group.GROUP && nbGroup > 0);

    };

    private exclude = (toos: TextOrObject[], too: TextOrObject): boolean => {
        let tooPos = toos.indexOf(too);
        if (tooPos !== -1) {
            toos.splice(tooPos, 1);
            return true;
        }
        return false;
    }

    private generateAllContentToCheck = (levelState: LevelState, lrbrContent: string, lrbrNot: boolean): {
        contents: string[],
        not: boolean,
    } => {
        if (lrbrNot) {
            if (lrbrContent === All.ALL) {
                return {
                    contents: [Text.TEXT, Empty.EMPTY],
                    not: true,
                };
            }
            if (lrbrContent === Group.GROUP) {
                return {
                    contents: levelState.getWordsRealObjectToMatch().filter(word => !this.group.includes(word)),
                    not: true,
                };
            }
            return {
                contents: [lrbrContent],
                not: true,
            };
        }
        if (lrbrContent === All.ALL) {
            return {
                contents: levelState.getWordsRealObjectToMatch(),
                not: false,
            };
        }
        if (lrbrContent === Group.GROUP) {
            return {
                contents: this.group,
                not: true,
            };
        }
        return {
            contents: [lrbrContent],
            not: false,
        };
    }

    private generateContent = (levelState: LevelState, lrbrContent: string, lrbrNot: boolean): string[] => {
        if (lrbrNot) {
            if (lrbrContent === All.ALL) {
                return [Text.TEXT, Empty.EMPTY]
            }
            if (lrbrContent === Group.GROUP) {
                return levelState.getWordsRealObjectToMatch().filter(word => !this.group.includes(word));
            }
            return levelState.getWordsRealObjectToMatch().filter(word => word !== lrbrContent);

        }

        if (lrbrContent === All.ALL) {
            return levelState.getWordsRealObjectToMatch();
        }
        if (lrbrContent === Group.GROUP) {
            return levelState.getWordsRealObjectToMatch().filter(word => this.group.includes(word));
        }
        return [lrbrContent];

    }

    private isConditionTooChecked = (
        levelState: LevelState,
        toos: TextOrObject[],
        leftRightBaseRules: LeftRightBaseRule[]
    ): boolean => {
        if (toos.length === 0) {
            return false;
        }

        // all leftRightBaseRules musy be checked to return true
        return leftRightBaseRules.every(lrbr => {

            let { contents, not } = this.generateAllContentToCheck(levelState, lrbr.content, lrbr.not);


            return contents.every(content => {
                //return false if fails :
                let tooPos = toos.findIndex(too => this.isbasicLeftChecked(content, not, too.isObject, too.nbGroup, too.name, too.isLevel));

                let too = tooPos === -1 ? null : toos[tooPos];
                if (too && tooPos !== -1) {
                    toos.splice(tooPos, 1);
                    return true;
                }
                else {
                    return false;
                }

                // baba facing flag => 'flag' === textOrObject.name
            })
        });
    }

    private isLonelyChecked = (levelState: LevelState, rule: Rule, textOrObject: TextOrObject): boolean => {
        return (
            !rule.lonely
            || rule.notLonely === levelState.getTextAndObjectsAt(
                textOrObject.x,
                textOrObject.y
            ).some(too => !too.isGlobalLevel && too !== textOrObject)
        );
    }

    private isConditionRuleCheckedWithParentLevel = (
        levelState: LevelState,
        condition: ConditionRule,
        textOrObject: TextOrObject
    ): boolean => {
        
        let toos:TextOrObject[] = [
            textOrObject
        ];
        let ls = levelState;
        if (textOrObject.isGlobalLevel && condition.condition !== Condition.ON) {
            let parentLevelState = levelState.parentLevelState;
            if (null !== parentLevelState) {
                ls = parentLevelState;
                toos = parentLevelState.textOrObjects.filter(too => too.levelPath === levelState.level.file);
            }
        }
        return toos.some(too => this.isConditionRuleChecked(ls, condition, too));
    }

    private isConditionRuleChecked = (
        levelState: LevelState,
        condition: ConditionRule,
        textOrObject: TextOrObject
    ): boolean => {


        let x = textOrObject.x;
        let y = textOrObject.y;


        let dxys: ([number, number])[] = [];

        if (condition.condition === Condition.ON) {
            dxys.push([0, 0]);

        }
        else if (condition.condition === Condition.FACING) {
            if (textOrObject.moveDirection === TextOrObject.UP) {
                dxys.push([0, -1]);
            }
            else if (textOrObject.moveDirection === TextOrObject.RIGHT) {
                dxys.push([1, 0]);
            }
            else if (textOrObject.moveDirection === TextOrObject.DOWN) {
                dxys.push([0, 1]);
            }
            else {
                dxys.push([-1, 0]);
            }
        }
        else if (condition.condition === Condition.NEAR) {
            dxys.push([-1, -1]);
            dxys.push([0, -1]);
            dxys.push([1, -1]);
            dxys.push([-1, 0]);
            dxys.push([0, 0]);
            dxys.push([1, 0]);
            dxys.push([-1, 1]);
            dxys.push([0, 1]);
            dxys.push([1, 1]);
        }

        return (dxys.some(dxy => {
            let dx = dxy[0];
            let dy = dxy[1];
            let toos: TextOrObject[] = [];

            // todo : use parent context if needed

            if (textOrObject.isGlobalLevel) {//works only for ON

                if (condition.condition === Condition.ON) {
                    toos.push(...levelState.textOrObjects.filter(too => !too.isGlobalLevel));
                }
                else {
                    toos.push(...levelState.getTextAndObjectsAt(x + dx, y + dy));
                }
            }
            else {
                toos.push(...levelState.getTextAndObjectsAt(x + dx, y + dy));
            }
            if (dx === 0 && dy === 0) {
                this.exclude(toos, textOrObject);
            }
            return (condition.not !== this.isConditionTooChecked(
                levelState,
                toos,
                condition.leftRightBaseRules
            ));
        }));

    }

    public isRuleChecked = (// levelState, rules, left, rule.conditions, rule.verb, right, too
        levelState: LevelState,
        rules: Rule[],
        currentRule: Rule,
        left: LeftRightBaseRule,
        right: LeftRightBaseRule,
        textOrObject: TextOrObject
    ): boolean => {


        // if current rule to apply is pointed by a rule that disable it, then we look at conditions
        // if conditions are true, then rule is disabled and can't be true for textOrObject
        // we do the same thing as rule.lefts[...].disableRights but only for textOrObject

        // ex : baba on flag is not you
        //      baba is you
        // we considere that textOrObject is baba and is at the same position as a flag object

        // forbiddenRules = [baba on flag is not you];
        // next we search in forbiddenRules a rule which correspond to the rule "baba is you" and where conditions are true

        // we considere now that textOrObject is baba and is alone on the cell
        // forbiddenRules = [baba on flag is not you];
        // next we search in forbiddenRules a rule which correspond to the rule "baba is you" and where conditions are true
        // but there is no forbiddenRules for that
        // then "baba is you" is checked

        if (
            this.calcIsForbiddenPart(levelState, rules, currentRule, left, right, textOrObject)
        ) {
            return false;
        }

        // same thing :
        // if rule is "a is b" and rules contains "a is a"  => [a is b] rule is disabled, [a is not b] rule is not disabled
        if (
            this.calcIsTransformPartForbidden(levelState, rules, currentRule, left, right, textOrObject)
        ) {
            return false;
        }

        return (
            this.isLonelyChecked(levelState, currentRule, textOrObject)
            && currentRule.conditions.every(condition => this.isConditionRuleCheckedWithParentLevel(levelState, condition, textOrObject))
        );
    }



    private specificTerm = (left: LeftRightBaseRule, textOrObject: TextOrObject | null): string | null => {
        if (
            left.not
            || left.content === All.ALL
            || left.content === Group.GROUP
        ) {
            if (null === textOrObject) {
                return null;
            }
            return (
                this.isbasicLeftChecked(left.content, left.not, textOrObject.isObject, textOrObject.nbGroup, textOrObject.name, textOrObject.isLevel)
                    ? textOrObject.name
                    : null
            );
        }

        return left.content;
    }

    /**
     * this function is not symetric :
     *      isLeftLeftRuleChecked('baba', 'all', null) -> rule [baba is ...] can be forbidden by [all is ...] ? -> yes
     *      isLeftLeftRuleChecked('all', 'baba', null) -> rule [all is ...] can be forbidden by [baba is ...] ? -> no in the general case, for exemple :
     *          isLeftLeftRuleChecked('all', 'baba', 'flag') -> flag is concerned by [all is ...] and not concerned by [baba is ...]
     *          isLeftLeftRuleChecked('all', 'baba', 'baba') -> baba is concerned by [all is ...] and concerned by [baba is ...] too
     * 
     * same thing [not baba is] and [flag is]...
     */
    private isLeftLeftRuleChecked = (left: LeftRightBaseRule, ruleLeft: LeftRightBaseRule, textOrObject: TextOrObject | null): boolean => {
        if (
            left.not
            || left.content === All.ALL
            || left.content === Group.GROUP
        ) {
            // left.not is not used here because when we analyse, we see the same result with left.not = true or false
            if (null === textOrObject) {
                return false;
            }
            return this.isbasicLeftChecked(ruleLeft.content, ruleLeft.not, textOrObject.isObject, textOrObject.nbGroup, textOrObject.name, textOrObject.isLevel);
        }
        let isText = left.content === Text.TEXT;
        let nbGroup = this._group.filter(g => g === left.content).length;
        if (textOrObject) {
            nbGroup = textOrObject.nbGroup;
        }
        return this.isbasicLeftChecked(ruleLeft.content, ruleLeft.not, !isText, nbGroup, left.content, false);
    }

    private calcIsForbiddenPart = (
        levelState: LevelState,
        rules: Rule[],
        currentRule: Rule,
        left: LeftRightBaseRule,
        right: LeftRightBaseRule,
        textOrObject: TextOrObject | null
    ): boolean => {
        return (
            !right.not
            && rules.some(rule => {

                return (
                    // baba is not flag
                    rule !== currentRule
                    && rule.lefts.some(ruleLeft => {
                        return this.isLeftLeftRuleChecked(left, ruleLeft, textOrObject);
                    })
                    && rule.verb === currentRule.verb
                    && rule.rights.some(ruleRight => {

                        return (
                            (
                                ruleRight.content === right.content
                                // we can compare word here
                                // for exemple :
                                //  [baba is not all] disables [baba is all]
                            )
                            && ruleRight.not

                        )
                    })
                    && this.isAllConditionRuleChecked(levelState, textOrObject, rule)
                )
            })
        );
    }

    private isAllConditionRuleChecked = (levelState: LevelState, textOrObject: TextOrObject | null, rule: Rule): boolean => {
        return (
            (
                textOrObject === null
                && rule.conditions.length === 0
                && !rule.lonely
            ) || (
                textOrObject !== null
                && this.isLonelyChecked(levelState, rule, textOrObject)
                && rule.conditions.every(condition => this.isConditionRuleCheckedWithParentLevel(levelState, condition, textOrObject))
            )
        );
    }

    private calcIsTransformPartForbidden = (
        levelState: LevelState,
        rules: Rule[],
        currentRule: Rule,
        left: LeftRightBaseRule,
        right: LeftRightBaseRule,
        textOrObject: TextOrObject | null
    ) => {

        return (
            left.content !== right.content // A is B => disable [A is B] ?
            && !Quality.qualities.includes(right.content)
            && !right.not
            && right.content !== Group.GROUP
            && currentRule.verb === Verb.IS
            && rules.some(rule => (
                // baba is baba
                rule !== currentRule
                && rule.verb === Verb.IS
                && this.isAllConditionRuleChecked(levelState, textOrObject, rule)
                && rule.lefts.some(ruleLeft => (
                    this.isLeftLeftRuleChecked(left, ruleLeft, textOrObject)
                    && rule.rights.some(ruleRight => {
                        return (
                            !ruleRight.not

                            && !Quality.qualities.includes(ruleRight.content)
                            && this.isLeftLeftRuleChecked(ruleLeft, ruleRight, textOrObject)
                            && !ruleLeft.disableRights.includes(ruleRight.content)
                            /*
                            we test : "A is B"
                            we see a rule : "A is A"
                            but "A is A" is disabled because we have anothe rule "A is not A"
    
                            => "A is A".disableRights.include('A') is true
                            => "A is B" is not disabled
                            */
                        )
                    })
                )
                )
            )
            )
        );
    }

    public applyDisable = (
        levelState: LevelState,
        rules: Rule[],
        filter: (
            levelState: LevelState,
            rules: Rule[],
            currentRule: Rule,
            left: LeftRightBaseRule,
            right: LeftRightBaseRule,
            textOrObject: TextOrObject | null
        ) => boolean
    ) => {
        rules.forEach(rule => {
            rule.lefts.forEach(left => {
                rule.rights.filter(right =>
                    filter(levelState, rules, rule, left, right, null)
                ).forEach(right => {
                    left.disableRights.push(right.content);
                });
            });
        });
    }

    public calcDisabledRules = (levelState: LevelState, rules: Rule[]) => {
        //  a is not b, a is b => [a is b] rule is disabled
        this.applyDisable(levelState, rules, this.calcIsForbiddenPart);

        // a is a, a is b  => [a is b] rule is disabled, [a is not b] rule is not disabled
        this.applyDisable(levelState, rules, this.calcIsTransformPartForbidden);
    }

    public calcSelfDestroy = (levelState: LevelState, rules: Rule[], treatmentFn: (obj: TextOrObject) => void) => {
        // a is not a => a
        levelState.textOrObjects
            .filter(textOrObject => textOrObject.isObject)
            .filter(textOrObject =>
                rules.some(
                    rule => (
                        rule.lefts.some(left => {
                            let generated = this.specificTerm(left, textOrObject);
                            return (
                                // rule is true for object 
                                this.isbasicLeftChecked(left.content, left.not, textOrObject.isObject, textOrObject.nbGroup, textOrObject.name, textOrObject.isLevel)
                                && rule.rights.some(right => (
                                    right.not
                                    && !left.disableRights.includes(right.content)
                                    && (right.content === generated) // not bird is not baba => baba is not baba => self destroy
                                    && (!Quality.qualities.includes(right.content))

                                    // condition is true
                                    && this.isRuleChecked(
                                        levelState,
                                        rules,
                                        rule,
                                        left,
                                        right,
                                        textOrObject
                                    )
                                ))
                            )
                        })
                    )
                )
            )
            .forEach(textOrObject => {
                treatmentFn(textOrObject);
            })
            ;
    }

    public processMultipleQualities = (
        levelState: LevelState,
        rules: Rule[],
        quality: string,
        treatmentFn: (obj: TextOrObject) => void,
        treatmentRule: (rule: Rule, contents: string[]) => void = () => { },
        resetFn: (obj: TextOrObject) => void = () => { }
    ) => {
        let positives: TextOrObject[] = [];
        rules.forEach(rule => {
            rule.lefts.forEach(left => {
                rule.rights.forEach(right => {
                    if (
                        !right.not
                        && !left.disableRights.includes(right.content)
                        && quality === right.content
                    ) {
                        treatmentRule(rule, this.generateContent(levelState, left.content, left.not));
                        levelState.textOrObjects.filter(
                            textOrObject => (
                                this.isbasicLeftChecked(left.content, left.not, textOrObject.isObject, textOrObject.nbGroup, textOrObject.name, textOrObject.isLevel)
                                && this.isRuleChecked(levelState, rules, rule, left, right, textOrObject)
                            )
                        ).forEach(too => {
                            positives.push(too);
                        });
                    }
                });
            });
        });
        levelState.textOrObjects.forEach(too => {
            resetFn(too);
        })
        positives.forEach(too => {
            treatmentFn(too);
        })
    };

    public get group(): string[] {
        return this._group;
    }
    public set group(value: string[]) {
        this._group = value;
    }
}