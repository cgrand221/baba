import { ConditionRule } from "../Models/ConditionRule";
import { LeftRightBaseRule } from "../Models/LeftRightBaseRule";
import { LevelState } from "../Models/LevelState";
import { Rule } from "../Models/Rule";
import { And } from "../Models/Rules/And";
import { Condition } from "../Models/Rules/Condition";
import { Group } from "../Models/Rules/Group";
import { Lonely } from "../Models/Rules/Lonely";
import { Not } from "../Models/Rules/Not";
import { Quality } from "../Models/Rules/Quality";
import { Verb } from "../Models/Rules/Verb";
import { TextOrObject } from "../Models/TextOrObject";
import { CombinaisonManager } from "./CombinaisonManager";
import { LezerManager } from "./Lezer/LezerManager";
import { RuleFinderManager } from "./RuleFinderManager";
export class RuleCompileManager {
    private static _instance: RuleCompileManager | null = null;
    private static _langageWords = [
        ...Verb.verbs,
        ...Quality.qualities,
        Not.NOT,
        And.AND,
        Lonely.LONELY,
        ...Condition.conditions,
    ];

    private _lezerManager: LezerManager;
    private _combinaisonManager: CombinaisonManager;
    private _ruleFinderManager: RuleFinderManager;
    private _wordsToMatch: string[];

    private constructor() {
        this._ruleFinderManager = RuleFinderManager.instance;
        this._lezerManager = LezerManager.instance;
        this._combinaisonManager = CombinaisonManager.instance;
        this._wordsToMatch = [];
    }

    public init = (levelState: LevelState) => {
        this._wordsToMatch = levelState.getWordsToMatch();
    }

    public static get instance(): RuleCompileManager {
        if (!RuleCompileManager._instance) {
            RuleCompileManager._instance = new RuleCompileManager();
        }
        return RuleCompileManager._instance;
    }

    public getRules = (levelState: LevelState, isVertical: boolean): Rule[] => {
        let rules: Rule[] = [];
        let textWordss: TextOrObject[][] = [];

        let iLimit = (isVertical ? levelState.level.width : levelState.level.height);
        let jLimit = (isVertical ? levelState.level.height : levelState.level.width);
        for (let i = 0; i < iLimit; i++) {
            for (let j = 0; j < jLimit; j++) {
                const [x, y] = isVertical ? [i, j] : [j, i];
                const textWords = levelState.getTextAndObjectsAt(x, y).filter(too => !too.isGlobalLevel && (!too.isObject || too.isWord))
               
                if (textWords.length > 0) {
                    textWordss.push(textWords);
                } else {
                    rules = rules.concat(this.generateRulesFromTextAndObjectss(textWordss));
                    textWordss = [];
                }
            }
            if (textWordss.length > 0) {
                rules = rules.concat(this.generateRulesFromTextAndObjectss(textWordss));
                textWordss = [];
            }
        }

        return rules;
    }

    private selectCombination = (
        combination:  TextOrObject[], 
        textToCompileBefore: string, 
        textToCompileAfter: string
    ): TextOrObject[] => {

        let begin = textToCompileBefore.indexOf(textToCompileAfter);
        if(begin === -1){
            return [];
        }
        let end = begin + textToCompileAfter.length;

        let finalCombination: TextOrObject[] = [];
       
        let size = 0;
        let beginOK = false;
        let endOK = false;
        combination.forEach(too => {
            if(size === begin){
                beginOK = true;
            }
            if (size >= begin && size < end){
                finalCombination.push(too);
            }
            size += too.name.length;
            if(size === end){
                endOK = true;
            }
        });
      
        if(beginOK && endOK){
            return finalCombination;
        }        
        return [];
       
    }

    private generateRulesFromTextAndObjectss = (textAndObjectss: TextOrObject[][]): Rule[] => {
        let rules: Rule[] = [];
        let combinations = this._combinaisonManager.getCombinations(textAndObjectss, 0);
        combinations.forEach(combination => {
            rules = rules.concat(
                this.buildRules(combination)
            );
        });        
        return rules;
    }

   

    private buildRules = (combination: TextOrObject[]): Rule[] => {

        let rules: Rule[] = [];

        let textToCompileBefore = combination
            .map(too => {
                return too.name
            })
            .join('');


        

        // a b ab b is you => b ab a is you :
        let matchdItemss = this._combinaisonManager.findMatchedInTextWithCutAndMerge(textToCompileBefore, this._wordsToMatch); 

        matchdItemss = matchdItemss.map(matchdItems => matchdItems.map(matchdItem => RuleCompileManager._langageWords.includes(matchdItem) ? matchdItem.toUpperCase() : matchdItem));
        
        matchdItemss.forEach(matchdItems => {
            // we rebuild textToCompile with matched items to exclude not recognized elements
            let textToCompileAfter = matchdItems.join('').toLowerCase();

            
            
            // we rebuild combination 
            let finalCombination  = this.selectCombination(combination, textToCompileBefore, textToCompileAfter);


            if(finalCombination.length > 0){
                //assert combination.map(...).join('') === textToCompile === matchdItems.join('') :
                if(
                    textToCompileAfter !== finalCombination
                        .map(too => {
                            return too.name
                        })
                        .join('')
                ){
                    console.error('no match')
                }

                this._ruleFinderManager.init(finalCombination, textToCompileAfter, matchdItems);

                let posVerbs:number[] = [];
                let index = 0;
                while (index < matchdItems.length) {
                    let matchdItem = matchdItems[index] ?? null;
                    if (matchdItem && Verb.verbs.some(verb => verb.toUpperCase() === matchdItem)){
                        posVerbs.push(index);
                    }
                    index++;
                }

                index = 0;
                let posVeb = posVerbs[index] ?? matchdItems.length;
                let nextPosVeb = 0;
                let previousPosVeb = 0;
                
                while (posVeb < matchdItems.length) {
                    nextPosVeb = posVerbs[index+1] ?? matchdItems.length;
                    let rule = this.getValidRule(matchdItems, previousPosVeb, posVeb, nextPosVeb);
                    if(null !== rule){
                        let left =  rule.lefts[0] ?? null;
                        let right =  rule.rights[rule.rights.length-1] ?? null;
                        if(
                            null !== left
                            && null !== right
                        ){
                            this._ruleFinderManager.tagRule(rule, rule.start, right.end);
                            rules.push(rule);
                        }
                    }
                    previousPosVeb = posVeb + 1;
                    posVeb = nextPosVeb;
                    index++;
                }
            }
        });
        
        return rules;
    }

    private getValidRule = (
        matchdItems: string[], 
        previousPosVeb:number, 
        posVeb:number, 
        nextPosVeb:number
    ): Rule|null => {

        let lefts = matchdItems.slice(previousPosVeb, posVeb);
        
        let rights = matchdItems.slice(posVeb, nextPosVeb);
        let rule:Rule|null = this._lezerManager.parse(lefts, rights);

        // 0  1    2  3    4
        // on flag is keke

        if(null !== rule){
            this.setItemPos(rule, rule.lefts, rule.conditions, lefts, previousPosVeb);
            this.setItemPos(null, rule.rights, [], rights, posVeb);
        }

        return rule;
    }
    
  

    private setItemPos = (rule:Rule|null, lrbrs: LeftRightBaseRule[], conditions:ConditionRule[], subItems: string[], previousPosVeb:number) => {
        let posItems:number[] = [];
        subItems.forEach((subItem, index) => {
            for (let i=0;i < subItem.length;i++){
                posItems.push(index);
            }
        });

        let lastPos = subItems.length;
        conditions.forEach(condition => {
            condition.start = (posItems[condition.start] ?? 0) + previousPosVeb;
            condition.end = (posItems[condition.end] ?? lastPos) + previousPosVeb;
        });
        lrbrs.forEach(lrbr => {
            lrbr.start = (posItems[lrbr.start] ?? 0) + previousPosVeb;
            lrbr.end = (posItems[lrbr.end] ?? lastPos) + previousPosVeb;
        });
        if(null !== rule){
            rule.start = (posItems[rule.start] ?? 0) + previousPosVeb;
        }
    }


    

}