export class ConfigLoader{   
    
    public filePath:string = 'levels/';

    private static _instance: ConfigLoader|null = null;

    private constructor(){
    }

    public static get instance(): ConfigLoader
    {
        if(!ConfigLoader._instance){
            ConfigLoader._instance = new ConfigLoader();
        }
        return ConfigLoader._instance;
    }
}