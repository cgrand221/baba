export class CombinaisonManager{
    private static _instance: CombinaisonManager|null = null;

    private constructor(){
    }

    public static get instance(): CombinaisonManager
    {
        if(!CombinaisonManager._instance){
            CombinaisonManager._instance = new CombinaisonManager();
        }
        return CombinaisonManager._instance;
    }


    public getCombinations = <T>(letterss: T[][], startAt: number): T[][] => {
        let combinations: T[][] = [];
        let begins = letterss[startAt];
        if(undefined === begins){
            return combinations;
        }
        
        let restCombinations = this.getCombinations(letterss, startAt + 1);
        begins.forEach(begin => {
            if (restCombinations.length > 0) {
                restCombinations.forEach((restCombination) => {
                    combinations.push([begin].concat(restCombination));
                });
            }
            else {
                combinations.push([begin].concat([]));
            }
        });
        
        return combinations;
    }


    public findMatchedInText = (currentText: string, words: string[]): string[][] => {
        let combinations: string[][] = [];
        
        words.forEach(begin => {
            if (currentText.startsWith(begin)) {
                let restCombinations = this.findMatchedInText(currentText.slice(begin.length), words);
                if (restCombinations.length > 0) {
                    restCombinations.forEach((restCombination) => {
                        combinations.push([begin].concat( restCombination))
                    });
                }
                else {
                    combinations.push([begin].concat([]));
                }
            }
        });

        return combinations;
    };

    public findMatchedInTextWithCut = (currentText: string, words: string[]): string[][] => {
        let combinations: string[][] = [];
        for (let i = 0; i < currentText.length; i++) {
            combinations.push(...this.findMatchedInText(currentText.slice(i), words));
        }
        return combinations;
    }

    private isSubArray = (subArr: string[], mainArr: string[]): boolean => {
        if (subArr.length > mainArr.length) return false;
    
        for (let i = 0; i <= mainArr.length - subArr.length; i++) {
            let foundSubArray = true;
    
            for (let j = 0; j < subArr.length; j++) {
                if (mainArr[i + j] !== subArr[j]) {
                    foundSubArray = false;
                    break;
                }
            }
    
            if (foundSubArray) return true;
        }
    
        return false;
    };

    public findMatchedInTextWithCutAndMerge = (text: string, words: string[]): string[][] => {
        const matchedItems: string[][] = this.findMatchedInTextWithCut(text, words);
    
        // Filtrage des sous-tableaux pour ne garder que les combinaisons maximales
        return matchedItems.filter(
            (arr) => !matchedItems.some(
                otherArr => otherArr !== arr && this.isSubArray(arr, otherArr)
            )
        );
    }
}