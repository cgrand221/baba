# Changelog

Some notes to rebuild dependencies, if broken dependencies

## Instructions

- run commandes :
```
rm -rf node_modules
rm -rf .parcel-cache
rm -rf dist
rm package.json
rm package-lock.json

# to use node@lts in the project for this session :
volta install node@lts

# install and save devDependencies 
npm i typescript ts-jest @types/jest parcel parcel-reporter-static-files-copy @lezer/generator --save-dev

# install and save dependencies 
npm i xml2js --save

# to pin node@lts on the project for everyone new on the project
volta pin node@lts
```

- with git, restore the scrip part in ```package.json```
- test each commands:
```
npm run test
npm run bl
npm run dev
npm run deploy
npm run prepare_left
npm run prepare_right
```