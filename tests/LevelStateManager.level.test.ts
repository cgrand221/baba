import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'L': 'level',
    'B': 'baba',
    'F': 'flag',
    'R': 'rock',
    'I': 'is',
    'Y': 'you',
    'W': 'win',
    'M': 'move',
    'O': 'on',
    'E': 'melt',
    'H': 'hot',
    'K': 'weak',

    'b': 'baba',
    'f': 'flag',
    'r': 'rock',
};

scriptTest(declarations, [
    {
        grid:   "        \n"+
                "  LIY   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid:   "        \n"+
                "  LIY   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 1, 
        levelY: 0, 
    },
   
]);



scriptTest(declarations, [
    {
        grid:   "  BIY   \n"+
                "bL IW   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid:   "  BIY   \n"+
                " bLIW   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_WIN,
        levelX: 0, 
        levelY: 0, 
    },
   
]);



scriptTest(declarations, [
    {
        grid: ( "  b B IY\n"+
                "     LIW\n"+
                "     BIM").replace('b', '[>b]'),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_NOT_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( "   bB IY\n"+
                "     LIW\n"+
                "     BIM"),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_NOT_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( "    bBIY\n"+
                "     LIW\n"+
                "     BIM"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_IS_WIN,
        levelX: 0, 
        levelY: 0, 
    },
]);



scriptTest(declarations, [
    {
        grid:   "  b  BIY\n"+
                "     LIM",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid:   " b   BIY\n"+
                "     LIM",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 1, 
    },
]);



scriptTest(declarations, [
    {
        grid:   "     BIY\n"+
                "   bL IF",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid:   "     BIY\n"+
                "    bLIF",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_NEW_PARENT_RULES,
        levelX: 0, 
        levelY: 0, 
    },
]);



scriptTest(declarations, [
    {
        grid: ( " LOFIW  \n"+
                "      fb\n"+
                " BIY    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " LOFIW  \n"+
                "      fb\n"+
                " BIY    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_IS_WIN,
        levelX: 0, 
        levelY: 0, 
    },
]);


scriptTest(declarations, [
    {
        grid: ( " LOFIW  \n"+
                " RI F rb\n"+
                " BIY    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " LOFIW  \n"+
                " RI F z \n"+
                " BIY    ").replace('z', '[br]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " LOFIW  \n"+
                " RI Fbr \n"+
                " BIY    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " LOFIW  \n"+
                " RIFb f \n"+
                " BIY    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_IS_WIN,
        levelX: 0, 
        levelY: 0, 
    },
]);




scriptTest(declarations, [
    {
        grid: ( " FOLIW  \n"+
                " RI F rb\n"+
                " BIY    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " FOLIW  \n"+
                " RI F z \n"+
                " BIY    ").replace('z', '[br]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " FOLIW  \n"+
                " RI Fbr \n"+
                " BIY    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " FOLIW  \n"+
                " RIFb f \n"+
                " BIY    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " FOLIW  \n"+
                " RIF bf \n"+
                " BIY    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " FOLIW  \n"+
                " RIF  z \n"+
                " BIY    ").replace('z', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_WIN,
        levelX: 0, 
        levelY: 0, 
    },
]);



scriptTest(declarations, [
    {
        grid: ( " R IE  \n"+
                "bLIH  r\n"+
                " BIY   "),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( "bR IE  \n"+
                " LIH  r\n"+
                " BIY   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
    {
        grid: ( " bRIE  \n"+
                " LIH   \n"+
                " BIY   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
        levelX: 0, 
        levelY: 0, 
    },
   
]);



scriptTest(declarations, [
    {
        grid: ( "BIY     \n"+
                "b L IK  \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     \n"+
                " bL IK  \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);