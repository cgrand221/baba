import { KeyCode } from '../src/Models/KeyCode';
import { LevelState } from '../src/Models/LevelState';
import { scriptTest } from './scripts';

let declarations = {
    'B': 'belt',
    'C': 'baba',

    'I': 'is',
    'G': 'g',
    'H': 'h',
    'O': 'o',
    'S': 's',
    'T': 't',
    'A': 'a',
    'R': 'r',
    'F': 'fall',
    'Y': 'you',

    'g': 'ghost',
    's': 'star',
    'b': 'belt',
    'c': 'baba',
};

scriptTest(declarations, [
    {
        grid:   "B GHOSTAR F sb\n"+
                " I       I    \n"+
                " c       c    \n"+
                "CIY           ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIGHOSTARIF  g\n"+
                " c       c    \n"+
                "              \n"+
                "CIY         s ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
