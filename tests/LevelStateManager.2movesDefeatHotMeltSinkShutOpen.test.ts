import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'keke',  
    'I': 'is',
    'Y': 'you',
    'V': 'move',
     
    'U': 'shut',
    'O': 'open',
    'H': 'hot',
    'M': 'melt',
    'S': 'sink',
    'D': 'defeat',
    
   
    'b': 'baba',
    'k': 'keke',
};



scriptTest(declarations, [
    {
        grid: ( "BIY KID \n"+
                "BIV BIV \n"+
                " bk     ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY KID \n"+
                "BIV BIV \n"+
                "  kb    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY KID \n"+
                "BIV BIV \n"+
                " bk     ").replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY KID \n"+
                "BIV BIV \n"+
                "  k b   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);

scriptTest(declarations, [
    {
        grid: ( "    KIS \n"+
                "BIV BIV \n"+
                " bk     ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "    KIS \n"+
                "BIV BIV \n"+
                "  kb    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);

scriptTest(declarations, [
    {
        grid: ( "    KIS \n"+
                "BIV BIV \n"+
                " bk BIY ").replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "    KIS \n"+
                "BIV BIV \n"+
                "  k bBIY"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIH KIM \n"+
                "BIV BIV \n"+
                " bk BIY ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIH KIM \n"+
                "BIV BIV \n"+
                "  kbBIY "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIH KIM \n"+
                "BIV BIV \n"+
                " bk BIY ").replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIH KIM \n"+
                "BIV BIV \n"+
                "  k bBIY"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIM KIH \n"+
                "BIV BIV \n"+
                " bk BIY ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM KIH \n"+
                "BIV BIV \n"+
                "  kbBIY "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIM KIH \n"+
                "BIV BIV \n"+
                " bk BIY ").replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM KIH \n"+
                "BIV BIV \n"+
                "  k bBIY"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIO KIU \n"+
                "BIV BIV \n"+
                "  bk    ").replace('b', '[<b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIO KIU \n"+
                "BIV BIV \n"+
                "b  k    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIO KIU \n"+
                "BIV BIV \n"+
                "  bk    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIO KIU \n"+
                "BIV BIV \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIU KIO \n"+
                "BIV BIV \n"+
                "  bk    ").replace('b', '[<b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIU KIO \n"+
                "BIV BIV \n"+
                "b  k    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIU KIO \n"+
                "BIV BIV \n"+
                "  bk    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIU KIO \n"+
                "BIV BIV \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
    
]);
