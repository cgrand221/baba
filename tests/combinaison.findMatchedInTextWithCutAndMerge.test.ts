import { CombinaisonManager }  from "../src/Utils/CombinaisonManager";
let combinaisonManager:CombinaisonManager = CombinaisonManager.instance;

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('you', ['you', 'baba', 'is'])
    ).toEqual(
        [['you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('isyou', ['you', 'baba', 'is'])
    ).toEqual(
        [['is', 'you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('babaisyou', ['you', 'baba', 'is'])
    ).toEqual(
        [['baba', 'is', 'you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('babaisflagisyou', ['you', 'baba', 'is', 'flag'])
    ).toEqual(
        [['baba', 'is', 'flag', 'is', 'you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('babaflag', ['baba', 'flag'])
    ).toEqual(
        [['baba', 'flag']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('babaflag', ['ba', 'flag'])
    ).toEqual(
        [['ba', 'ba', 'flag']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('bababaflag', ['baba', 'ba', 'flag'])
    ).toEqual(
        [['baba', 'ba', 'flag'], ['ba', 'baba', 'flag'], ['ba', 'ba', 'ba', 'flag']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('abcdefg', ['abc', 'bcd', 'cde', 'def', 'efg'])
    ).toEqual(
        [['abc', 'def'], ['bcd', 'efg'], ['cde']]
    );

});


test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('babamoflag', ['baba', 'flag'])
    ).toEqual(
        [['baba'], ['flag']]
    );

});


test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('beltisghostarisfall', ['belt', 'baba', 'ghost', 'star', 'fall', 'is'])
    ).toEqual(
        [['belt', 'is', 'ghost'], ['star', 'is', 'fall']]
    );
});