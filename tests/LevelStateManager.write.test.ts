import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'W': 'write',
    'F': 'flag',
    'T': 'text',
    'E': 'empty',
    'A': 'all',
    'N': 'win',
    'O': 'not',
   
    'b': 'baba',
    'f': 'flag',
};

scriptTest(declarations,  [
    {
        grid:   " BIY  f\n"+
                " BWF   \n"+
                "   b IY",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY  f\n"+
                " BWF   \n"+
                "    FIY",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY f \n"+
                " BWF   \n"+
                "    FIY",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations,  [
    {
        grid:   " BIY  f\n"+
                " FWT   \n"+
                "   b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    {
        grid:   " BIY  T\n"+
                " FWT   \n"+
                "    b  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);

scriptTest(declarations,  [
    {
        grid:   " BIY  f\n"+
                " FWE   \n"+
                "   b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    {
        grid:   " BIY  E\n"+
                " FWE   \n"+
                "    b  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);

scriptTest(declarations,  [
    {
        grid:   " BIY  f\n"+
                " FWE   \n"+
                "   b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    {
        grid:   " BIY  E\n"+
                " FWE   \n"+
                "    b  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);

scriptTest(declarations,  [
    {
        grid:   " BIY  f\n"+
                " FWA   \n"+
                "   b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY  x\n"+
                " FWA   \n"+
                "    b  ").replace('x', '[BF]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   " BIY  f\n"+
                " FWN   \n"+
                "   b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY  N\n"+
                " FWN   \n"+
                "    b  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   " BIY  f \n"+
                " FWON   \n"+
                " FWN  b ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY  f \n"+
                " FWON   \n"+
                " FWN   b",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },   
]);
