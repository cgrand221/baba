import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'F': 'flag',
    'I': 'is',
    'Y': 'you',
    'S': 'swap',
    'D': 'down',
    'M': 'move',

    'b': 'baba',
    'f': 'flag',
};

scriptTest(declarations, [
    {
        grid:   "BIY FIS \n"+
                "FID FI  \n"+
                "        \n"+
                "    b   \n"+
                "    f   \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY FIS \n"+
                "FID FI  \n"+
                "        \n"+
                "    f   \n"+
                "    b   \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid:   "BIY FIS \n"+
                "FID FIM \n"+
                "        \n"+
                "    b   \n"+
                "    f   \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY FIS \n"+
                "FID FIM \n"+
                "        \n"+
                "    b   \n"+
                "    f   \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



