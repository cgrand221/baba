import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'L': 'wall',
    'N': 'win',
    'W': 'word',
    'Z': 'not',
    'R': 'near',
    'P': 'push',
    'S': 'stop',

    'b': 'baba',
    'f': 'flag',
};
;

scriptTest(declarations, [
    {
        grid:   "  IW    \n"+
                " BIY    \n"+
                " b      ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIW    \n"+
                " bIY    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIW    \n"+
                "  bIY   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid:   "  IW BIY  \n"+
                " B    IZW \n"+
                " b   b    ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIW BIY  \n"+
                " b   bIZW \n"+
                "          ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_IS_INFINITE_LOOP,
    },
]);


scriptTest(declarations, [
    {
        grid:   " BRFIF BIY \n"+
                "           \n"+
                " b f   b  f",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BRFIF BIY \n"+
                "           \n"+
                "  ff    b f",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BRFIF BIY \n"+
                "           \n"+
                "  ff     ff",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   " BRFIW BIY \n"+
                "           \n"+
                " b f       \n"+
                "  I        \n"+
                "  F        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BRFIW BIY \n"+
                "           \n"+
                "  ff       \n"+
                "  I        \n"+
                "  F        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid:   "BIY FIS   \n"+
                "   f      \n"+
                "b  I      \n"+
                " FIW      \n"+
                "          \n"+
                "          \n"+
                "fIP bf    ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY FIS   \n"+
                "   f      \n"+
                " b I      \n"+
                " FIW      \n"+
                "          \n"+
                "          \n"+
                "fIP  bf   ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY FIS   \n"+
                "   f      \n"+
                "   I      \n"+
                " bIW      \n"+
                " F        \n"+
                "          \n"+
                "fIP  bf   ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY FIS   \n"+
                "   f      \n"+
                "   I      \n"+
                "  IW      \n"+
                " b        \n"+
                " F        \n"+
                "fIP  bf   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY FIS   \n"+
                "   f      \n"+
                "   I      \n"+
                "  IW      \n"+
                "  b       \n"+
                " F        \n"+
                "fIP  bf   ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);