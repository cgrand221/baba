import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'key',
    'D': 'door',
    'O': 'open',
    'U': 'shut',
    'I': 'is',
    'Y': 'you',
    'A': 'wall',
    'W': 'weak',
    'P': 'push',
    'S': 'stop',
    'M': 'move',

    'b': 'baba',
    'a': 'wall',
    'k': 'key',
    'd': 'door',
};


scriptTest(declarations, [
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "   b a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "     a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "  bI a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "   bIa \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "    Ia \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "  bb a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "   bba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "     a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                " bbI a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "  bbIa \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "   bIa \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW    \n"+
                "    Ia \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY BIP\n"+
                "BIW AIS\n"+
                "  bb a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIP\n"+
                "BIW AIS\n"+
                "   bba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIP\n"+
                "BIW AIS\n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIP\n"+
                "BIW AIS\n"+
                "     a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY BIP\n"+
                "BIW AIS\n"+
                " bbI a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIP\n"+
                "BIW AIS\n"+
                "  bbIa \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIP\n"+
                "BIW AIS\n"+
                "   bIa \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid:   "BIY DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                " bk d      ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "  bkd      ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "   b       ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "           ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "BIY DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                " bx d      ").replace('x', '[Ik]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "  bxd      ").replace('x', '[Ik]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "   xd      ").replace('x', '[Ik]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY  \n"+
                "BIW  \n"+
                " b d "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY  \n"+
                "BIW  \n"+
                "  bd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY  \n"+
                "BIW  \n"+
                "   d "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "BIM    \n"+
                "BIW    \n"+
                "   b a \n"+
                "       ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM    \n"+
                "BIW    \n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM    \n"+
                "BIW    \n"+
                "     a \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "BIM AIS\n"+
                "BIW    \n"+
                "   b a \n"+
                "       ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM AIS\n"+
                "BIW    \n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM AIS\n"+
                "BIW    \n"+
                "   b a \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIM AIS\n"+
                "BIW    \n"+
                "  bI a \n"+
                "       ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIM AIS\n"+
                "BIW    \n"+
                "   bIa \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM AIS\n"+
                "BIW    \n"+
                "  b Ia \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                "  kb a \n"+
                "       ").replace('b', '[>b]').replace('k', '[>k]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                "   kba \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                "   bka \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                "  bk a \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                " kbI a \n"+
                "       ").replace('b', '[>b]').replace('k', '[>k]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                "  kbIa \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                "  bkIa \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "AIS    \n"+
                "BIM BIW\n"+
                "KIM KIW\n"+
                " bk Ia \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
   
]);


// not real game

scriptTest(declarations, [
    {
        grid: ( "BIM BIP\n"+
                "BIW AIS\n"+
                "  bb a \n"+
                "       ").replace('b', '[>b]').replace('k', '[>k]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIM BIP\n"+
                "BIW AIS\n"+
                "   bba \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM BIP\n"+
                "BIW AIS\n"+
                "  bb a \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM BIP\n"+
                "BIW AIS\n"+
                " bb  a \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

// not real game

scriptTest(declarations, [
    {
        grid: ( "BIM BIP\n"+
                "BIW AIS\n"+
                " bbI a \n"+
                "       ").replace('b', '[>b]').replace('k', '[>k]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIM BIP\n"+
                "BIW AIS\n"+
                "  bbIa \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM BIP\n"+
                "BIW AIS\n"+
                " bb Ia \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM BIP\n"+
                "BIW AIS\n"+
                "bb  Ia \n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIM DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                " bk d      ").replace('b', '[>b]').replace('k', '[>k]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIM DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "  bkd      ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "   b       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "    b      ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "BIM DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                " bx d      ").replace('b', '[>b]').replace('x', '[Ik]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                "  bxd      ").replace('x', '[Ik]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS DIU\n"+
                "BIW KIP KIO\n"+
                "           \n"+
                " b xd      ").replace('x', '[Ik]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                " ba    ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                "  ab   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                "  a  b "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                "  a  b "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                "  ab   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                " ba    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                " ba    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BIM\n"+
                "BIW    \n"+
                "  ab   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                " bk d  ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },

    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                "  bkd  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                "   bd  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                "  b d  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                "bIk d  ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },

    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                " bIkd  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                "  bId  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS\n"+
                "KIW KIP\n"+
                " b Id  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);