import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'P': 'push',
    'F': 'flag',
    'S': 'stop',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [

    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "   bb   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "    bb  \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "   bb   \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "  bb    \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                " bb     \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "bb      \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "bb      \n"+
                "        ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid:   " BIY   \n"+
                " BIP   \n"+
                "    bb \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY   \n"+
                " BIP   \n"+
                "     bb\n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY   \n"+
                " BIP   \n"+
                "     bb\n"+
                "       ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "  bFb   \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                " bFb    \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "bFb     \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "bFb     \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "  bFb   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "   bFb  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "    bFb \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIP    \n"+
                " BIY    \n"+
                "     bFb\n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                "     bFb\n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid:   "        \n"+
                " BIY    \n"+
                "  bFb   \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                " bFb    \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                "bFb     \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIY    \n"+
                "xb      \n"+
                "        ").replace('x','[Fb]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIY    \n"+
                "xb      \n"+
                "        ").replace('x','[Fb]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "        \n"+
                " BIY    \n"+
                "  bFb   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                "   bFb  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                "    bFb \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                "     bFb\n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIY    \n"+
                "      bx\n"+
                "        ").replace("x", "[Fb]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                "  bFx   \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                " bFx    \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                "bFx     \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                "bFx     \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },

]);



scriptTest(declarations, [
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                "    bFx \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                "     bFx\n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIP    \n"+
                " BIY    \n"+
                "     bFx\n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( " BIS    \n"+
                " BIY    \n"+
                "  bFx   \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIS    \n"+
                " BIY    \n"+
                " bFx    \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIS    \n"+
                " BIY    \n"+
                "bFx     \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIS    \n"+
                " BIY    \n"+
                "bFx     \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },

]);



scriptTest(declarations, [
    {
        grid: ( " BIS    \n"+
                " BIY    \n"+
                "    bFx \n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIS    \n"+
                " BIY    \n"+
                "     bFx\n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIS    \n"+
                " BIY    \n"+
                "     bFx\n"+
                "        ").replace("x", "[bb]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( " FIP    \n"+
                " BIY    \n"+
                "  by    \n"+
                "        ").replace(/y/g, "[bf]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " FIP    \n"+
                " BIY    \n"+
                "   by   \n"+
                "        ").replace(/y/g, "[bf]"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

