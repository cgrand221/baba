import { CombinaisonManager } from "../src/Utils/CombinaisonManager";

let combinaisonManager: CombinaisonManager = CombinaisonManager.instance;


test('ruleCompileManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('babaisyou', [
            'baba',
            'is',
            'you',
        ])
    ).toEqual(
        [[
            'baba',
            'is',
            'you',
        ]]
    )
});

test('ruleCompileManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('youisbaba', [
            'baba',
            'is',
            'you',
        ])
    ).toEqual(
        [[
            'you',
            'is',
            'baba',
        ]]
    )
});


test('ruleCompileManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('youisbaba', [
            'baba',
            'is',
        ])
    ).toEqual(
        [[
            'is',
            'baba',
        ]]
    )
});




test('ruleCompileManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('babaisyouflagiswin', [
            'baba',
            'is',
            'you',
            'win',
            'flag',
        ])
    ).toEqual(
        [[
            'baba',
            'is',
            'you',
            'flag',
            'is',
            'win',
        ]]
    )
});


