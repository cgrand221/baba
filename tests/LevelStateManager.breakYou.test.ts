import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [
    {
        grid:   "   b    \n"+
                " BIY    \n"+
                "   f    \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIb    \n"+
                "   X    \n"+
                "        ").replace('X', '[Yf]'),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIb    \n"+
                "   X    \n"+
                "        ").replace('X', '[Yf]'),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);
