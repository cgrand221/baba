import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',
    'G': 'group',
    'O': 'on',
    'N': 'not',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [
    {
        grid: ( " BIG       \n"+
                " GIY       \n"+
                "   b       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIG       \n"+
                " GIY       \n"+
                "    b      "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( " BIY BIG   \n"+
                " GINY      \n"+
                "           \n"+
                "   b       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY BIG   \n"+
                " GINY      \n"+
                "           \n"+
                "   b       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( " BIY BOFIG \n"+
                " GINY      \n"+
                "           \n"+
                "   b    x  ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY BOFIG \n"+
                " GINY      \n"+
                "           \n"+
                "    b   x  ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( " GIY BIG   \n"+
                " BING      \n"+
                "           \n"+
                "   b f     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " GIY BIG   \n"+
                " BING      \n"+
                "           \n"+
                "   b f     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( " GIY BIG   \n"+
                " GINB      \n"+
                "           \n"+
                "   b f     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " GIY BIG   \n"+
                " GINB      \n"+
                "           \n"+
                "     f     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( " GIY BIG   \n"+
                " GING      \n"+
                "           \n"+
                "   b f     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " GIY BIG   \n"+
                " GING      \n"+
                "           \n"+
                "   b f     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_INFINITE_LOOP,
    },
]); 



scriptTest(declarations, [
    {
        grid: ( " BIY BOFIG \n"+
                " GING      \n"+
                "           \n"+
                "   b f     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY BOFIG \n"+
                " GING      \n"+
                "           \n"+
                "    bf     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY BOFIG \n"+
                " GING      \n"+
                "           \n"+
                "     x     ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_INFINITE_LOOP,
    },
    {
        grid: ( " BIY BOFIG \n"+
                " GING      \n"+
                "           \n"+
                "     x     ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_INFINITE_LOOP,
    },
]);


scriptTest(declarations, [
    {
        grid: ( " BIG BIY   \n"+
                " GIG       \n"+
                "   b       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIG BIY   \n"+
                " GIG       \n"+
                "    b      "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);