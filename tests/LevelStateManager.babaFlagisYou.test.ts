import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "   b    \n"+
                "    f   ").replace('X', '[BF]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "   b    \n"+
                "    f   ").replace('X', '[BF]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "    b   \n"+
                "     f  ").replace('X', '[BF]'),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "        \n"+
                "    bf  ").replace('X', '[BF]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "        \n"+
                "     bf ").replace('X', '[BF]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "        \n"+
                "      bf").replace('X', '[BF]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "        \n"+
                "       x").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "       x\n"+
                "        ").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "      x \n"+
                "        ").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "     x  \n"+
                "        ").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "    x   \n"+
                "        ").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " XIY    \n"+
                "   x    \n"+
                "        ").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "   Y    \n"+
                " XIx    \n"+
                "        \n"+
                "        ").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "   Y    \n"+
                " XIx    \n"+
                "        \n"+
                "        ").replace('X', '[BF]').replace('x', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);
