import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'P': 'push',
    'F': 'flag',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [

    {
        grid:   "        \n"+
                " BIY    \n"+
                "bbb     \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                " bbb    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    
]);
