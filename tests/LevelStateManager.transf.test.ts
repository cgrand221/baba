import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'F': 'flag',
    'K': 'keke',
    'I': 'is',
    'Y': 'you',
    'W': 'win',

    'b': 'baba',
    'f': 'flag',
    'k': 'keke',
};


scriptTest(declarations, [
    {
        grid: ( "BIY KIF \n"+
                "FIW     \n"+
                "   x    ").replace('x', '[bk]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY KIF \n"+
                "FIW     \n"+
                "   y    ").replace('y', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_IS_WIN,
    },
   
]);

scriptTest(declarations, [
    {
        grid: ( "BIY KIB \n"+
                "KIB     \n"+
                "   k    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY KIB \n"+
                "KIB     \n"+
                "   x    ").replace('x', '[bb]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);