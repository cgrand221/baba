import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'W': 'wall',
    'N': 'win',
    'S': 'stop',
    'P': 'push',

    'b': 'baba',
    'w': 'wall',
};


scriptTest(declarations, [
    {
        grid:   "     Ww  \n"+
                " B   I   \n"+
                " I  bSw  \n"+
                " Y    w  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "     Ww  \n"+
                " B   I   \n"+
                " I  bSw  \n"+
                " Y    w  ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "     Ww  \n"+
                " B  bI   \n"+
                " I   Sw  \n"+
                " Y    w  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "     Ww  \n"+
                " B   bI  \n"+
                " I   Sw  \n"+
                " Y    w  ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "     Ww  \n"+
                " B  b I  \n"+
                " I   Sw  \n"+
                " Y    w  ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "     Ww  \n"+
                " B    I  \n"+
                " I  bSw  \n"+
                " Y    w  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "     Ww  \n"+
                " B    I  \n"+
                " I   bx  \n"+
                " Y    w  ").replace('x', '[Sw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    
]);


scriptTest(declarations, [
    {
        grid:   "BIY     \n"+
                "WIS     \n"+
                "WIP  b  \n"+
                "     w  \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY     \n"+
                "WIS     \n"+
                "WIP     \n"+
                "     b  \n"+
                "     w  ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY     \n"+
                "WIS     \n"+
                "WIP     \n"+
                "     b  \n"+
                "     w  ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid:   "BIY \n"+
                "BIS \n"+
                "  bb",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY \n"+
                "BIS \n"+
                " bb ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY \n"+
                "BIS \n"+
                "bb  ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY \n"+
                "BIS \n"+
                "bb  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY \n"+
                "BIS \n"+
                " bb ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY \n"+
                "BIS \n"+
                "  bb",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY \n"+
                "BIS \n"+
                "  bb",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid:   "BIY  w  \n"+
                "BIP  w  \n"+
                "WIS  w  \n"+
                "  bb w  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY  w  \n"+
                "BIP  w  \n"+
                "WIS  w  \n"+
                "   bbw  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY  w  \n"+
                "BIP  w  \n"+
                "WIS  w  \n"+
                "   bbw  ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
