import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    "B": "baba",
    "A": "water",
    "C": "cog",
    "E": "pipe",
    "F": "flag",
    "R": "rock",
    "D": "door",
    "W": "wall",
  
    "I": "is",
    "Y": "you",
    "S": "sink",
    "O": "stop",
    "T": "text",
    "H": "has",
    "P": "push",
    "N": "win",
    "U": "shut",
    "Z": "and",
    "L": "level",
    "Q": "open",
    "J": "word",
  
  
    "b": "baba",
    "a": "water",
    "c": "cog",
    "e": "pipe",
    "f": "flag",
    "r": "rock",
    "d": "door",
    "w": "wall",
    
  };


scriptTest(declarations,  [
    {
        grid:   "BIYw        ddddd   wFIJ\n"+
                "wwww wwwwwwwwwwww   wwww\n"+
                "AISw wDIUZOwLI Tb   wFIQ\n"+
                "wwwwNwwwwwwwwwwww   wwww\n"+
                "WIOw            a       \n"+
                "wwww            a       \n"+
                "   wwww    I f  a     ee\n"+
                "      w      H  a     e \n"+
                " c    w         a     e \n"+
                "  c   w         a    eee\n"+
                "      waaaaaaaaaa    e  \n"+
                "ee    waaaaaaaaaa    e c\n"+
                " e c  w         a   eee \n"+
                " e    w         a     e \n"+
                "eee c w         a     ee\n"+
                "  e   w  RIP  r a     e \n"+
                "c eee w         a ceeee \n"+
                "  e e w         a  e  e ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIYw        ddddd   wFIJ\n"+
                "wwww wwwwwwwwwwww   wwww\n"+
                "AISw wDIUZOwLITb    wFIQ\n"+
                "wwwwNwwwwwwwwwwww   wwww\n"+
                "WIOw            a       \n"+
                "wwww            a       \n"+
                "   wwww    I f  a     ee\n"+
                "      w      H  a     e \n"+
                " c    w         a     e \n"+
                "  c   w         a    eee\n"+
                "      waaaaaaaaaa    e  \n"+
                "ee    waaaaaaaaaa    e c\n"+
                " e c  w         a   eee \n"+
                " e    w         a     e \n"+
                "eee c w         a     ee\n"+
                "  e   w  RIP  r a     e \n"+
                "c eee w         a ceeee \n"+
                "  e e w         a  e  e ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_IS_NEW_PARENT_RULES,
    },
]);
