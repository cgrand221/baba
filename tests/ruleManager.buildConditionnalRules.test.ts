import { RuleCompileManager } from "../src/Utils/RuleCompileManager";
import { TextOrObject } from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";

let ruleCompileManager:RuleCompileManager = RuleCompileManager.instance;

let levelState = new LevelState(
    new Level([
        'flag',
        'baba',
        'keke',
    ], [], [], [], '', 20, 5)
);

// test contigus text to rule, no geometry consideration

let babaText1 = new TextOrObject('baba', false, 0, 0);
let babaText2 = new TextOrObject('baba', false, 0, 0);
let onText1 = new TextOrObject('on', false, 0, 0);
let onText2 = new TextOrObject('on', false, 0, 0);
let flagText1 = new TextOrObject('flag', false, 0, 0);
let flagText2 = new TextOrObject('flag', false, 0, 0);
let andText1 = new TextOrObject('and', false, 0, 0);
let kekeText1 = new TextOrObject('keke', false, 0, 0);
let isText1 = new TextOrObject('is', false, 0, 0);
let isText2 = new TextOrObject('is', false, 0, 0);
let youText1 = new TextOrObject('you', false, 0, 0);

levelState.addTextOrObject(new TextOrObject('baba', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('flag', true, 0, 0));

levelState.addTextOrObject(babaText1);
levelState.addTextOrObject(babaText2);
levelState.addTextOrObject(onText1);
levelState.addTextOrObject(onText2);
levelState.addTextOrObject(flagText1);
levelState.addTextOrObject(flagText2);
levelState.addTextOrObject(andText1);
levelState.addTextOrObject(kekeText1);
levelState.addTextOrObject(isText1);
levelState.addTextOrObject(isText2);
levelState.addTextOrObject(youText1);


// baba on flag is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    onText1.rules = [];
    flagText1.rules = [];
    isText1.rules = [];
    youText1.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            onText1,
            flagText1,
            isText1,
            youText1,
        ]
    );

    expect(
        [
            babaText1.rules.length,
            onText1.rules.length,
            flagText1.rules.length,
            isText1.rules.length,
            youText1.rules.length,
        ]

    ).toEqual(
        [
            1,
            1,
            1,
            1,
            1,
        ]
    )
});



// baba on flag and keke is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    onText1.rules = [];
    flagText1.rules = [];
    andText1.rules = [];
    kekeText1.rules = [];
    isText1.rules = [];
    youText1.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            onText1,
            flagText1,
            andText1,
            kekeText1,
            isText1,
            youText1,
        ]
    );

    expect(
        [
            babaText1.rules.length,
            onText1.rules.length,
            flagText1.rules.length,
            andText1.rules.length,
            kekeText1.rules.length,
            isText1.rules.length,
            youText1.rules.length,
        ]

    ).toEqual(
        [
            1,
            1,
            1,
            1,
            1,
            1,
            1,
        ]
    )
});



// baba and flag on keke is you => (baba and flag) (on keke) (is you)
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    andText1.rules = [];
    flagText1.rules = [];
    onText1.rules = [];
    kekeText1.rules = [];
    isText1.rules = [];
    youText1.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            andText1,
            flagText1,
            onText1,
            kekeText1,
            isText1,
            youText1,
        ]
    );

    expect(
        [
            babaText1.rules.length,
            andText1.rules.length,
            flagText1.rules.length,
            onText1.rules.length,
            kekeText1.rules.length,
            isText1.rules.length,
            youText1.rules.length,
        ]

    ).toEqual(
        [
            1,
            1,
            1,
            1,
            1,
            1,
            1,
        ]
    )
});



// baba on flag flag is you => flag is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    onText1.rules = [];
    flagText1.rules = [];
    flagText2.rules = [];
    isText1.rules = [];
    youText1.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            onText1,
            flagText1,
            flagText2,
            isText1,
            youText1,
        ]
    );


    expect(
        [
            babaText1.rules.length,
            onText1.rules.length,
            flagText1.rules.length,
            flagText2.rules.length,
            isText1.rules.length,
            youText1.rules.length,
        ]

    ).toEqual(
        [
            0,
            0,
            0,
            1,
            1,
            1,
        ]
    )
});


// baba on flag on flag is you =>  flag (on flag) (is you)
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    onText1.rules = [];
    flagText1.rules = [];
    onText2.rules = [];
    flagText2.rules = [];
    isText1.rules = [];
    youText1.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            onText1,
            flagText1,
            onText2,
            flagText2,
            isText1,
            youText1,
        ]
    );


    expect(
        [
            babaText1.rules.length,
            onText1.rules.length,
            flagText1.rules.length,
            onText2.rules.length,
            flagText2.rules.length,
            isText1.rules.length,
            youText1.rules.length,
        ]

    ).toEqual(
        [
            0,
            0,
            1,
            1,
            1,
            1,
            1,
        ]
    )
});



// baba on flag and on flag is you =>  baba (on flag) (and on flag) (is you)

test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    onText1.rules = [];
    flagText1.rules = [];
    andText1.rules = [];
    onText2.rules = [];
    flagText2.rules = [];
    isText1.rules = [];
    youText1.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            onText1,
            flagText1,
            andText1,
            onText2,
            flagText2,
            isText1,
            youText1,
        ]
    );


    expect(
        [
            babaText1.rules.length,
            onText1.rules.length,
            flagText1.rules.length,
            andText1.rules.length,
            onText2.rules.length,
            flagText2.rules.length,
            isText1.rules.length,
            youText1.rules.length,
        ]

    ).toEqual(
        [
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
        ]
    )
});





// baba and flag on keke is you => flag (on keke) (is you)
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    flagText1.rules = [];
    onText2.rules = [];
    kekeText1.rules = [];
    isText1.rules = [];
    youText1.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            flagText1,
            onText2,
            kekeText1,
            isText1,
            youText1,
        ]
    );


    expect(
        [
            babaText1.rules.length,
            flagText1.rules.length,
            onText2.rules.length,
            kekeText1.rules.length,
            isText1.rules.length,
            youText1.rules.length,
        ]

    ).toEqual(
        [
            0,
            1,
            1,
            1,
            1,
            1,
        ]
    )
});


// baba on flag is keke on flag is baba => baba (on flag) (is keke), keke (on flag) (is baba)

test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules = [];
    onText1.rules = [];
    flagText1.rules = [];
    isText1.rules = [];
    kekeText1.rules = [];
    onText2.rules = [];
    flagText2.rules = [];
    isText2.rules = [];
    babaText2.rules = [];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            onText1,
            flagText1,
            isText1,
            kekeText1,
            onText2,
            flagText2,
            isText2,
            babaText2,
        ]
    );

    expect(
        [
            babaText1.rules.length,
            onText1.rules.length,
            flagText1.rules.length,
            isText1.rules.length,
            kekeText1.rules.length,
            onText2.rules.length,
            flagText2.rules.length,
            isText2.rules.length,
            babaText2.rules.length,
        ]

    ).toEqual(
        [
            1,
            1,
            1,
            1,
            2,
            1,
            1,
            1,
            1,
        ]
    )
});

// on flag is keke =>  flag is keke
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    onText1.rules = [];
    flagText1.rules = [];
    isText1.rules = [];
    kekeText1.rules = [];
   

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            onText1,
            flagText1,
            isText1,
            kekeText1,
        ]
    );

    expect(
        [
            onText1.rules.length,
            flagText1.rules.length,
            isText1.rules.length,
            kekeText1.rules.length,
        ]

    ).toEqual(
        [
            0,
            1,
            1,
            1,
        ]
    )
});


// and on flag is keke =>  flag is keke
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    andText1.rules = [];
    onText1.rules = [];
    flagText1.rules = [];
    isText1.rules = [];
    kekeText1.rules = [];
   

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            andText1,
            onText1,
            flagText1,
            isText1,
            kekeText1,
        ]
    );

    expect(
        [
            andText1.rules.length,
            onText1.rules.length,
            flagText1.rules.length,
            isText1.rules.length,
            kekeText1.rules.length,
        ]

    ).toEqual(
        [
            0,
            0,
            1,
            1,
            1,
        ]
    )
});