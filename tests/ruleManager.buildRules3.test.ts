import { TextOrObject } from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";
import { Rule } from "../src/Models/Rule";
import { RuleCompileManager } from "../src/Utils/RuleCompileManager";
import { LeftRightBaseRule } from "../src/Models/LeftRightBaseRule";

let ruleCompileManager: RuleCompileManager = RuleCompileManager.instance;

let levelState = new LevelState(
    new Level([
        'flag',
        'baba',
        'keke',
    ], [], [], [], '', 20, 5)
);

// test contigus text to rule, no geometry consideration

let babaText = new TextOrObject('baba', false, 0, 0);
let flagText = new TextOrObject('flag', false, 0, 0);
let andText = new TextOrObject('and', false, 0, 0);
let kekeText = new TextOrObject('keke', false, 0, 0);
let isText = new TextOrObject('is', false, 0, 0);
let youText = new TextOrObject('you', false, 0, 0);
let winText = new TextOrObject('win', false, 0, 0);

levelState.addTextOrObject(new TextOrObject('baba', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('flag', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('keke', true, 0, 0));



levelState.addTextOrObject(babaText);
levelState.addTextOrObject(flagText);
levelState.addTextOrObject(andText);
levelState.addTextOrObject(kekeText);
levelState.addTextOrObject(isText);
levelState.addTextOrObject(youText);
levelState.addTextOrObject(winText);

// baba and flag is you => [baba is you, flag is you]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    andText,
                    flagText,
                    isText,
                    youText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1), new LeftRightBaseRule(0, 'flag', 2, 3)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 4, 5)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// baba is you and flag => [baba is you, baba is flag]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    isText,
                    youText,
                    andText,
                    flagText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3), new LeftRightBaseRule(0, 'flag', 4, 5)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// baba and flag is you and keke => => [baba is you, flag is you, baba is keke, flag is keke]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    andText,
                    flagText,
                    isText,
                    youText,
                    andText,
                    kekeText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1), new LeftRightBaseRule(0, 'flag', 2, 3)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 4, 5), new LeftRightBaseRule(0, 'keke', 6, 7)],
                ),

            ]
        )
    ).toEqual(
        true
    )
});


// baba and is you and keke => 
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    andText,
                    isText,
                    youText,
                    andText,
                    kekeText,
                ]
            ),
            [
            ]
        )
    ).toEqual(
        true
    )
});


// and flag is you and keke => [flag is you, flag is keke]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    andText,
                    flagText,
                    isText,
                    youText,
                    andText,
                    kekeText,
                ]
            ),
            [
                new Rule(
                    1,
                    [new LeftRightBaseRule(0, 'flag', 1, 2)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 3, 4), new LeftRightBaseRule(0, 'keke', 5, 6)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// baba and and flag is you and keke => [flag is you, flag is keke]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    andText,
                    andText,
                    flagText,
                    isText,
                    youText,
                    andText,
                    kekeText,
                ]
            ),
            [
                new Rule(
                    3,
                    [new LeftRightBaseRule(0, 'flag', 3, 4)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 5, 6), new LeftRightBaseRule(0, 'keke', 7, 8)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// flag is is you and keke => []
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    flagText,
                    isText,
                    isText,
                    youText,
                    andText,
                    kekeText,
                ]
            ),
            [
            ]
        )
    ).toEqual(
        true
    )
});


// baba and you is keke => []
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    andText,
                    youText,
                    isText,
                    kekeText,
                ]
            ),
            [
            ]
        )
    ).toEqual(
        true
    )
});




// you and baba is keke => [baba is keke]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    youText,
                    andText,
                    babaText,
                    isText,
                    kekeText,
                ]
            ),
            [
                new Rule(
                    2,
                    [new LeftRightBaseRule(0, 'baba', 2, 3)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'keke', 4, 5)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});



// and baba and flag is you and keke => [baba is you, baba is keke, flag is you, flag is keke]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    andText,
                    babaText,
                    andText,
                    flagText,
                    isText,
                    youText,
                    andText,
                    kekeText,
                ]
            ),
            [
                new Rule(
                    1,
                    [new LeftRightBaseRule(0, 'baba', 1, 2), new LeftRightBaseRule(0, 'flag', 3, 4)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 5, 6), new LeftRightBaseRule(0, 'keke', 7, 8)],
                ),

            ]
        )
    ).toEqual(
        true
    )
});