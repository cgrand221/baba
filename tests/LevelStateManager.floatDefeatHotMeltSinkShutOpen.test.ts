import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'keke',  
    'R': 'robot',  
    'I': 'is',
    'Y': 'you',
    'F': 'float',
    'W': 'win',
     
    'U': 'shut',
    'O': 'open',
    'H': 'hot',
    'M': 'melt',
    'S': 'sink',
    'D': 'defeat',
    'T': 'stop',
    'P': 'push',
    
   
    'b': 'baba',
    'k': 'keke',
    'r': 'robot',
};



scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KID KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KID KI F\n"+
                "  k     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KID KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KID KI F\n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KID KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KID KIF \n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KID KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KID KIF \n"+
                "  k     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);




scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIS KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIS KI F\n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIS KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIS KI F\n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIS KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIS KIF \n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIS KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIS KIF \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIM KI F\n"+
                " bk BIH "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIM KI F\n"+
                "  b BIH "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIM KI F\n"+
                " bk BIH "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIM KI F\n"+
                "  x BIH ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIM KIF \n"+
                " bk BIH "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIM KIF \n"+
                "  x BIH ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIM KIF \n"+
                " bk BIH "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIM KIF \n"+
                "  b BIH "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);






scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIH KI F\n"+
                " bk BIM "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIH KI F\n"+
                "  k BIM "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIH KI F\n"+
                " bk BIM "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIH KI F\n"+
                "  x BIM ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIH KIF \n"+
                " bk BIM "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIH KIF \n"+
                "  x BIM ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIH KIF \n"+
                " bk BIM "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIH KIF \n"+
                "  k BIM "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);






scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIU KI F\n"+
                " bk BIO "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIU KI F\n"+
                "    BIO "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations,  [
    {
        grid: ( "BIY BIF \n"+
                "KIU KI F\n"+
                " bk BIO "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIU KI F\n"+
                "  x BIO ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIU KIF \n"+
                " bk BIO "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIU KIF \n"+
                "  x BIO ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIU KIF \n"+
                " bk BIO "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIU KIF \n"+
                "    BIO "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);




scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIO KI F\n"+
                " bk BIU "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIO KI F\n"+
                "    BIU "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIO KI F\n"+
                " bk BIU "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIO KI F\n"+
                "  x BIU ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIO KIF \n"+
                " bk BIU "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIO KIF \n"+
                "  x BIU ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIO KIF \n"+
                " bk BIU "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIO KIF \n"+
                "    BIU "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);





scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIW KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIW KI F\n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_WIN,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIW KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIW KI F\n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);




scriptTest(declarations,  [
    {
        grid: ( "BIY BI F\n"+
                "KIW KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIW KIF \n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIW KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIW KIF \n"+
                "  x     ").replace('x', '[bk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_WIN,
    },
    
]);





scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIT KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIT KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIT KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIT KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);




scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIT KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIT KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);




scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIT KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIT KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);




scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIP KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIP KI F\n"+
                "  bk    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);




scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIP KI F\n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIP KI F\n"+
                "  bk    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);




scriptTest(declarations, [
    {
        grid: ( "BIY BI F\n"+
                "KIP KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BI F\n"+
                "KIP KIF \n"+
                "  bk    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIY BIF \n"+
                "KIP KIF \n"+
                " bk     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIF \n"+
                "KIP KIF \n"+
                "  bk    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);

