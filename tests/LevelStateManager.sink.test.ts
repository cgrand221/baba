import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',
    'S': 'sink',
    'P': 'push',
    'O': 'stop',
    'T': 'text',
    'D': 'defeat',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [
    {
        grid:   " BIY \n"+
                " BIS \n"+
                "  bb ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " bb  ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                "bb   ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                "     ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                "  bb ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                " bb  ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                "bb   ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                "bb   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                " bb  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                "  bb ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                "   bb",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIS \n"+
                " BIP \n"+
                "   bb",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " BIS \n"+
                "  bf ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " BIS \n"+
                "     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " FIO \n"+
                "f b f",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " FIO \n"+
                "f  bf",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " FIO \n"+
                "f  bf",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " FIO \n"+
                "f b f",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " FIO \n"+
                "fb  f",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " FIS \n"+
                " FIO \n"+
                "fb  f",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid:   " BIY \n"+
                " BIO \n"+
                " FIS \n"+
                " b f ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIO \n"+
                " FIS \n"+
                "  bf ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY \n"+
                " BIO \n"+
                " FIS \n"+
                "     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   " TIY   \n"+
                "       \n"+
                " FIS f \n"+
                " b     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "  TIY  \n"+
                "       \n"+
                "  FISf \n"+
                " b     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "   TIY \n"+
                "       \n"+
                "   FI  \n"+
                " b     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid:   " TIYb  \n"+
                "       \n"+
                " TIS   \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "  TI   \n"+
                "       \n"+
                "  TIS  \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   " TIYb  \n"+
                " BID   \n"+
                " TIS   \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "  TI   \n"+
                "  BID  \n"+
                "  TIS  \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);

