import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'R': 'rock',
    'B': 'baba',
    'L': 'belt',
    'O': 'on',
    'T': 'text',
    'I': 'is',
    'P': 'push',
    'H': 'shift',
    'F': 'float',
    'Y': 'you',
   
    'r': 'rock',
    'b': 'baba',
    'l': 'belt',
};

scriptTest(declarations, [
    {
        grid: ( "BIY LIH  \n"+
                "ROTIP TIF\n"+
                "  bIlr   ").replace('l', '[>l]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY LIH  \n"+
                "ROTIP TIF\n"+
                "   bxr   ").replace('x', '[Il]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY LIH  \n"+
                "ROTIP TIF\n"+
                "   bxr   ").replace('x', '[Il]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY LIH  \n"+
                "ROTIP TIF\n"+
                "    lbx  ").replace('x', '[Ir]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

