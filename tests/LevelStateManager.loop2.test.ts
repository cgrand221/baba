import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'R': 'rock',  
    'I': 'is',
    'Y': 'you',
   
    'U': 'shut',
    'O': 'open',
    'A': 'all',
    'H': 'has',
  
    'b': 'baba',
    'r': 'rock',
};




scriptTest(declarations, [
   
    
    {
        grid: ( "AHBIY    \n"+
                "AIU      \n"+
                "BI Ob    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AHBIY    \n"+
                "AIU      \n"+
                "BIOb     "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
