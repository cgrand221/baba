import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'N': 'not',
    'T': 'text',
    'I': 'is',
    'Y': 'you',
    'B': 'baba',

    'b': 'baba',
};

scriptTest(declarations, [
    {
        grid:   " NTIY   \n"+
                "        \n"+
                "   b    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " NTIY   \n"+
                "        \n"+
                "    b   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
