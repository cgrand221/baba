import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'F': 'flag',
    'I': 'is',
    'N': 'not',
    'Y': 'you',
    'A': 'and',
    'W': 'wall',
    'R': 'rock',

    'b': 'baba',
    'f': 'flag',
    'w': 'wall',
    'r': 'rock',
};


scriptTest(declarations,  [
    {
        grid:   " BINW       \n"+
                " BIW BIF BIR\n"+
                "   b        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BINW       \n"+
                " BIW BIF BIR\n"+
                "   x        ").replace('x', '[fr]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);


scriptTest(declarations,  [
    {
        grid:   " BINWANF    \n"+
                " BIW BIF BIR\n"+
                "   b        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BINWANF    \n"+
                " BIW BIF BIR\n"+
                "   r        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);


scriptTest(declarations,  [
    {
        grid:   " BIWANF     \n"+
                "     BIF BIR\n"+
                "   b        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIWANF     \n"+
                "     BIF BIR\n"+
                "   x        ").replace('x', '[rw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);


scriptTest(declarations,  [
    {
        grid:   " BINWAF     \n"+
                " BIW     BIR\n"+
                "   b        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BINWAF     \n"+
                " BIW     BIR\n"+
                "   x        ").replace('x', '[fr]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);