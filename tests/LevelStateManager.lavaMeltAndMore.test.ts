import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'L': 'lava',
    'I': 'is',
    'M': 'more',
    'A': 'and',
    'H': 'hot',
    'F': 'flag',
    'E': 'melt',

    'l': 'lava',
    'f': 'flag',
};

scriptTest(declarations, [
    {
        grid:   "LIMAH   \n"+
                "FIE     \n"+
                "        \n"+
                "l f     ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "LIMAH   \n"+
                "FIE     \n"+
                "l       \n"+
                "llf     ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "LIMAH   \n"+
                "FIE     \n"+
                "ll      \n"+
                "lll     ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);
