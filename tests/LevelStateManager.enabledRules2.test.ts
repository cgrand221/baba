import { gridNbRulesEnabled, scriptTest } from './scripts';

let declarations = {
    'B': 'baba',
    'F': 'flag',
    'K': 'keke',
    'O': 'on',
    'I': 'is',
    'N': 'not',

    'b': 'baba',
    'f': 'flag',
    'k': 'keke',
};

gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BIB    \n"+
                " BOFIK  \n"+
                "        ",
    },
    [
        0,0,0,
        1,1,1,1,1,
    ]
);

gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BINF   \n"+
                " BOKIF  \n"+
                "        ",
    },
    [
        0,0,0,0,
        1,1,1,1,1,
    ]
);
