import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'T': 'text',
    'I': 'is',
    'Y': 'you',
    'G': 'bug',
    'F': 'flag',
    'W': 'win',
    'D': 'defeat',
    'P': 'push',

    'b': 'baba',
    'f': 'flag',
    'g': 'bug',
};


scriptTest(declarations, [
    {
        grid:   " BIY    \n"+
                " GID    \n"+
                "   bg   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY    \n"+
                " GID    \n"+
                "    g   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   " BIY    \n"+
                " GID    \n"+
                " b bg   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY    \n"+
                " GID    \n"+
                "  b g   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY    \n"+
                " GID    \n"+
                "   bg   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   " BIY   G\n"+
                " GID   I\n"+
                "   bg  P\n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY   G\n"+
                " GID   I\n"+
                "    bg P\n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY   G\n"+
                " GID   I\n"+
                "     bgP\n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY   G\n"+
                " GID   I\n"+
                "     bgP\n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY   G\n"+
                " GID   I\n"+
                "    b gP\n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid:   " BIY    \n"+
                "bB ID   \n"+
                "  I     \n"+
                "  W     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY    \n"+
                "  BID   \n"+
                "  I     \n"+
                "  W     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "   BIY  \n"+
                " bB ID  \n"+
                "   FIY  \n"+
                "   f    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "   BIY  \n"+
                "   BID  \n"+
                "   FIY  \n"+
                "    f   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "  TIY  \n"+
                " T ID  \n"+
                "  FIY  \n"+
                "   f   \n"+
                "       ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " TIY   \n"+
                "T ID   \n"+
                " FIY   \n"+
                "  f    \n"+
                "       ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "       \n"+
                "       \n"+
                "       \n"+
                " f     \n"+
                "       ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

