import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'D': 'door',
    'K': 'key',
    'S': 'stop',
    'W': 'weak',
    'H': 'has',
    'U': 'shut',
    'O': 'open',
    'P': 'push',

    'b': 'baba',
    'f': 'flag',
    'd': 'door',
    'k': 'key',
};


scriptTest(declarations,  [
    {
        grid:   "BIY BIW \n"+
                "FIS BHF \n"+
                "    bf  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIW \n"+
                "FIS BHF \n"+
                "    ff  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
   
]);



scriptTest(declarations, [
    {
        grid:   "BIY BIW \n"+
                "DIU DIS \n"+
                "KIP KIO \n"+
                " bk d   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIW \n"+
                "DIU DIS \n"+
                "KIP KIO \n"+
                "  bkd   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BIW \n"+
                "DIU DIS \n"+
                "KIP KIO \n"+
                "   b    ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);


scriptTest(declarations, [
    {
        grid:   "BIY KHF \n"+
                "DIU DIS \n"+
                "KIP KIO \n"+
                " bk d   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY KHF \n"+
                "DIU DIS \n"+
                "KIP KIO \n"+
                "  bkd   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY KHF \n"+
                "DIU DIS \n"+
                "KIP KIO \n"+
                "   x    " ) .replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);



scriptTest(declarations, [
    {
        grid:   "BIY BHF \n"+
                "BIW FIY \n"+
                " bk     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BHF \n"+
                "BIW FIY \n"+
                "  k     ").replace('k', '[fk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY BHF \n"+
                "BIW FIY \n"+
                "  kf    ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);
