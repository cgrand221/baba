import { TextOrObject } from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";
import { Rule } from "../src/Models/Rule";
import { RuleCompileManager } from "../src/Utils/RuleCompileManager";
import { LeftRightBaseRule } from "../src/Models/LeftRightBaseRule";

let ruleCompileManager: RuleCompileManager = RuleCompileManager.instance;

let levelState = new LevelState(
    new Level([
        'flag',
        'baba',
        'keke',
    ], [], [], [], '', 20, 5)
);

// test contigus text to rule, no geometry consideration

let babaText = new TextOrObject('baba', false, 0, 0);
let flagText = new TextOrObject('flag', false, 0, 0);
let andText = new TextOrObject('and', false, 0, 0);
let notText = new TextOrObject('not', false, 0, 0);
let kekeText = new TextOrObject('keke', false, 0, 0);
let isText = new TextOrObject('is', false, 0, 0);
let youText = new TextOrObject('you', false, 0, 0);
let winText = new TextOrObject('win', false, 0, 0);

levelState.addTextOrObject(new TextOrObject('baba', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('flag', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('keke', true, 0, 0));

levelState.addTextOrObject(babaText);
levelState.addTextOrObject(flagText);
levelState.addTextOrObject(andText);
levelState.addTextOrObject(kekeText);
levelState.addTextOrObject(isText);
levelState.addTextOrObject(youText);
levelState.addTextOrObject(winText);

// not baba is you => [flag is you, keke is you]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    notText,
                    babaText,
                    isText,
                    youText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(1, 'baba', 0, 2)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 3, 4)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// baba is not you => [baba is not you]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    isText,
                    notText,
                    youText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(1, 'you', 2, 4)],
                ),

            ]
        )
    ).toEqual(
        true
    )
});


// baba is not flag => [baba is not flag]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    isText,
                    notText,
                    flagText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(1, 'flag', 2, 4)],
                ),

            ]
        )
    ).toEqual(
        true
    )
});


// not baba is not baba => [not baba is not baba]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    notText,
                    babaText,
                    isText,
                    notText,
                    flagText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(1, 'baba', 0, 2)],
                    [],
                    'is',
                    [new LeftRightBaseRule(1, 'flag', 3, 5)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// not not baba is not not baba => [baba is baba]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    notText,
                    notText,
                    babaText,
                    isText,
                    notText,
                    notText,
                    babaText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(2, 'baba', 0, 3)],
                    [],
                    'is',
                    [new LeftRightBaseRule(2, 'baba', 4, 7)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// not not baba not flag and keke is not you => [not flag and keke is not you] 
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    notText,
                    notText,
                    babaText,
                    notText,
                    flagText,
                    andText,
                    kekeText,
                    isText,
                    notText,
                    youText,
                ]
            ),
            [

                new Rule(
                    3,
                    [new LeftRightBaseRule(1, 'flag', 3, 5), new LeftRightBaseRule(0, 'keke', 6, 7)],
                    [],
                    'is',
                    [new LeftRightBaseRule(1, 'you', 8, 10)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// not baba and not flag is keke and you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    notText,
                    babaText,
                    andText,
                    notText,
                    flagText,
                    isText,
                    kekeText,
                    andText,
                    youText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(1, 'baba', 0, 2), new LeftRightBaseRule(1, 'flag', 3, 5)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'keke', 6, 7), new LeftRightBaseRule(0, 'you', 8, 9)],
                ),

            ]
        )
    ).toEqual(
        true
    )
});


// not and baba keke is and baba => []
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    notText,
                    andText,
                    babaText,
                    kekeText,
                    isText,
                    andText,
                    babaText,
                ]
            ),
            [
            ]
        )
    ).toEqual(
        true
    )
});


// flag is baba and and keke => [flag is baba]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    flagText,
                    isText,
                    babaText,
                    andText,
                    andText,
                    kekeText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'flag', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'baba', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// flag is not not not not keke and not baba => [flag is keke and not baba]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    flagText,
                    isText,
                    notText,
                    notText,
                    notText,
                    notText,
                    kekeText,
                    andText,
                    notText,
                    babaText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'flag', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(4, 'keke', 2, 7), new LeftRightBaseRule(1, 'baba', 8, 10)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// flag is not baba is flag => [flag is not baba, not baba is flag]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    flagText,
                    isText,
                    notText,
                    babaText,
                    isText,
                    flagText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'flag', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(1, 'baba', 2, 4)],
                ),
                new Rule(
                    2,
                    [new LeftRightBaseRule(1, 'baba', 2, 4)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'flag', 5, 6)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// flag is not flag is baba => [flag is not flag, not flag is baba]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    flagText,
                    isText,
                    notText,
                    flagText,
                    isText,
                    babaText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'flag', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(1, 'flag', 2, 4)],
                ),
                new Rule(
                    2,
                    [new LeftRightBaseRule(1, 'flag', 2, 4)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'baba', 5, 6)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});
