import { CombinaisonManager }  from "../src/Utils/CombinaisonManager";
let combinaisonManager:CombinaisonManager = CombinaisonManager.instance;

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('you', ['you', 'baba', 'is'])
    ).toEqual(
        [['you']]
    );
});

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('isyou', ['you', 'baba', 'is'])
    ).toEqual(
        [['is', 'you']]
    );
});

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('babaisyou', ['you', 'baba', 'is'])
    ).toEqual(
        [['baba', 'is', 'you']]
    );
});

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('babaisflagisyou', ['you', 'baba', 'is', 'flag'])
    ).toEqual(
        [['baba', 'is', 'flag', 'is', 'you']]
    );
});

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('babaflag', ['baba', 'flag'])
    ).toEqual(
        [['baba', 'flag']]
    );
});

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('babaflag', ['ba', 'flag'])
    ).toEqual(
        [['ba', 'ba', 'flag']]
    );
});

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('bababaflag', ['baba', 'ba', 'flag'])
    ).toEqual(
        [['baba', 'ba', 'flag'], ['ba', 'baba', 'flag'], ['ba', 'ba', 'ba', 'flag']]
    );
});


test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('abcdefg', ['abc', 'bcd', 'cde', 'def', 'efg'])
    ).toEqual(
        [['abc', 'def']]
    );

});


test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('babamoflag', ['baba', 'flag'])
    ).toEqual(
        [['baba']]
    );

});


test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('beltisghostarisfall', ['belt', 'baba', 'ghost', 'star', 'fall', 'is'])
    ).toEqual(
        [['belt', 'is', 'ghost']]
    );
});


