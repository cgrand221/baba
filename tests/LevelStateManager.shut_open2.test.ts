import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'K': 'key',
    'W': 'win',
    'D': 'door',
    'T': 'stop',
    'S': 'shut',
    'O': 'open',
    'P': 'push',
    'M': 'move',

    'b': 'baba',
    'k': 'key',
    'd': 'door',
};


scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " dd  kb "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " dd kb  "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " ddkb   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " d b    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " db     "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " db     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " dd kkb "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " ddkkb  "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " d kb   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " dkb    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  b     "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " b      "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "b       "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "b       "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " dd  xb ").replace('x', '[kk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " dd xb  ").replace('x', '[kk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " ddxb   ").replace('x', '[kk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " dkb    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  b     "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  y  kb ").replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  y kb  ").replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  ykb   ").replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  db    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  db    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  y  xb ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  y xb  ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  yxb   ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   b    ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  b     ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  d  xb ").replace('x', '[Yk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  d xb  ").replace('x', '[Yk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  dxb   ").replace('x', '[Yk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  dxb   ").replace('x', '[Yk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  d  kb ").replace('b', '[<b]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  dkb   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  b     "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  d  xb ").replace('x', '[kk]').replace('b', '[<b]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  dxb   ").replace('x', '[kk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                " kb     ").replace('x', '[kk]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  y  kb ").replace('y', '[dd]').replace('b', '[<b]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  ykb   ").replace('y', '[dd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "  db    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
    
]);



scriptTest(declarations, [
    {
        grid: ( "BIM DIS BIM\n"+
                "DIT KIO BIM\n"+
                "KIP        \n"+
                "     ddkkb ").replace('b', '[<b]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS BIM\n"+
                "DIT KIO BIM\n"+
                "KIP        \n"+
                "      b    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "  d bb  "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "  dbb   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "   b    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIP     \n"+
                "  d bb  "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIP     \n"+
                "  dbb   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIP     \n"+
                "   b    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIT     \n"+
                "  d bb  "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIT     \n"+
                "  dbb   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIT     \n"+
                "   b    "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [    
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "  d x   ").replace('x', '[bb]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "  dx    ").replace('x', '[bb]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "  b     ").replace('x', '[bb]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
