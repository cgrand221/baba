import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',
    'A': 'all',
    'N': 'not',
    'P': 'push',
    'O': 'on',
    'T': 'text',

    'b': 'baba',
    'f': 'flag',
};

scriptTest(declarations, [
    {
        grid:   " AIY    \n"+
                "      f \n"+
                "   b    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " AIY    \n"+
                "       f\n"+
                "    b   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid:   "NAIY    \n"+
                "      f \n"+
                "   b    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " NAIY   \n"+
                "      f \n"+
                "   b    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "NAIY BIP   \n"+
                "FIP        \n"+
                "      f    \n"+
                "   b       ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "NAIY BIP   \n"+
                "FIP   f    \n"+
                "           \n"+
                "   b       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " NAIY BIP  \n"+
                " FIP   f   \n"+
                "           \n"+
                "    b      ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BOAIY   \n"+
                "   b    \n"+
                "   y    \n"+
                "   z    ").replace('y', '[bf]').replace('z', '[bbf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BOAIY   \n"+
                "   b    \n"+
                "   y    \n"+
                "   fz   ").replace('y', '[bf]').replace('z', '[bb]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "AIA     \n"+
                "   b    \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AIA     \n"+
                "   x    \n"+
                "        \n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "AIA     \n"+
                "   f    \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AIA     \n"+
                "   x    \n"+
                "        \n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "AINA    \n"+
                "   f    \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AINA    \n"+
                "   f    \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "AINA    \n"+
                "AIA f   \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AINA    \n"+
                "AIA f   \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "AINA    \n"+
                "BIA f   \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AINA    \n"+
                "BIA f   \n"+
                "        \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   " AIF    \n"+
                " BIB  f \n"+
                "   b    \n"+
                "        ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " AIF    \n"+
                " BIB  f \n"+
                "   b    \n"+
                "        ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

scriptTest(declarations,  [
    {
        grid:   " AINF   \n"+
                " BIF    \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " AINF   \n"+
                " BIF    \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   " BINA   \n"+
                " BIF    \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " BINA   \n"+
                " BIF    \n"+
                "    f   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations,  [
    {
        grid:   " NAINF  \n"+
                " TIF    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " NAINF  \n"+
                " TIF    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   " AINB   \n"+
                "        \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " AINB   \n"+
                "        \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   "   AIY     \n"+
                "  BINY  fb ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "   AIY     \n"+
                "  BINY f b ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations,  [
    {
        grid:   "  NFIY     \n"+
                "  BINY  fb ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "  NFIY     \n"+
                "  BINY  fb ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( " BIY       \n"+
                " AINY      \n"+
                "           \n"+
                "   b       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY       \n"+
                " AINY      \n"+
                "           \n"+
                "   b       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);