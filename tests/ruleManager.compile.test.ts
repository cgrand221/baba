import { RuleManager } from "../src/Utils/RuleManager";
import { TextOrObject } from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";
import { Rule } from "../src/Models/Rule";
import { LeftRightBaseRule } from "../src/Models/LeftRightBaseRule";

let ruleManager: RuleManager = RuleManager.instance;

let levelState = new LevelState(
    new Level([
        'baba',
        'flag',
    ], [], [], [], '', 8, 5)
);

// test contigus text to rule, no geometry consideration

let babaText = new TextOrObject('baba', false, 1, 1);
let isText = new TextOrObject('is', false, 2, 1);
let youText = new TextOrObject('you', false, 3, 1);
let flagText = new TextOrObject('flag', false, 2, 0);
let winText = new TextOrObject('win', false, 2, 2);

levelState.addTextOrObject(babaText);
levelState.addTextOrObject(isText);
levelState.addTextOrObject(youText);
levelState.addTextOrObject(flagText);
levelState.addTextOrObject(winText);


levelState.addTextOrObject(new TextOrObject('baba', true, 5, 5));
levelState.addTextOrObject(new TextOrObject('flag', true, 5, 6));

/*
    F
B   I   Y
    W
*/
test('ruleManager.compileAndCalcProperties', () => {
    let rules: Rule[] = [];
    ruleManager.init(levelState);
    ruleManager['compileAndCalcProperties'](
        levelState,
        rules,
        true
    )
    expect(
        Rule.equalsAll(
            rules,
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3)],
                ),
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'flag', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'win', 2, 3)],
                ),
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'text', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'push', 2, 3)],
                ),
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'level', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'stop', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});



/*
    F
B   I   Y

    W
*/
test('ruleManager.compileAndCalcProperties', () => {
    winText.y = 3;
    levelState.resetIndex();
    ruleManager.init(levelState);
    let rules: Rule[] = [];
    ruleManager['compileAndCalcProperties'](
        levelState,
        rules,
        true
    )
    expect(
        Rule.equalsAll(
            rules,
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3)],
                ),
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'text', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'push', 2, 3)],
                ),
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'level', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'stop', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});

/*
    F
B   I       Y
    W
*/
test('ruleManager.compileAndCalcProperties', () => {
    winText.y = 2;
    youText.x = 4
    levelState.resetIndex();
    ruleManager.init(levelState);
    let rules: Rule[] = [];
    ruleManager['compileAndCalcProperties'](
        levelState,
        rules,
        true
    )
    expect(
        Rule.equalsAll(
            rules,
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'flag', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'win', 2, 3)],
                ),
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'text', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'push', 2, 3)],
                ),
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'level', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'stop', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});