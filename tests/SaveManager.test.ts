import {SaveManager} from '../src/Utils/SaveManager';

let saveManager:SaveManager = SaveManager.instance;

test('SaveManager.toString/fromString', () => {
    let required = '{"test/test.xml":{"isWin":"true","isBonus":"false","nbSpores":0,"nbRequiredSpores":null,"nbSporesZoneCompleted":null,"savedRules":[],"parentSavedRules":[],"select":null}}';
    saveManager.fromString(required);
    let calculated = saveManager.toString();
    expect(
        calculated
    ).toEqual(
        required
    )
});