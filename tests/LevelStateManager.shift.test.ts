import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'E': 'belt',
    'S': 'shift',
    'M': 'move',
    'O': 'on',

    'b': 'baba',
    'e': 'belt',
};

scriptTest(declarations, [
    {
        grid: ( "BIY EIS   \n"+
                " b eee    \n"+
                "          ").replace('e', '[>e]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS   \n"+
                "  beee    \n"+
                "          "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS   \n"+
                "   exe    \n"+
                "          ").replace('x', '[be]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS   \n"+
                "   eex    \n"+
                "          ").replace('x', '[be]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS   \n"+
                "   eeeb   \n"+
                "          "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS   \n"+
                "   eeeb   \n"+
                "          "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },  
]);



scriptTest(declarations, [
    {
        grid: ( "BIY EIS  B\n"+
                "     xeeI \n"+
                "         M").replace('e', '[>e]').replace('x', '[vb>e]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS  B\n"+
                "     exeI \n"+
                "         M").replace('x', '[be]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS  B\n"+
                "     eexI \n"+
                "         M").replace('x', '[be]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS  B\n"+
                "     eeebI\n"+
                "         M"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS  B\n"+
                "     eeebI\n"+
                "         M"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY EIS   \n"+
                "        be\n"+
                "          ").replace('e', '[^e]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY EIS  b\n"+
                "         e\n"+
                "          "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BIS   \n"+
                "EOBIM   be\n"+
                "          ").replace('e', '[^e]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIS  e\n"+
                "EOBIM    b\n"+
                "          ").replace('x', '[be]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "BIY BIS   \n"+
                "    EI Mbe\n"+
                "          ").replace('e', '[^e]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIS   \n"+
                "    EI M x\n"+
                "          ").replace('x', '[be]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIS   \n"+
                "    EI Mbe\n"+
                "          "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIS   \n"+
                "    EIMb e\n"+
                "          "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIS   \n"+
                "    EIMbe \n"+
                "          "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY   BIM \n"+
                "EIS  w    \n"+
                "     xy   ").replace('w', '[vb]').replace('x', '[>e]').replace('y', '[^e]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   BIM \n"+
                "EIS       \n"+
                "     ex   ").replace('x', '[be]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   BIM \n"+
                "EIS   b   \n"+
                "     ee   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);