import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'K': 'key',
    'D': 'door',
    'T': 'stop',
    'S': 'shut',
    'O': 'open',
    'P': 'push',
    'M': 'move',
    'H': 'has',
    'A': 'all',

    'b': 'baba',
    'k': 'key',
    'd': 'door',
};


scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                " bkd    ").replace('k', '[kkkk]').replace('d', '[dddd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                "  b     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIO \n"+
                "KIP KIS \n"+
                "        \n"+
                " bkd    ").replace('k', '[kkkk]').replace('d', '[dddd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIO \n"+
                "KIP KIS \n"+
                "        \n"+
                "  b     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BIO \n"+
                "BHB DIS \n"+
                "        \n"+
                "  bd    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIO \n"+
                "BHB DIS \n"+
                "        \n"+
                "  b     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BIO \n"+
                "BHB BIS \n"+
                "        \n"+
                "  b     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BIO \n"+
                "BHB BIS \n"+
                "        \n"+
                "   b    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "AHBIY DIT\n"+
                "I I      \n"+
                "S O b d  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AHBIY DIT\n"+
                "I I      \n"+
                "S O  bd  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "AHBIY DIT\n"+
                "I I      \n"+
                "S O  bb  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
