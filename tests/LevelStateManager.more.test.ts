import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'M': 'more',

    'b': 'baba',
    'f': 'flag',
};

scriptTest(declarations, [
    {
        grid:   "BIY  FIM \n"+
                "      f  \n"+
                "   b     \n"+
                "         ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY  FIM \n"+
                "     fff \n"+
                "   b  f  \n"+
                "         ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY  FIM \n"+
                "    fffff\n"+
                "   b fff \n"+
                "      f  ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY fFIMf\n"+
                "   ffffff\n"+
                "   bfffff\n"+
                "     fff ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIYffFIMf\n"+
                "  fffffff\n"+
                "   xfffff\n"+
                "    fffff").replace('x', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIYffFIMf\n"+
                " ffffffff\n"+
                "  fxfffff\n"+
                "   ffffff").replace('x', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIYffFIMf\n"+
                "fffffffff\n"+
                " ffxfffff\n"+
                "  fffffff").replace('x', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIYffFIMf\n"+
                "fffffffff\n"+
                "fffxfffff\n"+
                " ffffffff").replace('x', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIYffFIMf\n"+
                "fffffffff\n"+
                "fffxfffff\n"+
                "fffffffff").replace('x', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIYffFIMf\n"+
                "fffffffff\n"+
                "fffxfffff\n"+
                "fffffffff").replace('x', '[bf]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
