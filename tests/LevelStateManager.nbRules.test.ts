import { gridNbRules } from './scripts';

let declarations = {
    'B': 'baba',
    'C': 'ba',
    'I': 'is',
    'Y': 'you',

    'b': 'baba',
};

gridNbRules(
    declarations, 
    {
        grid:   "CIY     \n"+
                "        \n"+
                "BIY     \n"+
                "        ",
    },
    [
        0,0,0,
        1,1,1,
    ]
);

gridNbRules(
    declarations, 
    {
        grid:   "CCCIY   \n"+
                "        ",
    },
    [
        0,1,1,1,1
    ]
);
