import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'R': 'robot',
    'I': 'is',
    'Y': 'you',
    'W': 'win',
    'D': 'door',
    'S': 'shut',
    'O': 'open',
    'P': 'push',
    'T': 'stop',
    'M': 'move',

    'b': 'baba',
    'd': 'door',
    'r': 'robot',
};


scriptTest(declarations, [
    {
        grid: ( "BIY  RIM\n"+
                "DIT     \n"+
                "  rD IS \n"+
                "    I   \n"+
                " bd O   ").replace('r', '[>r]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY  RIM\n"+
                "DIT     \n"+
                "   rDIS \n"+
                "    I   \n"+
                " b  O   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY  RIM\n"+
                "DIT     \n"+
                "    rDIS\n"+
                "    I   \n"+
                "  b O   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY  RIM\n"+
                "DIT     \n"+
                "   r DIS\n"+
                "    I   \n"+
                "   bO   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

