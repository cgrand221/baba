import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'wall',
    'E': 'belt',
    'H': 'shift',
    'S': 'stop',
    'L': 'level',
    'K': 'key',

    'b': 'baba',
    'f': 'flag',
    'w': 'wall',
    'e': 'belt',
    'k': 'key',
};

scriptTest(declarations, [
    {
        grid:   "BIY bEI W \n"+
                "EIH WIW   \n"+
                "WIS   weew",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY  bEIW \n"+
                "EIH WIW   \n"+
                "WIS   wwww",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "LI Kb  \n"+
                "      k\n"+
                "BIY    ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "LIKb   \n"+
                "      k\n"+
                "BIY    ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_IS_NEW_PARENT_RULES,
    },
]);

scriptTest(declarations, [
    {
        grid:   "LI Kb  \n"+
                "KIK   k\n"+
                "BIY    ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "LIKb   \n"+
                "KIK   k\n"+
                "BIY    ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_IS_NEW_PARENT_RULES,
    },
]);
