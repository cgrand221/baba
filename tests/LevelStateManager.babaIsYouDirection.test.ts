import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'keke',
    'I': 'is',
    'Y': 'you',
    'D': 'door',
    'S': 'stop',
    'M': 'move',

    'b': 'baba',
    'd': 'door',
    'k': 'keke',
};

scriptTest(declarations, [
    {
        grid: ( "BIY KIM  \n"+
                "BI Mk    \n"+
                "         \n"+
                "b        \n"+
                "         ").replace('k', '[<k]').replace('b', '[^b]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY KIM  \n"+
                "BIMk     \n"+
                "         \n"+
                "b        \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY KIM  \n"+
                "BIM k    \n"+
                "         \n"+
                " b       \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
