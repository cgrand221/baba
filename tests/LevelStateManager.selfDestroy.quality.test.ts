import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'I': 'is',
    'Y': 'you',
    'T': 'text',
    'O': 'float',
    'N': 'not',
    'P': 'stop',
    'A': 'and',
    'S': 'sink',
    'H': 'win',
    'U': 'push',



    'B': 'baba',
    'R': 'rock',
    'W': 'water',
    'G': 'grass',
    'F': 'flag',
    'K': 'keke',

    'b': 'baba',
    'r': 'rock',
    'w': 'water',
    'g': 'grass',
    'f': 'flag',
    'k': 'keke',
};


scriptTest(declarations, [
   
    
    {
        grid: ( "BIY                 \n"+
                "                    \n"+
                "     bNBI NY    wgf "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY                 \n"+
                "                    \n"+
                "      bNBINY    wgf "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);