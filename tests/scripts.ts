import { expect, test } from '@jest/globals';

import { QuickLevelStateBuild } from "../src/Utils/QuickLevelStateBuild";
import { LevelStateManager } from "../src/Utils/LevelStateManager";
import { LevelState } from "../src/Models/LevelState";
import { KeyCode } from '../src/Models/KeyCode';

let levelStateManager: LevelStateManager = LevelStateManager.instance;
let quickLevelStateBuild: QuickLevelStateBuild = QuickLevelStateBuild.instance;


export let scriptTest = (
    declarations: { [key: string]: string; },
    scripts: {
        grid: string,
        key: KeyCode,
        state: number,
        levelX?: number,
        levelY?: number,
    }[]
) => {
    test('levelState1Manager.getNextLevellState', () => {
        let script = scripts[0] ?? null;
        expect(
            null !== script
        ).toBeTruthy();

        if (null === script) {
            return;
        }
     
        let pls: LevelState | null;
        let ls = quickLevelStateBuild.buildLevelState(
            declarations,
            script.grid,
        )
        expect(
            ls
        ).toBeInstanceOf(
            LevelState
        );

        if (null === ls) {
            return;
        }
        
        for (let i = 0; i < scripts.length; i++) {
            script = scripts[i] ?? null;
            let nextScript = scripts[i + 1] ?? null;
            if (null !== script && null !== nextScript) {
                
                [pls, ls] = levelStateManager.getNextLevellState(
                    ls,
                    script.key,
                );

                expect(
                    quickLevelStateBuild.levelStateToString(declarations, ls, null)
                ).toEqual(
                    nextScript.grid
                );

                expect(
                    ls.state
                ).toEqual(
                    nextScript.state
                )

                let levelToo = ls.textOrObjects.find(too => too.isGlobalLevel);
                expect(
                    undefined === levelToo 
                    || (
                        (undefined === nextScript.levelX && undefined === nextScript.levelX)
                        || (levelToo.x === nextScript.levelX && levelToo.y === nextScript.levelY)
                    )
                ).toBeTruthy();
            }
        }
        
    });
}



export let gridNbRulesEnabled = (
    declarations: { [key: string]: string; },
    script: {
        grid: string
    },
    exprectedDisableds: number[]
) => {
    test('levelState1Manager.init', () => {
        expect(
            script
        ).toBeDefined();

        let ls = quickLevelStateBuild.buildLevelState(
            declarations,
            script.grid,
        )
        expect(
            ls
        ).toBeInstanceOf(
            LevelState
        );

        if (null === ls) {
            return;
        }

        let calculatedDisableds = (
            ls.textOrObjects
                .filter(too => !too.isGlobalLevel)// we ignore level's TextOrObject
                .map(
                    too =>
                        too.rules.filter(
                            rule => rule.calcAllRightsDisabled()
                        ).length
                )
        );

        expect(
            calculatedDisableds
        ).toEqual(
            exprectedDisableds
        );
    });
}


export let gridNbRules = (
    declarations: { [key: string]: string; },
    script: {
        grid: string
    },
    exprectedDisableds: number[]
) => {
    test('gridNbRules', () => {
        expect(
            script
        ).toBeDefined();

        let ls = quickLevelStateBuild.buildLevelState(
            declarations,
            script.grid,
        )
        expect(
            ls
        ).toBeInstanceOf(
            LevelState
        );

        if (null === ls) {
            return;
        }

        let calculatedDisableds = (
            ls.textOrObjects
                .filter(too => !too.isGlobalLevel)// we ignore level's TextOrObject
                .map(
                    too => too.rules.length
                )
        );

        expect(
            calculatedDisableds
        ).toEqual(
            exprectedDisableds
        );
    });
}