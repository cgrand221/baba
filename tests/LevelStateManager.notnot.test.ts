import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'skull',
    'I': 'is',
    'Y': 'you',
    'L': 'level',
    'D': 'defeat',

    'b': 'baba',
    'k': 'skull',
};


scriptTest(declarations, [
    {
        grid:   "KID    k\n"+
                "BIY     \n"+
                "LI Bb   ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "KID    k\n"+
                "BIY     \n"+
                "LIBb    ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_IS_NEW_PARENT_RULES,
    },
]);
