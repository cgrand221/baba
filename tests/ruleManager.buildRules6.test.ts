import { TextOrObject }  from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";
import { RuleCompileManager } from "../src/Utils/RuleCompileManager";

let ruleCompileManager: RuleCompileManager = RuleCompileManager.instance;

let levelState = new LevelState(
    new Level([
        'flag',
        'baba',
        'keke',
    ], [], [], [], '', 20, 5)
);

// test contigus text to rule, no geometry consideration

let babaText1 = new TextOrObject('baba', false, 0, 0);
let babaText2 = new TextOrObject('baba', false, 0, 0);
let flagText1 = new TextOrObject('flag', false, 0, 0);
let flagText2 = new TextOrObject('flag', false, 0, 0);
let andText1 = new TextOrObject('and', false, 0, 0);
let andText2 = new TextOrObject('and', false, 0, 0);
let andText3 = new TextOrObject('and', false, 0, 0);
let notText1 = new TextOrObject('not', false, 0, 0);
let notText2 = new TextOrObject('not', false, 0, 0);
let notText3 = new TextOrObject('not', false, 0, 0);
let notText4 = new TextOrObject('not', false, 0, 0);
let notText5 = new TextOrObject('not', false, 0, 0);
let kekeText1 = new TextOrObject('keke', false, 0, 0);
let isText1 = new TextOrObject('is', false, 0, 0);
let isText2 = new TextOrObject('is', false, 0, 0);
let hasText1 = new TextOrObject('has', false, 0, 0);
let hasText2 = new TextOrObject('has', false, 0, 0);
let youText1 = new TextOrObject('you', false, 0, 0);
let winText1 = new TextOrObject('win', false, 0, 0);

levelState.addTextOrObject(new TextOrObject('baba', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('flag', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('keke', true, 0, 0));

levelState.addTextOrObject(babaText1);
levelState.addTextOrObject(babaText2);
levelState.addTextOrObject(flagText1);
levelState.addTextOrObject(flagText2);
levelState.addTextOrObject(andText1);
levelState.addTextOrObject(andText2);
levelState.addTextOrObject(andText3);
levelState.addTextOrObject(notText1);
levelState.addTextOrObject(notText2);
levelState.addTextOrObject(notText3);
levelState.addTextOrObject(notText4);
levelState.addTextOrObject(notText5);
levelState.addTextOrObject(kekeText1);
levelState.addTextOrObject(isText1);
levelState.addTextOrObject(isText2);
levelState.addTextOrObject(hasText1);
levelState.addTextOrObject(hasText1);
levelState.addTextOrObject(youText1);
levelState.addTextOrObject(winText1);

// has baba has flag => [baba has flag]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);

    hasText1.rules=[];
    babaText1.rules=[];
    hasText2.rules=[];
    flagText1.rules=[];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            hasText1,
            babaText1,
            hasText2,
            flagText1,
        ]
    );
    
    expect(
       [
            hasText1.rules.length,
            babaText1.rules.length,
            hasText2.rules.length,
            flagText1.rules.length,
       ]
       
    ).toEqual(
        [
            0,
            1,
            1,
            1,
        ]
    )
});



// baba has flag => [baba has flag]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    babaText1.rules=[];
    hasText2.rules=[];
    flagText1.rules=[];

    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            babaText1,
            hasText2,
            flagText1,
        ]
    );
    
    expect(
       [
            babaText1.rules.length,
            hasText2.rules.length,
            flagText1.rules.length,
       ]
       
    ).toEqual(
        [
            1,
            1,
            1,
        ]
    )
});



// not baba not flag has not not not keke and you and and flag => [not flag has not not not keke]
// => [baba has not keke, keke has not keke].length === 2
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    notText1.rules=[];
    babaText1.rules=[];
    notText2.rules=[];
    flagText1.rules=[];
    hasText1.rules=[];
    notText3.rules=[];
    notText4.rules=[];
    notText5.rules=[];
    kekeText1.rules=[];
    andText1.rules=[];
    youText1.rules=[];
    andText2.rules=[];
    andText3.rules=[];
    flagText2.rules=[];
    ruleCompileManager['buildRules'](
        
        // contigous texts
        [
            notText1,
            babaText1,
            notText2,
            flagText1,
            hasText1,
            notText3,
            notText4,
            notText5,
            kekeText1,
            andText1,
            youText1,
            andText2,
            andText3,
            flagText2,
        ]
    );
   
    
    expect(
       [
        notText1.rules.length,
        babaText1.rules.length,
        notText2.rules.length,
        flagText1.rules.length,
        hasText1.rules.length,
        notText3.rules.length,
        notText4.rules.length,
        notText5.rules.length,
        kekeText1.rules.length,
        andText1.rules.length,
        babaText2.rules.length,
        andText2.rules.length,
        andText3.rules.length,
        flagText2.rules.length,
       ]
       
    ).toEqual(
        [
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
        ]
    )
});
