import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'S': 'swap',
    'P': 'pull',
    'T': 'text',
    'A': 'and',
    'M': 'move',
    'Z': 'stop',

    'b': 'baba',
    'f': 'flag',
    'W': 'wall',
};

scriptTest(declarations, [
    {
        grid:   "BIY     \n"+
                "FIS     \n"+
                "   bf   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY     \n"+
                "FIS     \n"+
                "   fb   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY     \n"+
                "BIS     \n"+
                "   bf   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY     \n"+
                "BIS     \n"+
                "   fb   \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "BIY     \n"+
                "BIS     \n"+
                "        \n"+
                "b       ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY     \n"+
                "BIS     \n"+
                "        \n"+
                "b       ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "BIM     \n"+
                "BIS     \n"+
                "        \n"+
                "b       ").replace('b', '[<b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM     \n"+
                "BIS     \n"+
                "        \n"+
                " b      ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "BIY     \n"+
                "FIS     \n"+
                "   bx   \n"+
                "        ").replace('x', '[If]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     \n"+
                "FIS     \n"+
                "   fbI  \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY     \n"+
                "BIS     \n"+
                "   bx   \n"+
                "        ").replace('x', '[If]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     \n"+
                "BIS     \n"+
                "   xb   \n"+
                "        ").replace('x', '[If]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIY FIP \n"+
                "TIS     \n"+
                "  fbY   \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY FIP \n"+
                "TIS     \n"+
                "   xb   \n"+
                "        ").replace('x', '[Yf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY TIP \n"+
                "TIS     \n"+
                "  Tb    \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TIP \n"+
                "TIS     \n"+
                "   Tb   \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY TIP \n"+
                "TIS     \n"+
                "  TbY   \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TIP \n"+
                "TIS     \n"+
                "   xb   \n"+
                "        ").replace('x', '[TY]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "BIY TIP \n"+
                "FIS     \n"+
                "  bxI   \n"+
                "        ").replace('x', '[If]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TIP \n"+
                "FIS     \n"+
                " bIIf   \n"+
                "        "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY TIPAS\n"+
                "FIS      \n"+
                "  bxI    \n"+
                "         ").replace('x', '[If]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TIPAS\n"+
                "FIS      \n"+
                " bIIf    \n"+
                "         ").replace('x', '[If]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY     \n"+
                "FIS     \n"+
                "     bIf\n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     \n"+
                "FIS     \n"+
                "      xI\n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     \n"+
                "FIS   b \n"+
                "      fI\n"+
                "        "),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
