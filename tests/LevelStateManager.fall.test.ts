import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'R': 'rock',
    'K': 'key',
    'D': 'door',
    'W': 'win',
    'F': 'fall',
    'T': 'text',
    'S': 'stop',
    'H': 'shut',
    'O': 'open',
    'P': 'push',

    'b': 'baba',
    'r': 'rock',
    'k': 'key',
    'd': 'door',
};

scriptTest(declarations, [
    {
        grid:   "  b     \n"+
                " BIY    \n"+
                " BIF    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "   b    \n"+
                " BIY    \n"+
                " BIF    \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                " BIF    \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "   b    \n"+
                " BIY    \n"+
                "        \n"+
                "   BIF  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " BIY    \n"+
                "    b   \n"+
                "   BIF  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "     I  \n"+
                "     I  \n"+
                "BIY  I  \n"+
                "TIF bI  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                "     I  \n"+
                "BIY  I  \n"+
                "TIF  xI ").replace('x', '[Ib]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY          \n"+
                "DIS DIH  bk  \n"+
                "KIP KIO KIF  \n"+
                "           d \n"+
                "             ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY          \n"+
                "DIS DIH   b  \n"+
                "KIP KIO KIF  \n"+
                "             \n"+
                "             ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);