import { CombinaisonManager }  from "../src/Utils/CombinaisonManager";
let combinaisonManager:CombinaisonManager = CombinaisonManager.instance;

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('you', ['you', 'baba', 'is'])
    ).toEqual(
        [['you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('isyou', ['you', 'baba', 'is'])
    ).toEqual(
        [['is', 'you'], [ 'you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('babaisyou', ['you', 'baba', 'is'])
    ).toEqual(
        [['baba', 'is', 'you'], ['is', 'you'], ['you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('babaisflagisyou', ['you', 'baba', 'is', 'flag'])
    ).toEqual(
        [['baba', 'is', 'flag', 'is', 'you'], [ 'is', 'flag', 'is', 'you'], ['flag', 'is', 'you'], ['is', 'you'],  ['you']]
    );
});

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('babaflag', ['baba', 'flag'])
    ).toEqual(
        [['baba', 'flag'], ['flag']]
    );
});

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('babaflag', ['ba', 'flag'])
    ).toEqual(
        [['ba', 'ba', 'flag'], ['ba', 'flag'], ['flag']]
    );
});

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('bababaflag', ['baba', 'ba', 'flag'])
    ).toEqual(
        [['baba', 'ba', 'flag'], ['ba', 'baba', 'flag'], ['ba', 'ba', 'ba', 'flag'], ['baba', 'flag'], [ 'ba', 'ba', 'flag' ], ['ba', 'flag'], ['flag']]
    );
});

test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('abcdefg', ['abc', 'bcd', 'cde', 'def', 'efg'])
    ).toEqual(
        [['abc', 'def'], ['bcd', 'efg'], ['cde'], ['def'], ['efg']]
    );

});


test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('babamoflag', ['baba', 'flag'])
    ).toEqual(
        [['baba'], ['flag']]
    );

});


test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('beltisghostarisfall', ['belt', 'baba', 'ghost', 'star', 'fall', 'is'])
    ).toEqual(
        [['belt', 'is', 'ghost'], ['is', 'ghost'], ['ghost'], ['star', 'is', 'fall'], ['is', 'fall'], ['fall']]
    );
});

