import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

//compared with real game
let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',
    'T': 'text',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [
    {
        grid:   "  F b      \n"+
                " BIY       \n"+
                "  T   BIfIB\n"+
                "           ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "  Fb       \n"+
                " BIY       \n"+
                "  T   BIFIB\n"+
                "           ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " Ff        \n"+
                " BIY       \n"+
                "  T   BIFIB\n"+
                "           ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
   
    {
        grid:   " Fb        \n"+
                " BIY       \n"+
                "  T   BIFIB\n"+
                "           ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },

    {
        grid:   "Ff         \n"+
                " BIY       \n"+
                "  T   BIFIB\n"+
                "           ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);
