import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',

    'b': 'baba',
};


scriptTest(declarations, [
    {
        grid:   "      b \n"+
                " B      \n"+
                " I      \n"+
                " Y      ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                " B    b \n"+
                " I      \n"+
                " Y      ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);



scriptTest(declarations, [
    {
        grid:   "      b \n"+
                "        \n"+
                "        \n"+
                "     BIY",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "        \n"+
                "      b \n"+
                "        \n"+
                "     BIY",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);
