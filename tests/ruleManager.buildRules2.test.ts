import { TextOrObject } from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";
import { Rule } from "../src/Models/Rule";
import { RuleCompileManager } from "../src/Utils/RuleCompileManager";
import { LeftRightBaseRule } from "../src/Models/LeftRightBaseRule";

let ruleCompileManager: RuleCompileManager = RuleCompileManager.instance;

let levelState = new LevelState(
    new Level([
        'flag',
        'baba',
    ], [], [], [], '', 20, 5)
);

// test contigus text to rule, no geometry consideration

let babaText = new TextOrObject('baba', false, 0, 0);
let flagText = new TextOrObject('flag', false, 0, 0);
let isText = new TextOrObject('is', false, 0, 0);
let youText = new TextOrObject('you', false, 0, 0);
let winText = new TextOrObject('win', false, 0, 0);

levelState.addTextOrObject(new TextOrObject('baba', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('flag', true, 0, 0));



levelState.addTextOrObject(babaText);
levelState.addTextOrObject(flagText);
levelState.addTextOrObject(isText);
levelState.addTextOrObject(youText);
levelState.addTextOrObject(winText);

// baba is is you => 
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    babaText,
                    isText,
                    isText,
                    youText,
                ]
            ),
            [
            ]
        )
    ).toEqual(
        true
    )
});


// baba is you is => baba is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    babaText,
                    isText,
                    youText,
                    isText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});

// is baba is you => baba is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    isText,
                    babaText,
                    isText,
                    youText,
                ]
            ),
            [
                new Rule(
                    1,
                    [new LeftRightBaseRule(0, 'baba', 1, 2)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 3, 4)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// flag baba is you => baba is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    flagText,
                    babaText,
                    isText,
                    youText,
                ]
            ),
            [
                new Rule(
                    1,
                    [new LeftRightBaseRule(0, 'baba', 1, 2)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 3, 4)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// baba is you flag => baba is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    babaText,
                    isText,
                    youText,
                    flagText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});

// baba is you you => baba is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    babaText,
                    isText,
                    youText,
                    youText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// baba is you flag is win => baba is you, flag is win
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    babaText,
                    isText,
                    youText,
                    flagText,
                    isText,
                    winText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3)],
                ),
                new Rule(
                    3,
                    [new LeftRightBaseRule(0, 'flag', 3, 4)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'win', 5, 6)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});



// baba is flag is win => baba is flag, flag is win
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    babaText,
                    isText,
                    flagText,
                    isText,
                    winText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'flag', 2, 3)],
                ),
                new Rule(
                    2,
                    [new LeftRightBaseRule(0, 'flag', 2, 3)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'win', 4, 5)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});


// win is baba => 
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    winText,
                    isText,
                    babaText,
                ]
            ),
            [
            ]
        )
    ).toEqual(
        true
    )
});


// win baba is is baba win is flag is baba win flag => flag is baba
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](
                // contigous texts
                [
                    winText,
                    babaText,
                    isText,
                    isText,
                    babaText,
                    winText,
                    isText,
                    flagText,
                    isText,
                    babaText,
                    winText,
                    flagText,
                ]
            ),
            [
                new Rule(
                    7,
                    [new LeftRightBaseRule(0, 'flag', 7, 8)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'baba', 9, 10)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});