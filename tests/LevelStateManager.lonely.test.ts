import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'F': 'flag',
    'W': 'wall',

    'I': 'is',
    'N': 'not',
    'Y': 'you',
    'L': 'lonely',

    'b': 'baba',
    'f': 'flag',
    'w': 'wall',
};


scriptTest(declarations, [
    {
        grid:   "LBIF    \n"+
                "BIY     \n"+
                "        \n"+
                "   b    ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "LBIF    \n"+
                "BIY     \n"+
                "        \n"+
                "   f    ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "LBIF    \n"+
                "BIY     \n"+
                "        \n"+
                "   f    ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "NLBIF   \n"+
                "BIY     \n"+
                "        \n"+
                " w b    ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "NLBIF   \n"+
                "BIY     \n"+
                "        \n"+
                " wb     ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "NLBIF   \n"+
                "BIY     \n"+
                "        \n"+
                " x      ").replace('x', '[fw]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
   
]);
