import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'skull',
    'I': 'is',
    'Y': 'you',
    'M': 'move',
    'S': 'swap',

    'b': 'baba',
    'k': 'skull',
    
};

scriptTest(declarations, [
    {
        grid: ( "BIY    K\n"+
                "KIS    I\n"+
                "    bxM \n"+
                "        ").replace('x', '[^k]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY    K\n"+
                "KIS    I\n"+
                "    kbM \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY    K\n"+
                "KIS    I\n"+
                "    k bM\n"+
                "        ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    { 
        grid:   "BIY    K\n"+
                "KIS k  I\n"+
                "      bM\n"+
                "        ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

