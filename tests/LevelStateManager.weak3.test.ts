import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'keke',
    'I': 'is',
    'Y': 'you',
    'W': 'weak',
    'P': 'pull',
    'S': 'stop',

    'b': 'baba',
    'k': 'keke',
};

scriptTest(declarations, [
    {
        grid:   "BIY KIS\n"+
                "KIW    \n"+
                "  bk   \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY KIS\n"+
                "KIW    \n"+
                "   b   \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY KIP\n"+
                "KIW    \n"+
                "   bk  \n"+
                "       ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY KIP\n"+
                "KIW    \n"+
                "  bk   \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY KIP\n"+
                "KIW    \n"+
                "   b   \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


