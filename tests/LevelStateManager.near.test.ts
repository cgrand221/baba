import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'F': 'flag',
    'N': 'near',
    'I': 'is',
    'Y': 'you',
    
    'b': 'baba',
    'f': 'flag',
};

scriptTest(declarations, [
    {
        grid: ( "BNFIY     \n"+
                "          \n"+
                "  x       \n"+
                "          ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BNFIY     \n"+
                "          \n"+
                "  fb      \n"+
                "          ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BNFIY     \n"+
                "          \n"+
                "  f b     \n"+
                "          ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BNFIY     \n"+
                "          \n"+
                "  f b     \n"+
                "          ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

