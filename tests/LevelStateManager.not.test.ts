import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'N': 'not',
    'F': 'flag',
    'R': 'rock',
    'W': 'win',
    'K': 'keke',

    'b': 'baba',
    'f': 'flag',
    'r': 'rock',
    'k': 'keke',
};

scriptTest(declarations,  [
    {
        grid:   " NBIY   \n"+
                " FIW f  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " NBIY   \n"+
                " FIW  f \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_IS_WIN,
    },
    
]);

scriptTest(declarations,  [
    {
        grid:   " BIY FIW \n"+
                " FINW    \n"+
                "    bf   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY FIW \n"+
                " FINW    \n"+
                "     x   ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    
]);

scriptTest(declarations,  [
    {
        grid:   " NFINF  \n"+
                " BIF    \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " NFINF  \n"+
                " BIF    \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   "  BINF  \n"+
                "  BIF   \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "  BINF  \n"+
                "  BIF   \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   " RINY   \n"+
                " NRIY   \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " RINY   \n"+
                " NRIY   \n"+
                "     b  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);

scriptTest(declarations,  [
    {
        grid:   "            \n"+
                " NRIY       \n"+
                "    b       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "            \n"+
                " NRIY       \n"+
                "     b      ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);

scriptTest(declarations,  [
    {
        grid:   "       FINY \n"+
                " NRIY       \n"+
                "    b       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "       FINY \n"+
                " NRIY       \n"+
                "     b      ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);

scriptTest(declarations,  [
    {
        grid:   "  BINY  \n"+
                " NRIY   \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "  BINY  \n"+
                " NRIY   \n"+
                "    b   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations,  [
    {
        grid:   " NBIF  \n"+
                " FIR   \n"+
                "    f  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " NBIF  \n"+
                " FIR   \n"+
                "    f  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations,  [
    {
        grid:   " NBINF  \n"+
                "        \n"+
                "    f   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " NBINF  \n"+
                "        \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   " NBIF  \n"+
                "  FIB  \n"+
                "    fr ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " NBIF  \n"+
                "  FIB  \n"+
                "    ff ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations,  [
    {
        grid:   " NBIF  \n"+
                "   bfr ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   " NBIF  \n"+
                "   bff ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations,  [
    {
        grid:   "     NBIK  \n"+
                "   RIF r   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "     NBIK  \n"+
                "   RIF x   ").replace('x', '[fk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);

scriptTest(declarations,  [
    {
        grid:   " BIY NBIY  \n"+
                "   RIF r   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY NBIY  \n"+
                "   RIF  f  ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

