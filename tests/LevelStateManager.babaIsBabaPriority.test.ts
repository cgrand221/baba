import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'R': 'rock',
    'W': 'win',
    'T': 'text',

    'b': 'baba',
    'f': 'flag',
    'r': 'rock',
};


scriptTest(declarations, [
    {
        grid: ( "        \n"+
                " BIX    \n"+
                "   b    \n"+
                "    f   ").replace('X', '[BFY]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIX    \n"+
                "  b     \n"+
                "    f   ").replace('X', '[BFY]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIX    \n"+
                " b      \n"+
                "    f   ").replace('X', '[BFY]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIX    \n"+
                "b       \n"+
                "    f   ").replace('X', '[BFY]').replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
   
]);


scriptTest(declarations, [
    {
        grid: ( "        \n"+
                " BIX    \n"+
                " RIY    \n"+
                "    b   ").replace('X', '[FRY]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIX    \n"+
                " RIY    \n"+
                "   x    ").replace('X', '[FRY]').replace('x', '[fr]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIX    \n"+
                " RIY    \n"+
                "  rf    ").replace('X', '[FRY]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "        \n"+
                " BIX    \n"+
                " RIY    \n"+
                " r f    ").replace('X', '[FRY]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);


scriptTest(declarations, [
    {
        grid: ( " TIT    \n"+
                " BIY    \n"+
                " TIB    \n"+
                "    B   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " TIT    \n"+
                " BIY    \n"+
                " TIB    \n"+
                "    B   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
   
]);

scriptTest(declarations, [
    {
        grid: ( "  IT    \n"+
                " BIY    \n"+
                " TIB    \n"+
                "    B   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "  bb    \n"+
                " bbb    \n"+
                " bbb    \n"+
                "    b   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
   
]);
