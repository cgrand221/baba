import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',
    'P': 'push',

    'b': 'baba',
    'f': 'flag',
};


scriptTest(declarations, [
    {
        grid:   "        \n"+
                " BIY f  \n"+
                "   FIP  \n"+
                "     b  ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "     f  \n"+
                " BIY P  \n"+
                "   FIb  \n"+
                "        ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "     X  \n"+
                " BIY b  \n"+
                "   FI   \n"+
                "        ").replace('X', '[Pf]'),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "     X  \n"+
                " BIY b  \n"+
                "   FI   \n"+
                "        ").replace('X', '[Pf]'),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
