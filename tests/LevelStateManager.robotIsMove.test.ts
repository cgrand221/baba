import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

//compared with real game
let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'R': 'robot',
    'M': 'move',
    'P': 'push',
    'S': 'stop',

    'b': 'baba',
    'r': 'robot',
};


scriptTest(declarations, [
    {
        grid: ( "BIY     b\n"+
                "RIM  x   \n"+
                "         ").replace('x', '[>r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM   r  \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM    r \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM     r\n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM    r \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM   r  \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM  r   \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM r    \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIMr     \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM r    \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM  r   \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIY     b\n"+
                " RIM  xB \n"+
                "         ").replace('x', '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                " RIM r B \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                " RIMr  B \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIMr   B \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM r  B \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM  r B \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM   rB \n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM    rB\n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY     b\n"+
                "RIM   r B\n"+
                "         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIY    b R\n"+
                " RI M x  I\n"+
                "         P").replace('x', '[<r]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   b  R\n"+
                " RI M r  I\n"+
                "         P"),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY      R\n"+
                " RI M b  I\n"+
                "      r  P"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY      R\n"+
                " RI Mb   I\n"+
                "      r  P"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY      R\n"+
                " RIMb    I\n"+
                "      r  P"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY      R\n"+
                " RIMb r  I\n"+
                "         P"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   r  R\n"+
                " RIMb    I\n"+
                "         P"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY      R\n"+
                " RIMb r  I\n"+
                "         P"),
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY b    R\n"+
                " RIM     I\n"+
                "      r  P"),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIYb     R\n"+
                " RIM  r  I\n"+
                "         P"),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   r  R\n"+
                " RIb     I\n"+
                "   M     P"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   r  R\n"+
                " RI b    I\n"+
                "   M     P"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "RIM BIS RIM \n"+
                " b x  b     ").replace('x', '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS RIM \n"+
                " b r  b     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS RIM \n"+
                " b   rb     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS RIM \n"+
                " b r  b     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "RIM BIS RIM RIM\n"+
                " bx    b       ").replace('x', '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS RIM RIM\n"+
                " b   r b       "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS RIM RIM\n"+
                " b  r  b       "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "RIM BIS RIM RIM\n"+
                " bx  b         ").replace('x', '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS RIM RIM\n"+
                " b  rb         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS RIM RIM\n"+
                " br  b         "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "RIM BIS\n"+
                " bxb   ").replace('x', '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS\n"+
                " brb   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM BIS\n"+
                " brb   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid: ( "RIM RIS\n"+
                " x     ").replace('x', '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "r      "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                " r     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "  r    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "RIM RIS\n"+
                " xx    ").replace(/x/g, '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "rr     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                " rr    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "  rr   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "   rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "    rr "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "     rr"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "    rr "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "   rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "RIM RIM RIS\n"+
                " xx        ").replace(/x/g, '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIS\n"+
                " rr        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIS\n"+
                "   rr      "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIS\n"+
                "     rr    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIS\n"+
                "       rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIS\n"+
                "         rr"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIS\n"+
                "       rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIS\n"+
                "     rr    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "RIM RIP\n"+
                " x     ").replace('x', '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "r      "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                " r     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "  r    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "RIM RIP\n"+
                " xx    ").replace(/x/g, '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "rr     "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                " rr    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "  rr   "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "   rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "    rr "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "     rr"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "    rr "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIP\n"+
                "   rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);





scriptTest(declarations, [
    {
        grid: ( "RIM RIM RIP\n"+
                " xx        ").replace(/x/g, '[<r]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIP\n"+
                " rr        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIP\n"+
                "   rr      "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIP\n"+
                "     rr    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIP\n"+
                "       rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIP\n"+
                "         rr"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIP\n"+
                "       rr  "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIM RIP\n"+
                "     rr    "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "RIM RIS\n"+
                "BIM BIS\n"+
                " x     ").replace('x', '[<r<b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "RIM RIS\n"+
                "BIM BIS\n"+
                "x      ").replace('x', '[br]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
   
]);



scriptTest(declarations, [
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "  xBx   \n"+
                "        ").replace(/x/g, "[<r]"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                " rBr    \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "rBr     \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                " rBr    \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "  rBr   \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "   rBr  \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "    rBr \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "     rBr\n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "    rBr \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( " RIP    \n"+
                " RIM    \n"+
                "   rBr  \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);