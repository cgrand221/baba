import { TextOrObject }  from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";
import { RuleCompileManager } from "../src/Utils/RuleCompileManager";

let ruleCompileManager: RuleCompileManager = RuleCompileManager.instance;

let levelState = new LevelState(
    new Level([
        'flag',
        'baba',
    ], [], [], [], '', 20, 5)
);

// test contigus text to rule, no geometry consideration

let babaText1 = new TextOrObject('baba', false, 0, 0);
let isText1 = new TextOrObject('is', false, 0, 0);
let youText1 = new TextOrObject('you', false, 0, 0);
let onText1 = new TextOrObject('on', false, 0, 0);

levelState.addTextOrObject(new TextOrObject('baba', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('flag', true, 0, 0));

levelState.addTextOrObject(babaText1);
levelState.addTextOrObject(isText1);
levelState.addTextOrObject(youText1);
levelState.addTextOrObject(onText1);


// is baba is flag => [baba is flag]
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
   
    babaText1.rules=[];
    onText1.rules=[];
    isText1.rules=[];
    youText1.rules=[];
    
    ruleCompileManager['buildRules'](
        // contigous texts
        [
            babaText1,
            onText1,
            isText1,
            youText1,
        ]
    );
    
    expect(
       [
            babaText1.rules.length,
            onText1.rules.length,
            isText1.rules.length,
            youText1.rules.length,
           
       ]
       
    ).toEqual(
        [
            0,
            0,
            0,
            0,
        ]
    )
});


