import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'C': 'ba',
    'I': 'is',
    'Y': 'you',
    'N': 'not',
    
    'b': 'baba',
};


scriptTest(declarations, [
    {
        grid:   "NCIY  \n"+
                "b     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "NCIY  \n"+
                "b     ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
]);
