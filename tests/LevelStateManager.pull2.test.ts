import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'R': 'rock',
    'L': 'pull',
    'I': 'is',
    'Y': 'you',

    'b': 'baba',
    'r': 'rock',
};

scriptTest(declarations, [
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "   brI  \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "  br I  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "  br I  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



scriptTest(declarations, [
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "   brI  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "   brI  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    
]);

