import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'key',
    'D': 'door',
    'O': 'open',
    'U': 'shut',
    'I': 'is',
    'Y': 'you',
    'A': 'wall',
    'W': 'weak',
    'P': 'push',
    'S': 'stop',
    'M': 'move',
    'F': 'float',
    'H': 'has',
    'N': 'and',

    'b': 'baba',
    'a': 'wall',
    'k': 'key',
    'd': 'door',
};


scriptTest(declarations, [
    {
        grid:   "BIY AIS\n"+
                "BIW BIF\n"+
                "   b a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIS\n"+
                "BIW BIF\n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY AIS\n"+
                "BIW BIF\n"+
                "     a \n"+
                "       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid:   "BIY    \n"+
                "BIW BIF\n"+
                "   b a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY    \n"+
                "BIW BIF\n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY    \n"+
                "BIW BIF\n"+
                "     x \n"+
                "       ").replace('x', '[ab]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid:   "BIY AIF\n"+
                "BIW BIF\n"+
                "   b a \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY AIF\n"+
                "BIW BIF\n"+
                "    ba \n"+
                "       ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY AIF\n"+
                "BIW BIF\n"+
                "     a \n"+
                "       "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid:   "BIY    \n"+
                " BIW   \n"+
                "   b   ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIYW   \n"+
                " BIb   \n"+
                "       ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIYW   \n"+
                " BI    \n"+
                "   b   ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIYW   \n"+
                " BI    \n"+
                "   b   ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIM BHB\n"+
                "BIW    \n"+
                "  b k  ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM BHB\n"+
                "BIW    \n"+
                "   bk  ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM BHB\n"+
                "BIW    \n"+
                "    x  ").replace('x', '[bk]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid:   "BIM BHB\n"+
                "BIW    \n"+
                "    kb ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY BHBNK\n"+
                "BIW      \n"+
                "  b      "),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY BHBNK\n"+
                "BIW      \n"+
                "  x      ").replace('x', '[bk]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);