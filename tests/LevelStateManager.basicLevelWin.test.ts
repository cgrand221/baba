import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'F': 'flag',
    'W': 'win',

    'b': 'baba',
    'f': 'flag',
};

scriptTest(declarations, [
    {
        grid:   "  F   b \n"+
                " BIY    \n"+
                "  W   f \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "  F     \n"+
                " BIY  b \n"+
                "  W   f \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "  F     \n"+
                " BIY    \n"+
                "  W   x \n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_IS_WIN,
    },
   
]);


scriptTest(declarations, [
    {
        grid:   "  F   b \n"+
                " BIY    \n"+
                " W    f \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "  F     \n"+
                " BIY  b \n"+
                " W    f \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "  F     \n"+
                " BIY    \n"+
                " W    x \n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);


scriptTest(declarations, [
    {
        grid:   "      b \n"+
                " BIY    \n"+
                " I      \n"+
                " W      ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "      b \n"+
                " BIY    \n"+
                " I      \n"+
                " W      ",
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_IS_WIN,
    },
   
   
]);
