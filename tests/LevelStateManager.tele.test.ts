import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'D': 'door',
    'K': 'keke',
    'T': 'tele',
    'F': 'float',

    'b': 'baba',
    'd': 'door',
    'k': 'keke',
};


scriptTest(declarations, [
    {
        grid:   " BIY     \n"+
                " DIT     \n"+
                "  b d  d ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY     \n"+
                " DIT     \n"+
                "   bd  d ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY     \n"+
                " DIT     \n"+
                "    d  x ").replace('x', '[bd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIY     \n"+
                " DIT     \n"+
                "    d  db",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( " BIY     \n"+
                " DIT     \n"+
                " bd  d  d"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY     \n"+
                " DIT     \n"+
                "  d  d  x").replace('x', '[bd]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY     \n"+
                " DIT     \n"+
                "  x  d  d").replace('x', '[bd]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY     \n"+
                " DIT     \n"+
                "  d  d  x").replace('x', '[bd]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY     \n"+
                " DIT     \n"+
                "  d  x  d").replace('x', '[bd]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "     KIT \n"+
                " DIT     \n"+
                "  d  x  k").replace('x', '[dk]'),  // 2 k->1 2 d->3
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "     KIT \n"+
                " DIT     \n"+
                "  k  d  x").replace('x', '[dk]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "     KIT \n"+
                " DIT     \n"+
                "  x  k  d").replace('x', '[dk]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( " BIY  BIF\n"+
                " DIT     \n"+
                " bd     d"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY  BIF\n"+
                " DIT     \n"+
                "  x     d").replace('x', '[bd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( " BIY  DIF\n"+
                " DIT     \n"+
                " bd     d"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY  DIF\n"+
                " DIT     \n"+
                "  x     d").replace('x', '[bd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( " BIY  DIF\n"+
                " DIT  BIF\n"+
                " bd     d"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( " BIY  DIF\n"+
                " DIT  BIF\n"+
                "  d     x").replace('x', '[bd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "BIY DIT  \n"+
                " bDd   d "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIT  \n"+
                "  bd   x ").replace('x', '[Dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);