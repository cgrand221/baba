import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'W': 'wall',
    'S': 'stop',

    'b': 'baba',
    'w': 'wall',
};

scriptTest(declarations, [
    {
        grid: ( "BIY     \n"+
                "WIS     \n"+
                "   x    \n"+
                "        ").replace('x', '[bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY     \n"+
                "WIS     \n"+
                "   wb   \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY     \n"+
                "WIS     \n"+
                "   wb   \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
