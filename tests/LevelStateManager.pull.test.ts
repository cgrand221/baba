import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'D': 'door',
    'K': 'key',
    'I': 'is',
    'Y': 'you',
    'M': 'move',
    'F': 'flag',
    'R': 'rock',
    'W': 'win',
    'L': 'pull',
    'P': 'push',
    'U': 'shut',
    'O': 'open',
    'S': 'stop',

    'b': 'baba',
    'd': 'door',
    'k': 'key',
    'f': 'flag',
    'r': 'rock',
};

scriptTest(declarations, [
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "   b  r \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "    b r \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "     br \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "     br \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "    br  \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "   br   \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "        \n"+
                "  br    \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "   b  r \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "    b r \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "     br \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "      br\n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "      br\n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "     br \n"+
                "        ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "    br  \n"+
                "        ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "BIY RIL \n"+
                "    RIP \n"+
                "     r  \n"+
                "    b   ",
        key: KeyCode.KEY_DOWN,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

// pull the push
// push under you
scriptTest(declarations, [
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   xr   \n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   xr   \n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "  xr    \n"+// b[fr]
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


// push the push, the push pul the pull
// pull under you
scriptTest(declarations, [
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   xf   \n"+
                "        ").replace('x', '[br]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "    xf  \n"+
                "        ").replace('x', '[br]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   brf  \n"+
                "        "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "  br f  \n"+
                "        "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);

scriptTest(declarations, [
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   x    \n"+
                "        ").replace('x', '[bf]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   fb   \n"+
                "        "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);   


// not real game
scriptTest(declarations, [
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   brxr \n"+
                "        ").replace('x', '[fr]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   brxr \n"+
                "        ").replace('x', '[fr]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "  brxr  \n"+
                "        ").replace('x', '[fr]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);   

scriptTest(declarations, [
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   brx  \n"+
                "        ").replace('x', '[fr]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "   brx  \n"+
                "        ").replace('x', '[fr]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY RIL \n"+
                "    FIP \n"+
                "  brrf  \n"+
                "        "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY DIS DIU \n"+
                "RIL KIL KIO \n"+
                "     kxr b  ").replace('x', '[dr]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS DIU \n"+
                "RIL KIL KIO \n"+
                "     kxrb   ").replace('x', '[dr]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
   {
        grid: ( "BIY DIS DIU \n"+
                "RIL KIL KIO \n"+
                "     kxrb   ").replace('x', '[dr]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS DIU \n"+
                "RIL KIL KIO \n"+
                "       rrb  ").replace('x', '[dr]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY   \n"+
                "RIL   \n"+
                " brb  "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   \n"+
                "RIL   \n"+
                "brb   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   \n"+
                "RIL   \n"+
                "brb   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   \n"+
                "RIL   \n"+
                " brb  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   \n"+
                "RIL   \n"+
                "  brb "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   \n"+
                "RIL   \n"+
                "   brb"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY   \n"+
                "RIL   \n"+
                "   brb"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);