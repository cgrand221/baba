import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'L': 'l',
    'O': 'o',
    'V': 'v',
    'E': 'e', 
    'X': 'love', 

    'b': 'baba',
    'f': 'flag',
    'l': 'love',
};


scriptTest(declarations, [
    {
        grid:   "        \n"+
                " LOVBIY \n"+
                "    E   \n"+
                "    b l ",
        key: KeyCode.KEY_UP,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "    B   \n"+
                " LOVEIY \n"+
                "    b   \n"+
                "      l ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "    B   \n"+
                " LOVEIY \n"+
                "    b   \n"+
                "       l",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
