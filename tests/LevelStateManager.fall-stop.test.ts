import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'L': 'belt',
    'G': 'ghost',
    'A': 'flag',

    'I': 'is',
    'F': 'fall',
    'Y': 'you',
    'S': 'stop',

    'b': 'baba',
    'l': 'belt',
    'g': 'ghost',
    'a': 'flag',
};




scriptTest(declarations, [
    {
        grid:   " A         a\n"+
                "L IG b BIY  \n"+
                " F          \n"+
                "           l\n"+
                "GIS         ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " A         a\n"+
                "L IGb  BIY  \n"+
                " F          \n"+
                "           l\n"+
                "GIS         ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " A          \n"+
                "LIGb   BIY  \n"+
                " F         a\n"+
                "           g\n"+
                "GIS         ",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
