import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'H': 'has',
    'Y': 'you',
    'N': 'not',
    'F': 'flag',
    'W': 'win',
    'E': 'weak',

    'b': 'baba',
    'f': 'flag',
};

scriptTest(declarations,  [
    {
        grid:   " BIB BIY\n"+
                " BIF b  \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BIB BIY\n"+
                " BIF  b \n"+
                "        ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations,  [
    {
        grid:   " BINB BIY \n"+
                "     b    \n"+
                "          ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BINB BIY \n"+
                "          \n"+
                "          ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations,  [
    {
        grid:   " BINB BIY \n"+
                " BIB b    \n"+
                " BIF      ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " BINB BIY \n"+
                " BIB  f   \n"+
                " BIF      ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations,  [
    {
        grid:   " FHNF BIY \n"+
                " FHF  FIE \n"+
                "    bf    ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " FHNF BIY \n"+
                " FHF  FIE \n"+
                "     b    ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);