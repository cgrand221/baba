import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'key',  
    'C': 'ice',  
    'R': 'robot', 
    'D': 'door', 
    'W': 'water', 
    'I': 'is',
    'Y': 'you',
    'F': 'float',
     
    'U': 'shut',
    'O': 'open',
    'T': 'stop',
    'P': 'push',
    
   
    'b': 'baba',
    'k': 'key',
    'c': 'ice',
    'r': 'robot',
    'd': 'door',
    'w': 'water',
};




scriptTest(declarations, [
    {
        grid: ( "BIY       \n"+
                "DIU WIU   \n"+//shuts
                "DIT WIT   \n"+//stops
                "KIO CIO   \n"+//opens
                "KIP CIP   \n"+//pushs
                "          \n"+//separation
                "KIF  CI F \n"+//floats
                "DIF  WI F \n"+//floats
                " bxy      ").replace('x', '[ck]').replace('y', '[dw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    {
        grid: ( "BIY       \n"+
                "DIU WIU   \n"+//shuts
                "DIT WIT   \n"+//stops
                "KIO CIO   \n"+//opens
                "KIP CIP   \n"+//pushs
                "          \n"+//separation
                "KIF  CI F \n"+//floats
                "DIF  WI F \n"+//floats
                "  b       ").replace('x', '[ck]').replace('y', '[dw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY       \n"+
                "DIU WI U  \n"+//shuts
                "DIT WIT   \n"+//stops
                "KIO CIO   \n"+//opens
                "KIP CIP   \n"+//pushs
                "          \n"+//separation
                "          \n"+//floats
                "          \n"+//floats
                " bxy      ").replace('x', '[ck]').replace('y', '[dw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    {
        grid: ( "BIY       \n"+
                "DIU WI U  \n"+//shuts
                "DIT WIT   \n"+//stops
                "KIO CIO   \n"+//opens
                "KIP CIP   \n"+//pushs
                "          \n"+//separation
                "          \n"+//floats
                "          \n"+//floats
                " bxy      ").replace('x', '[ck]').replace('y', '[dw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIY       \n"+
                "DIU WIU   \n"+//shuts
                "DIT WIT   \n"+//stops
                "KIO CIO   \n"+//opens
                "KIP CIP   \n"+//pushs
                "          \n"+//separation
                "KIF  CIF  \n"+//floats
                "DIF  WI F \n"+//floats
                " bxy      ").replace('x', '[ck]').replace('y', '[dw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
    {
        grid: ( "BIY       \n"+
                "DIU WIU   \n"+//shuts
                "DIT WIT   \n"+//stops
                "KIO CIO   \n"+//opens
                "KIP CIP   \n"+//pushs
                "          \n"+//separation
                "KIF  CIF  \n"+//floats
                "DIF  WI F \n"+//floats
                " bxy      ").replace('x', '[ck]').replace('y', '[dw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);