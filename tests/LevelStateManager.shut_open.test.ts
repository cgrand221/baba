import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'K': 'key',
    'W': 'win',
    'D': 'door',
    'T': 'stop',
    'S': 'shut',
    'O': 'open',
    'P': 'push',
    'M': 'move',

    'b': 'baba',
    'k': 'key',
    'd': 'door',
};


scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " bk  dd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  bk dd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   bkdd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "    b d "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "     bd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "     bd "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " bkk dd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  bkkdd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   bk d "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "    bkd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "     b  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "      b "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "       b"),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "       b"),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " bx  dd ").replace('x', '[kk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  bx dd ").replace('x', '[kk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   bxdd ").replace('x', '[kk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "    bkd "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "     b  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
   
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " bk  y  ").replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  bk y  ").replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   bky  ").replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "    bd  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "    bd  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " bx  y  ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  bx y  ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   bxy  ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "    b   ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "     b  ").replace('x', '[kk]').replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                " bx  d  ").replace('x', '[Yk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "  bx d  ").replace('x', '[Yk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   bxd  ").replace('x', '[Yk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT KIO \n"+
                "KIP     \n"+
                "   bxd  ").replace('x', '[Yk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                " bk  d  ").replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "   bkd  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "     b  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                " bx  d  ").replace('x', '[kk]').replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "   bxd  ").replace('x', '[kk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "     bk ").replace('x', '[kk]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);




scriptTest(declarations, [
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                " bk  y  ").replace('y', '[dd]').replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "   bky  ").replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIM DIS \n"+
                "DIT KIO \n"+
                "KIP BIM \n"+
                "    bd  ").replace('y', '[dd]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
    
]);


scriptTest(declarations, [
    {
        grid: ( "BIM DIS BIM\n"+
                "DIT KIO BIM\n"+
                "KIP        \n"+
                " bkkdd     ").replace('b', '[>b]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIM DIS BIM\n"+
                "DIT KIO BIM\n"+
                "KIP        \n"+
                "    b      "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "  bb d  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "   bbd  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "    b   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIP     \n"+
                "  bb d  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIP     \n"+
                "   bbd  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIP     \n"+
                "    b   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIT     \n"+
                "  bb d  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIT     \n"+
                "   bbd  "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "BIT     \n"+
                "    b   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [    
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "   x d  ").replace('x', '[bb]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "    xd  ").replace('x', '[bb]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "DIT BIO \n"+
                "        \n"+
                "     b  ").replace('x', '[bb]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
