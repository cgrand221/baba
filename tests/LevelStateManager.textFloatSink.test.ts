import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'W': 'water', 
    'I': 'is',
    'Y': 'you',
    'F': 'float',
    'T': 'text',
     
    'S': 'sink',
   
    'b': 'baba',
    'w': 'water',
};




scriptTest(declarations, [
    {
        grid: ( "BIY TI F\n"+
                "WIS     \n"+
                "b Bw    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TI F\n"+
                "WIS     \n"+
                " bBw    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TI F\n"+
                "WIS     \n"+
                "  b     "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BIY TIF \n"+
                "WIS     \n"+
                "b Bw    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TIF \n"+
                "WIS     \n"+
                " bBw    "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY TIF \n"+
                "WIS     \n"+
                "  bx    ").replace('x', '[Bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);
