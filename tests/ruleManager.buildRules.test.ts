import { TextOrObject } from "../src/Models/TextOrObject";
import { LevelState } from "../src/Models/LevelState";
import { Level } from "../src/Models/Level";
import { Rule } from "../src/Models/Rule";
import { RuleCompileManager } from "../src/Utils/RuleCompileManager";
import { LeftRightBaseRule } from "../src/Models/LeftRightBaseRule";

let ruleCompileManager: RuleCompileManager = RuleCompileManager.instance;

let levelState = new LevelState(
    new Level([
        'baba',
        'flag',
    ], [], [], [], '', 8, 5)
);

// test contigus text to rule, no geometry consideration

let babaText = new TextOrObject('baba', false, 0, 0);
let isText = new TextOrObject('is', false, 0, 0);
let youText = new TextOrObject('you', false, 0, 0);
let flagText = new TextOrObject('flag', false, 0, 0);
let winText = new TextOrObject('win', false, 0, 0);
let aText = new TextOrObject('a', false, 0, 0);
let bText = new TextOrObject('b', false, 0, 0);
let abText = new TextOrObject('ab', false, 0, 0);

levelState.addTextOrObject(babaText);
levelState.addTextOrObject(isText);
levelState.addTextOrObject(youText);
levelState.addTextOrObject(flagText);
levelState.addTextOrObject(winText);
levelState.addTextOrObject(aText);
levelState.addTextOrObject(bText);
levelState.addTextOrObject(abText);

levelState.addTextOrObject(new TextOrObject('baba', true, 0, 0));
levelState.addTextOrObject(new TextOrObject('flag', true, 0, 0));

// baba is you => baba is you
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    babaText,
                    isText,
                    youText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 2, 3)],
                ),
            ]
        )
    ).toEqual(
        true
    )
});

// flag is win => flag is win
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);
    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    flagText,
                    isText,
                    winText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'flag', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'win', 2, 3)],
                )
            ]
        )
    ).toEqual(
        true
    )
});

// b ab b is win => baba is win
test('ruleCompileManager.buildRules', () => {
    ruleCompileManager.init(levelState);

    expect(
        Rule.equalsAll(
            ruleCompileManager['buildRules'](

                // contigous texts
                [
                    bText,
                    abText,
                    aText,
                    isText,
                    winText,
                ]
            ),
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'win', 2, 3)],
                )
            ]
        )
    ).toEqual(
        true
    )
});

