import { LeftRightBaseRule } from "../src/Models/LeftRightBaseRule";
import { Rule } from "../src/Models/Rule";



test('ruleManager.compileAndCalcProperties', () => {
    expect(
        Rule.equalsAll(
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 0, 1)],
                )
            ]
            ,
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 0, 1)],
                )
            ]
        )

    ).toEqual(
        true
    )
});


test('ruleManager.compileAndCalcProperties', () => {
    expect(
        Rule.equalsAll(
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 0, 1)],
                )
            ]
            ,
            [
            ]
        )

    ).toEqual(
        false
    )
});



test('ruleManager.compileAndCalcProperties', () => {
    expect(
        Rule.equalsAll(
            [

            ]
            ,
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 0, 1)],
                )
            ]
        )

    ).toEqual(
        false
    )
});



test('ruleManager.compileAndCalcProperties', () => {
    let baba = new LeftRightBaseRule(0, 'baba', 0, 1);
    baba.disableRights.push('you');
    let rule = new Rule(
        0,
        [baba],
        [],
        'is',
        [new LeftRightBaseRule(0, 'you', 0, 1)],
    );

    expect(
        Rule.equalsAll(
            [
                new Rule(
                    0,
                    [new LeftRightBaseRule(0, 'baba', 0, 1)],
                    [],
                    'is',
                    [new LeftRightBaseRule(0, 'you', 0, 1)],
                )
            ]
            ,
            [
                rule
            ]
        )

    ).toEqual(
        false
    )
});