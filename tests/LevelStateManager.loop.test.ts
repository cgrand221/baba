import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'K': 'keke',  
    'I': 'is',
    'Y': 'you',
    'V': 'move',
     
    'U': 'shut',
    'O': 'open',
    'H': 'hot',
    'M': 'melt',
    'S': 'sink',
    'D': 'defeat',
    
   
    'b': 'baba',
    'k': 'keke',
};




scriptTest(declarations, [
   
    {
        grid: ( "BIO KIU \n"+
                "BIV BIV \n"+
                "  bk    ").replace('b', '[>b]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BIO KIU \n"+
                "BIV BIV \n"+
                "        "),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    
    
]);
