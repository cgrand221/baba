import { gridNbRulesEnabled, scriptTest } from './scripts';

let declarations = {
    'B': 'baba',
    'L': 'b',
    'M': 'ab',
    'N': 'a',
    'I': 'is',
    'H': 'has',
    'Y': 'you',
    'F': 'flag',
    'K': 'keke',
    'A': 'and',
    'T': 'not',

    'b': 'baba',
    'f': 'flag',
    'k': 'keke',
};

gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BIY    \n"+
                " BIBIF  \n"+
                "   b    \n"+
                "        ",
    },
    [
        0,0,0,
        0,0,1,1,1,
        0
    ]
);

gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BIB    \n"+
                " BBIF   \n"+
                "   b    \n"+
                "        ",
    },
    [
        0,0,0,
        0,1,1,1,
        0
    ]
);

gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BIB    \n"+
                " BIFF   \n"+
                "   b    \n"+
                "        ",
    },
    [
        0,0,0,
        1,1,1,0,
        0
    ]
);

gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BIB     \n"+
                " ABIFAKI \n"+
                "   b     \n"+
                "         ",
    },
    [
        0,0,0,
        0,1,1,1,1,1,0,
        0
    ]
);


gridNbRulesEnabled(
    declarations, 
    {
        grid:   " LMNIB   \n"+// b ab b is baba
                " BIF     \n"+// baba is flag
                "   b     \n"+
                "         ",
    },
    [
        0,0,0,0,0,
        1,1,1,
        0
    ]
);


gridNbRulesEnabled(
    declarations, 
    {
        grid:   " LMNIF   \n"+// b ab b is flag
                " BIB     \n"+// baba is baba
                "   b     \n"+
                "         ",
    },
    [
        1,1,1,1,1,
        0,0,0,
        0
    ]
);


gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BIYAK   \n"+
                "   b     \n"+
                " BIB     ",
    },
    [
        0,0,0,0,0,
        0,
        0,0,0,
    ]
);
// todo améliorer 
// 0,0,0,1,1,
// 0,
// 0,0,0,


gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BHB    \n"+
                " BHFF   \n"+
                "   b    \n"+
                "        ",
    },
    [
        0,0,0,
        0,0,0,0,
        0
    ]
);


gridNbRulesEnabled(
    declarations, 
    {
        grid:   " BIB    \n"+
                " BITK   \n"+
                "        ",
    },
    [
        0,0,0,
        0,0,0,0,
    ]
);