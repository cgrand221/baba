import { CombinaisonManager }  from "../src/Utils/CombinaisonManager";
let combinaisonManager:CombinaisonManager = CombinaisonManager.instance;

test('combinaisonManager.findMatchedInText', () => {
    expect(
        combinaisonManager.findMatchedInText('wallisstop', ['wall', 'all', 'is', 'stop'])
    ).toEqual(
        [['wall', 'is', 'stop']]
    );
});


test('combinaisonManager.findMatchedInTextWithCut', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCut('wallisstop', ['wall', 'all', 'is', 'stop'])
    ).toEqual(
        [['wall', 'is', 'stop'], ['all', 'is', 'stop'], ['is', 'stop'], ['stop']]
    );
});

test('combinaisonManager.findMatchedInTextWithCutAndMerge', () => {
    expect(
        combinaisonManager.findMatchedInTextWithCutAndMerge('wallisstop', ['wall', 'all', 'is', 'stop'])
    ).toEqual(
        [['wall', 'is', 'stop'], ['all', 'is', 'stop']]
    );
});
