import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'A': 'a', 
    'C': 'ab', 
    'L': 'belt', 

    'b': 'baba',
    'l': 'belt',
};


scriptTest(declarations, [
    {
        grid:   " LIY   C\n"+
                "   l    ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   " LIY   C\n"+
                "    l   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);


scriptTest(declarations, [
    {
        grid:   "ALIY   C\n"+
                "   l    ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid:   "ALIY   C\n"+
                "    l   ",
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);
