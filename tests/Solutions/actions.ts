
import { LevelState } from '../../src/Models/LevelState';
import { LevelStateBuildManager } from '../../src/Utils/LevelStateBuildManager';
import { FinalLevelReader } from '../../src/Utils/FinalLevelReader';
import * as fs from 'fs';
import { LevelStateManager } from '../../src/Utils/LevelStateManager';
import { KeyCode } from '../../src/Models/KeyCode';
import { SaveManager } from '../../src/Utils/SaveManager';

let levelStateBuildManager = LevelStateBuildManager.instance
let finalLevelReader = FinalLevelReader.instance
let levelStateManager = LevelStateManager.instance;
let saveManager = SaveManager.instance;

finalLevelReader.initLevelReader(true);

const recursiveReaddirSync = (path: string): string[] => {
  let retours: string[] = [];
  if (fs.lstatSync(path).isDirectory()) {
    fs.readdirSync(path).forEach(element => {
      retours.push(...recursiveReaddirSync(path + '/' + element))
    });
  }
  else {
    retours.push(path);
  }
  return retours;
}


const levelPath = fs.realpathSync(__dirname + '/../..')+'/static/levels/';

const isRequiredState = (file: string, levelState: LevelState): boolean => {

    // return no exists error
    return (
      levelState.level.actions.length > 0
      && levelState.level.actions.every(action => {
        

        action.requirements.forEach(requirement => {
          let otherLevel = requirement[0];
          let actionNumber = requirement[1];

          finalLevelReader.simulateSublevelWithFile(
            otherLevel,
            actionNumber,
            () => {}
          );
        });

        let ls = levelState.clone();
        levelStateManager.init(ls);
        const lastPos = action.keyOrNumberSolutions.length - 1;

        // try to find error
        return action.keyOrNumberSolutions.every((keyOrNumberSolution, index): boolean => {
          let actionRules = [...action.rules].join(',')
          let key = KeyCode.KEY_ENTER;
          if(typeof keyOrNumberSolution !== 'number'){
            key = keyOrNumberSolution
          }

          [, ls] = levelStateManager.getNextLevellState(ls, key);
          if(typeof keyOrNumberSolution === 'number'){
            // enter save cursor position in GameController

            let select = ls.calcLevelSelect();
            if (null !== select){
                let levelPath = ls.calcLevelPathOfSelect(select);
                if (null !== levelPath) {
                  saveManager.setSelectLevelPath(ls, levelPath);
                  finalLevelReader.simulateSublevel(
                    ls,
                    keyOrNumberSolution,
                    () => {}
                  );
                  ls = levelState.clone();
                  levelStateManager.init(ls);
                }
            }
          }
          
          let saveManagerRules = saveManager.getRules(file).map(rule => rule.toString()).join(',');
          
          return (
            index < lastPos
            && (
              // only last action in for win or for parent rule
              // we allow LevelState.STATE_IS_ZONE_COMPLETED before last action
              ls.state !== LevelState.STATE_IS_NEW_PARENT_RULES
              && ls.state !== LevelState.STATE_IS_WIN
            )
            && (
              actionRules === ''
              || saveManagerRules !== actionRules
            )
          ) || (
            index === lastPos
            && ls.state === action.doAction
            && saveManagerRules === actionRules
          )
          
          // error
          return false;
        });
      }));
  }

export const testMap = (map :string ) => {
    let filePaths = (recursiveReaddirSync(levelPath + map)).filter(level => level.endsWith('.xml')).map(filePath => filePath.substring(levelPath.length));
    filePaths.forEach(filePath => {
      let ls:LevelState|null = null
      finalLevelReader.readLevelState(filePath, (levelState: LevelState) => {
        ls = levelState;
      });

      test('Solution OK : ' + filePath, () => {
        saveManager.fromString('{}');
       
        expect(
          ls
        ).toBeInstanceOf(
          LevelState
        );
        
        if (null !== ls) {
          expect(
            ls.level.actions.length
          ).toBeGreaterThan(
            0
          );

          expect(
            isRequiredState(filePath, ls)
          ).toEqual(
            true
          )
        }
      });
    });
}


