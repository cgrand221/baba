import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'I': 'is',
    'Y': 'you',
    'K': 'key',
    'D': 'door',
    'S': 'shut',
    'O': 'open',
    'P': 'pull',
    'U': 'push',

    'b': 'baba',
    'k': 'key',
    'd': 'door',
};



scriptTest(declarations, [

    {
        grid: ( "BIY DIS \n"+
                "KIU KIO \n"+
                "        \n"+
                "    bkx ").replace('x', '[Id]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "KIU KIO \n"+
                "        \n"+
                "     b I").replace('x', '[Id]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
]);



scriptTest(declarations, [

    {
        grid:   "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                "     dbk",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                "     xk ").replace('x', '[bd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                "    b   "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);




scriptTest(declarations, [

    {
        grid:   "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                "   dbkkk",
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                "   xkkk ").replace('x', '[bd]'),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BIY DIS \n"+
                "KIP KIO \n"+
                "        \n"+
                "  b kk  "),
        key: KeyCode.KEY_LEFT,
        state: LevelState.STATE_CAN_PLAY,
    },
    
]);



