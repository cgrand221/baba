import { CombinaisonManager }  from "../src/Utils/CombinaisonManager";
let combinaisonManager:CombinaisonManager = CombinaisonManager.instance;

let a = 'A';
let b = 'B';
let c = 'C';
let d = 'D';
let e = 'E';


test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
                [
                    a, b,
                ],
                [
                    c, d,
                ]
            ], 0
        )
    ).toEqual(
        [
            [
                a, c,
            ],
            [
                a, d,
            ],
            [
                b, c,
            ],
            [
                b, d,
            ]
        ]
    )
});


test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
                [
                    a, b,
                ],
                [
                    c
                ]
            ], 0
        )
    ).toEqual(
        [
            [
                a, c,
            ],
            [
                b, c,
            ],
        ]
    )
});


test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
                [
                    a
                ],
                [
                    c
                ]
            ], 0
        )
    ).toEqual(
        [
            [
                a, c,
            ],
        ]
    )
});




test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
                [
                    a, c
                ],
            ], 0
        )
    ).toEqual(
        [
            [
                a,
            ],
            [
                c,
            ],
        ]
    )
});

test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
                [
                    a,
                ],
            ], 0
        )
    ).toEqual(
        [
            [
                a,
            ],
        ]
    )
});

test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
                [
                ],
            ], 0
        )
    ).toEqual(
        [
        ]
    )
});

test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
            ], 0
        )
    ).toEqual(
        [
        ]
    )
});


test('combinaisonManager.getCombinations', () => {
    expect(
        combinaisonManager.getCombinations(
            [
                [
                    a, b,
                ],
                [
                    c,
                ],
                [
                    d, e,
                ]
            ], 0
        )
    ).toEqual(
        [
            [
                a, c, d
            ],
            [
                a, c, e
            ],
            [
                b, c, d
            ],
            [
                b, c, e
            ],
        ]
    )
});

