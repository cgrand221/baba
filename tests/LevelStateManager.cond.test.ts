import { scriptTest } from './scripts';
import { KeyCode } from "../src/Models/KeyCode";
import { LevelState } from '../src/Models/LevelState';

let declarations = {
    'B': 'baba',
    'O': 'on',
    'W': 'wall',
    'I': 'is',
    'Y': 'you',
    'N': 'not',
    'F': 'flag',
    'A': 'and',

    'b': 'baba',
    'w': 'wall',
    'f': 'flag',
};

scriptTest(declarations, [
    {
        grid: ( "BOWIY   \n"+
                "        \n"+
                " xww    ").replace('x', '[bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BOWIY   \n"+
                "        \n"+
                " wxw    ").replace('x', '[bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BOWIY   \n"+
                "        \n"+
                " wwx    ").replace('x', '[bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BOWIY   \n"+
                "        \n"+
                " wwwb   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BOWIY   \n"+
                "        \n"+
                " wwwb   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BOWIB   \n"+
                "BIW     \n"+
                " x b    ").replace('x', '[bw]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BOWIB   \n"+
                "BIW     \n"+
                " x w    ").replace('x', '[bw]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BOWIB   \n"+
                "BIW     \n"+
                " x w    ").replace('x', '[bw]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BOWIW   \n"+
                "BIB     \n"+
                " x b    ").replace('x', '[bw]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BOWIW   \n"+
                "BIB     \n"+
                " x b    ").replace('x', '[bw]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BNOWIB  \n"+
                "BIW     \n"+
                " x b    ").replace('x', '[bw]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BNOWIB  \n"+
                "BIW     \n"+
                " x b    ").replace('x', '[ww]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BNOWIB  \n"+
                "BIW     \n"+
                " x b    ").replace('x', '[ww]'),
        key: KeyCode.KEY_SPACE,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BNNOWIY \n"+
                "        \n"+
                " xww    ").replace('x', '[bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BNNOWIY \n"+
                "        \n"+
                " wxw    ").replace('x', '[bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BNNOWIY \n"+
                "        \n"+
                " wwx    ").replace('x', '[bw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BNNOWIY \n"+
                "        \n"+
                " wwwb   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BNNOWIY \n"+
                "        \n"+
                " wwwb   "),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);


scriptTest(declarations, [
    {
        grid: ( "BONWIY  \n"+
                "        \n"+
                "b x z j ").replace('x', '[bw]').replace('z', '[bf]').replace('j', '[bfw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BONWIY  \n"+
                "        \n"+
                "b x fbkb").replace('x', '[bw]').replace('k', '[fw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
    {
        grid: ( "BONWIY  \n"+
                "        \n"+
                "b x fbkb").replace('x', '[bw]').replace('k', '[fw]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);



scriptTest(declarations, [
    {
        grid: ( "BOWAWIY  \n"+
                "         \n"+
                " b x z   ").replace('x', '[bw]').replace('z', '[bww]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_PLAY,
    },
    {
        grid: ( "BOWAWIY  \n"+
                "         \n"+
                " b x zb  ").replace('x', '[bw]').replace('z', '[ww]'),
        key: KeyCode.KEY_RIGHT,
        state: LevelState.STATE_CAN_NOT_PLAY,
    },
]);