# Typescript baba is you

## Description

This project is a replication of the "baba is you" operating principle. 
For educational use only. 

Please buy the original "baba is you" game if you wish to play.

See [demo](https://cgrand221.gitlab.io/baba/)

## How to start


Install volta (source https://docs.volta.sh/guide/getting-started)
```
curl https://get.volta.sh | bash
```

Check volta is running with the required node version : (source https://docs.volta.sh/guide/#why-volta)
- Open terminal in project and run :
```
node -v
```

Install required node modules with npm :
```
npm install
```

Run project :
- ```
  npm run dev
  ```
- Open [browser](http://localhost:1234)

Run unit tests :
```
npm test
```

Build application to deploy :
```
npm run deploy
```
